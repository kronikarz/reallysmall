#include "Private.h"
#include "../include/rsl/ExecutionContext.h"
#include "../include/rsl/CppInterop.h"
#include <assert.h>
#include <utf8/unchecked.h>
#include <unicode/utf8.h>
#include <unicode/uchar.h>

namespace rsl
{

	static inline constexpr auto DeclarationFlags = flag_bits(BaseObjectFlags::Marked, BaseObjectFlags::Permanent, BaseObjectFlags::Const);

#define RSL_FLOAT_IS_VALID_INDEX
	intptr_t CheckIndexOOB(ExecutionContext& vm, Value index, intptr_t length, const char* type)
	{
		intptr_t index_val = -1;
		if (index.IsInt())
			index_val = index.Int;
#ifdef RSL_FLOAT_IS_VALID_INDEX
		else if (index.IsFloat())
			index_val = (intptr_t)index.Float;
#endif
		else
			vm.ThrowRuntimeException("value of type '{}' is not a valid index to value of type '{}'", index.RealType()->FullyQualifiedName(), type);
		if (index_val < 0)
			index_val = index_val + length;
		if (index_val < 0 || index_val >= length)
			vm.ThrowRuntimeException(type, "index {} out of bounds; maximum length is {}", index_val, length);
		return index_val;
	}

	Value GCBase::IndexGet(ExecutionContext& vm, Value key) const
	{
		/// TODO: Metamethods
		vm.ThrowRuntimeException("cannot index value of this type");
	}

	void GCBase::IndexSet(ExecutionContext& vm, Value key, Value value)
	{
		vm.ThrowRuntimeException("cannot index value of this type");
	}

	intptr_t GCBase::IndexSize(ExecutionContext& vm) const
	{
		vm.ThrowRuntimeException("cannot index value of this type");
	}

	Method::Method(Class* parent_class, Sym* name, uint32_t flags)
		: Declaration(parent_class->ParentNamespace(), name), ParentClass(parent_class)
	{
	}

	Method::Method(Class* parent_class, SourcePos const& pos, Sym* name, uint32_t flags)
		: Declaration(parent_class->ParentNamespace(), pos, name)
		, ParentClass(parent_class)
		, Statements(std::make_shared<std::vector<StatTree>>())
	{
	}

	std::string Method::ToString() const
	{
		return fmt::format("func {}", mFullyQualifiedName);
	}

	void Module::Link(Method* method)
	{
		if (method->Native) return;

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Starting method link for {}", method->FullyQualifiedName());
#endif

		mCurrentMethod = method;
		try
		{
			for (auto& stat : *method->Statements)
			{
				CodeGen(stat);
			}
		}
		catch (RSException& e)
		{
			e.SubLines.push_back(fmt::format("while creating method '{}' here: {}", mCurrentMethod->FullyQualifiedName(), mCurrentMethod->mDeclPos));
			throw;
		}
		mCurrentMethod = nullptr;

		/// Clear the AST and parameter list after linking
		//method->Parameters.clear(); method->Parameters.shrink_to_fit();
		method->Statements.reset();

#ifdef RSL_DEBUG
		for (size_t i = 0; i < method->Code.size(); i++)
			Report(ReportType::Trace, ReportModule::CodeGen, "{}: {}", i, method->Code[i].ToString());

		Report(ReportType::Trace, ReportModule::CodeGen, "Finished method link for {}", method->FullyQualifiedName());
#endif
	}

	Obj::Obj(struct Class const* klass)
		: Class(klass)
	{
		new (RawSlots()) Value[klass->SlotCount()];
		//std::uninitialized_fill_n(Slots, parent_class->SlotCount, Value{ nullptr });
	}

	Obj* Obj::FromNative(void* obj)
	{
		auto obj_repr = (uint8_t*)obj;
		obj_repr -= offsetof(Obj, mAdditionalData);
		return reinterpret_cast<Obj*>(obj_repr);
	}

	std::string Obj::ToString() const
	{
		return fmt::format("instance of {}", Class->FullyQualifiedName());
	}

	void Obj::IndexSet(ExecutionContext& vm, Value key, Value val)
	{
		vm.ThrowRuntimeException("unimplemented index set on '{}'", Class->FullyQualifiedName());
	}

	Value Obj::IndexGet(ExecutionContext& vm, Value key) const
	{
		vm.ThrowRuntimeException("unimplemented index get on '{}'", Class->FullyQualifiedName());
	}

	void Obj::MarkMembers()
	{
		/// Note: We don't mark classes as they are PermanentF
		//const_cast<::Class*>(Class)->Mark();
		for (auto& slot : Slots())
		{
			slot.Mark();
		}
	}

	Sym::Sym(std::string_view str)
		: GCBase(DeclarationFlags)
		, Hash(std::hash<std::string_view>{}(str)), Len(str.size()), Data{}
	{
		auto data = const_cast<char*>(Data);
		new (data) char[str.size() + 1];
		memcpy(data, str.data(), Len);
		data[Len] = 0;
	}

	std::string Sym::ToString() const
	{
		return std::string{ '"' } + Data + '"';
	}

	void Sym::IndexSet(ExecutionContext& vm, Value key, Value val)
	{
		vm.ThrowRuntimeException("cannot set elements of symbol");
	}

	Value Sym::IndexGet(ExecutionContext& vm, Value key) const
	{
		/// TODO: Should we get the byte or the UTF-8 codepoint?
		auto index = CheckIndexOOB(vm, key, Len, "Symbol");
		return vm.Mod.MV((intptr_t)Data[index]);
	}

	Map::Map(bool permanent)
		: GCBase(permanent ? DeclarationFlags : 0)
	{

	}

	void Map::IndexSet(ExecutionContext& vm, Value key, Value val)
	{
		if (Is(BaseObjectFlags::Const))
			vm.ThrowRuntimeException("trying to modify constant map");

		if (val.IsNull())
			Values.erase(key);
		else
			Values.insert_or_assign(key, val);
	}

	Value Map::IndexGet(ExecutionContext& vm, Value key) const
	{
		auto it = Values.find(key);
		if (it == Values.end())
			return vm.Null;
		return it->second;
	}

	std::string Map::ToString() const
	{
		return "map";
	}

	void Map::MarkMembers()
	{
		for (auto& [k, v] : Values)
		{
			k.Mark();
			v.Mark();
		}
	}

	Value Arr::Push(Value val)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant array" };
		Values.push_back(val);
		return val;
	}

	Value Arr::Insert(intptr_t at, Value val)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant array" };
		at = CheckIndex(at);
		Values.insert(Values.begin() + at, val);
		return val;
	}

	void Arr::Resize(intptr_t s, Value v)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant array" };
		if (s < 0)
			s = std::max(0LL, intptr_t(Values.size()) + s);
		Values.resize(size_t(s), v);
	}

	Value Arr::Erase(intptr_t at)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant array" };
		at = CheckIndex(at);
		Value result = Values[at];
		Values.erase(Values.begin() + at);
		return result;
	}

	Value Arr::Pop(ExecutionContext const& vm)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant array" };
		if (Values.empty())
			return vm.Null;
		Value result = Values.back();
		Values.pop_back();
		return result;
	}

	void Arr::Clear()
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant array" };
		Values.clear();
	}

	std::string Arr::ToString() const
	{
		return std::string{ "arr(" } + std::to_string(Values.size()) + ")";
	}

	intptr_t Arr::CheckIndex(intptr_t index, intptr_t size) const
	{
		if (index < 0)
			index = -index;
		if (index < 0 || index >= size)
			throw RSRuntimeException{ "invalid index {} to array of size {}", index, size };
		return index;
	}

	void Arr::IndexSet(ExecutionContext& vm, Value key, Value val)
	{
		auto index = CheckIndexOOB(vm, key, Values.size(), "Array");
		Values[index] = val;
	}

	Value Arr::IndexGet(ExecutionContext& vm, Value key) const
	{
		auto index = CheckIndexOOB(vm, key, Values.size(), "Array");
		return Values[index];
	}

	void Arr::MarkMembers()
	{
		for (auto& v : Values)
			v.Mark();
	}

	Declaration::Declaration(Namespace const* pn, SourcePos const& pos, Sym* name, uint32_t flags) : GCBase(flags | DeclarationFlags), Name(name), mParentNamespace(pn), mDeclPos(pos) {}
	Declaration::Declaration(Namespace const* pn, Sym* name, uint32_t flags) : Declaration(pn, {}, name, flags) {}

	void Declaration::MarkMembers()
	{
	}

	Value Class::FindMemberUnlinked(Value name) const
	{
		if (auto field = Members->Values.find(name); field != Members->Values.end())
			return field->second;
		return Value::Invalid();
	}

	Method* Class::FindMethod(Module const & mod, std::string_view name) const
	{
		if (!Is(ClassFlags::Linked))
			throw RSRuntimeException("trying to access members of an incomplete class '{}'", mFullyQualifiedName);

		auto sym = mod.FindSymbol(name);
		if (!sym) return nullptr;

		if (auto field = Members->Values.find(mod.MV(sym)); field != Members->Values.end())
		{
			if (field->second.IsValid() && field->second.Is(IC::Method))
				return field->second.Method;
		}
		return nullptr;
	}

	Value Class::PreConstruct(ExecutionContext& vm) const
	{
		if (mInternal != IC::UserClass)
			vm.ThrowRuntimeException("internal: trying to construct an object of non-user class '{}'", mFullyQualifiedName);

		return Value{ this, vm.Heap().NewSized<Obj>(-1 + mSlotCount * sizeof(Value), this) };
	}

	Value Class::FindMember(Value name) const
	{
		if (!Is(ClassFlags::Linked))
			throw RSRuntimeException("trying to access members of an incomplete class '{}'", mFullyQualifiedName);
		return FindMemberUnlinked(name);
	}

	Field const* Class::FindFieldBySlot(intptr_t slot) const
	{
		for (auto& [name, member] : Members->Values)
			if (member.Is(IC::Field) && member.Field->SlotID == slot)
				return member.Field;
		return nullptr;
	}

	void Module::Link(Class* klass)
	{
		/// If we're already linked, or we're an interface, return
		if (are_any_flags_set(klass->Flags, ClassFlags::Linked, ClassFlags::Interface))
			return;

		if (klass->mBaseClass != klass)
		{
			/// Make sure base class is linked
			Link(klass->mBaseClass);

			/// Check for shadowing
			for (auto& [sym, member] : klass->Members->Values)
			{
				if (auto it = klass->mBaseClass->Members->Values.find(sym); it != klass->mBaseClass->Members->Values.end())
				{
					auto our_member_type = member.Type->Internal();
					auto parent_member_type = it->second.Type->Internal();
					if (!(our_member_type == IC::Method && parent_member_type == IC::Method))
					{
						/// If we're declaring shadowing with a field or our method is shadowing a field
						ReportDeclError(member.Declaration->mDeclPos, "member '{}' declared at {} shadows base class member declared at {}",
							sym.Symbol->Data, member.Declaration->mDeclPos, it->second.Declaration->mDeclPos);
					}
				}
			}

			/// Adjust our own field indices
			auto parent_slot_count = klass->mBaseClass->mSlotCount;
			klass->mSlotCount += parent_slot_count;

			for (auto& [sym, member] : klass->Members->Values)
			{
				if (member.Is(IC::Field))
					member.Field->SlotID += parent_slot_count;
			}

			/// Insert parent members to ourselves
			for (auto& [sym, member] : klass->mBaseClass->Members->Values)
			{
				if (member.Is(IC::Method))
					klass->Members->Values.try_emplace(sym, member); /// We don't get method from parent if we have one with the same name
				else //if (member.Is(IC::Field))
					klass->Members->Values[sym] = member; /// We inherit fields and constants from parents
			}
		}

		/// Then, check to implement all declared interfaces
		for (auto [interface, _] : klass->mImplementsClasses->Values)
		{
			if (interface.Is(IC::Class) && interface.Class->Is(ClassFlags::Interface))
			{
				/// Link the interface class
				auto interface_class = interface.Class;
				Link(interface_class);

				/// Check if we meet interface requirements
				for (auto [name, pos] : interface_class->RequiresMembers)
				{
					auto _name = MV(name);
					if (interface_class->Members->Values.find(_name) != interface_class->Members->Values.end())
						continue;

					if (!klass->FindMemberUnlinked(_name).IsValid())
					{
						ReportDeclError(klass, "class '{}' does not meet requirements of interface '{}': member '{}' is required here: {}",
							klass->FullyQualifiedName(), interface_class->FullyQualifiedName(), name->Data, pos);
					}
				}

				/// Check if the interface class method shadows any fields,
				/// if not, add it to ourselves
				for (auto [name, member] : interface_class->Members->Values)
				{
					if (!member.Is(IC::Method))
						continue; /// Should not happen?

					auto existing_member = klass->FindMemberUnlinked(name);
					if (!existing_member.IsValid())
					{
						klass->Members->Values.try_emplace(name, member);

						/// We don't have it, so add an instantiation
						auto interface_method = member.Method;
						mCurrentMethod = mHeap.New<Method>(*interface_method);
						mCurrentMethod->mFullyQualifiedName = klass->mFullyQualifiedName + "." + name.Symbol->Data;
						mCurrentMethod->ParentClass = klass;
						klass->Members->Values.insert_or_assign(name, Value{ MethodClass, mCurrentMethod });
						continue;
					}

					/// If shadows field or const
					if (existing_member.Is(IC::Field))
					{
						ReportDeclError(klass, "interface method '{}' would shadow the '{}' field of this class", member.Method->FullyQualifiedName(), existing_member.Field->FullyQualifiedName());
					}
					else if (existing_member.Is(IC::Constant))
					{
						ReportDeclError(klass, "interface method '{}' would shadow the '{}' constant of this class", member.Method->FullyQualifiedName(), existing_member.Constant->FullyQualifiedName());
					}
					/// If already implements method
					else if (existing_member.Is(IC::Method))
					{
						auto existing_method = existing_member.Method;
						auto interface_method = member.Method;

						/// Check for same (or greater) number of arguments
						if (existing_method->ParamCount == interface_method->ParamCount)
							continue; /// Do nothing since we have a similar method with this name already

						/// We have a eponymous method but the number of parameters is different, report this to use
						if (existing_method->ParentClass != klass)
						{
							/// If existing method is from another interface
							ReportWarningPos(ReportModule::Declaration, klass->mDeclPos,
								"interface method '{}' (declared here: {}) has a different number of parameters ({}) than interface method '' ({} arguments, declared here: {})",
								existing_method->FullyQualifiedName(), existing_method->mDeclPos, existing_method->ParamCount, interface_method->FullyQualifiedName(),
								interface_method->ParamCount, interface_method->mDeclPos);
						}
						else
						{
							/// If existing method is from ourselves
							ReportWarningPos(ReportModule::Declaration, klass->mDeclPos,
								"method '{}' (declared here: {}) has a different number of parameters ({}) than interface method '' ({} arguments, declared here: {})",
								existing_method->FullyQualifiedName(), existing_method->mDeclPos, existing_method->ParamCount, interface_method->FullyQualifiedName(),
								interface_method->ParamCount, interface_method->mDeclPos);
						}

					}
				}
			}
			else if (!interface.Is(IC::Class) || interface.Class != klass->mBaseClass)
				ReportDeclError(klass, "internal error - {} left in interface list", interface.ToString());
		}

		if (klass->mBaseClass != klass)
		{
			/// This class "implements" all its base classes
			auto base = klass->mBaseClass;
			do
				klass->mImplementsClasses->Values.try_emplace(MV(base), True);
			while (base->mBaseClass != base && (base = base->mBaseClass));
		}

		/// Link constructor
		if (klass->mConstructor)
		{
			StatTree init_fields{ klass->mConstructor->mDeclPos };
			init_fields.SetConstructorInitializer(klass->mSlotCount);
			for (auto& [sym, member] : klass->Members->Values)
			{
				if (member.Is(IC::Field))
				{
					auto field = member.Field;
					StatTree init_field{ field->mDeclPos };
					init_field.SetInitField(std::move(field->InitExp), field->SlotID);
					init_fields.Statements.push_back(std::move(init_field));
				}
			}
			klass->mConstructor->Statements->at(0) = std::move(init_fields);

			mThisConstructed = false;
			Link(klass->mConstructor);
			klass->mConstructor->Code.push_back(Instruction{ I::Return, 0, 0 }); /// return this
			mThisConstructed = true;
		}

		/// Link methods
		try
		{
			for (auto& [sym, member] : klass->Members->Values)
			{
				if (member.Is(IC::Method))
				{
					auto method = member.Method;
					if (method->ParentClass == klass) /// Only link our methods, other methods are linked
						Link(method);
				}
			}
		}
		catch (RSException& e)
		{
			e.SubLines.push_back(fmt::format("while creating class '{}' here: {}", klass->FullyQualifiedName(), klass->mDeclPos));
			throw;
		}

		/// We're linked!
		klass->Set(ClassFlags::Linked);
	}

	Class::Class(Namespace* pn, IC internal, Sym* name, uint32_t flags, Map* members, Method* constructor)
		: Declaration(pn, name, flags), mInternal(internal), Members(members), mConstructor(constructor)
	{
	}

	Class::Class(Namespace* pn, SourcePos const& pos, Sym* name, uint32_t flags)
		: Declaration(pn, pos, name, flags)
	{
	}

	std::string Class::ToString() const
	{
		return fmt::format("class {}", mFullyQualifiedName);
	}

	Namespace::Namespace(Namespace* pn, Sym* name)
		: Declaration(pn, name)
	{
	}

	Value Namespace::FindNamespaceMember(Value name_or_array) const
	{
		auto current = this;
		do
		{
			auto val = current->FindNamespaceMemberSelfOnly(name_or_array);
			if (val.IsValid())
				return val;
		} while ((current = current->mParentNamespace));
		return Value::Invalid();
	}

	Value Namespace::FindNamespaceMemberSelfOnly(Value name_or_array, size_t start) const
	{
		if (name_or_array.Is(IC::Symbol))
			return FindNamespaceMemberSelfOnly(name_or_array.Symbol);

		auto arr = name_or_array.Array;
		if (auto it = Members.find(arr->Values[start++].Symbol); it != Members.end())
		{
			if (arr->Values.size() == start && it->second.Is(IC::Class))
				return it->second;
			else if (arr->Values.size() > start && it->second.Is(IC::Namespace))
				return it->second.Namespace->FindNamespaceMemberSelfOnly(name_or_array, start);
		}
		return Value::Invalid();
	}

	Value Namespace::FindNamespaceMemberSelfOnly(Sym const* name) const
	{
		if (auto it = Members.find(name); it != Members.end())
			return it->second;
		return Value::Invalid();
	}

	Namespace const* Namespace::GlobalNamespace() const
	{
		if (mParentNamespace) return mParentNamespace->GlobalNamespace();
		return this;
	}

	std::string Namespace::ToString() const
	{
		return fmt::format("namespace {}", mFullyQualifiedName);
	}

	Field::Field(Class* parent_class, SourcePos const& declpos, Sym* name)
		: Declaration(parent_class->ParentNamespace(), declpos, name), ParentClass(parent_class)
	{
	}

	std::string Field::ToString() const
	{
		return "var";
	}

	Constant::Constant(Class* parent_class, SourcePos const& declpos, Sym* name)
		: Declaration(parent_class->ParentNamespace(), declpos, name), ParentClass(parent_class)
	{
	}

	std::string Instruction::ToString() const
	{
		switch (OpCode)
		{
		case I::GetMember:
		case I::GetThisMember:
		case I::PushConstant: return fmt::format("{} {}", Is[(int)OpCode], Value{ (Class const*)Param0, Param1 }.ToString());
		case I::New: return fmt::format("New {}", ((Class const*)Param1)->FullyQualifiedName());
		case I::UnOp:
		case I::BinOp: return fmt::format("{} {}", Is[(int)OpCode], Ops[Param0].Name);
		default: return fmt::format("{} {} {}", Is[(int)OpCode], Param0, Param1);
		}
	}

	Str::StringEncoding RequiredEncoding(char32_t cp)
	{
		if (cp <= 0xFF)
			return Str::Latin1;
		else if (U_IS_BMP(cp))
			return Str::UBMP;
		else
			return Str::UTF32;
	}

	char32_t Str::EnsureValidCodepoint(Value val)
	{
		auto our_encoding = Encoding();
		auto requires_encoding = our_encoding;

		if (!(val.Is(IC::Codepoint) || val.Is(IC::Integer)))
			throw RSRuntimeException(fmt::format("value of type '{}' is not a valid string codepoint", val.RealType()->FullyQualifiedName()));

		char32_t cp = val.CP;
		if (!U_IS_UNICODE_CHAR(cp))
			throw RSRuntimeException(fmt::format("codepoint #{} is not a valid string codepoint", (uint32_t)cp));

		requires_encoding = RequiredEncoding(cp);

		if (requires_encoding > our_encoding)
			SwitchToEncoding(requires_encoding);

		return cp;
	}

	void Str::IndexSet(ExecutionContext& vm, Value key, Value val)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant string" };

		const auto index = CheckIndexOOB(vm, key, Length, "String");
		const auto cp = EnsureValidCodepoint(val);

		switch (Encoding())
		{
		case Latin1: Latin1View()[index] = (uint8_t)cp; return;
		case UBMP: UBMPView()[index] = (char16_t)cp; return;
		case UTF32: UTF32View()[index] = cp; return;
		}
	}

	Value Str::IndexGet(ExecutionContext& vm, Value key) const
	{
		const auto index = CheckIndexOOB(vm, key, Length, "String");
		return vm.Mod.MV(GetCP(index));
	}

	Value Str::Push(Value val)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant string" };

		const auto cp = EnsureValidCodepoint(val);
		const auto enc = Encoding();

		Bytes.resize(Bytes.size() + ByteWidth[enc]);
		switch (enc)
		{
		case Latin1: Latin1View()[Length++] = (uint8_t)cp; break;
		case UBMP: UBMPView()[Length++] = (char16_t)cp; break;
		case UTF32: UTF32View()[Length++] = cp; break;
		}
		return val;
	}

	Value Str::Insert(intptr_t at, Value val)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant string" };
		throw RSRuntimeException{ "unimplemented" };
	}

	void Str::Resize(intptr_t s, Value v)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant string" };

		if (s < 0)
			s = std::max(0LL, Length + s);
		if (s < Length)
		{
			Bytes.resize(s);
			Length = s;
		}
		else if (s > Length)
		{
			const auto cp = EnsureValidCodepoint(v);
			auto dif = s - Length;
			const auto enc = Encoding();

			if (cp == 0)
			{
				Length = s;
				Bytes.resize(s * ByteWidth[enc], 0);
				return;
			}

			Bytes.resize(s * ByteWidth[enc]);
			switch (enc)
			{
			case Latin1: std::fill_n(Latin1View() + Length, dif, (uint8_t)cp); break;
			case UBMP: std::fill_n(UBMPView() + Length, dif, (char16_t)cp); break;
			case UTF32: std::fill_n(UTF32View() + Length, dif, cp); break;
			}
			Length = s;
		}
	}

	Value Str::Erase(intptr_t s)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant string" };

		throw RSRuntimeException{ "unimplemented" };
	}

	Value Str::Pop(ExecutionContext const& vm)
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant string" };

		if (Bytes.empty())
			return vm.Null;

		const auto enc = Encoding();

		--Length;
		char32_t cp = 0;
		switch (enc)
		{
		case Latin1: cp = Latin1View()[Length]; break;
		case UBMP: cp = UBMPView()[Length]; break;
		case UTF32: cp = UTF32View()[Length]; break;
		}
		Bytes.resize(Bytes.size() - ByteWidth[enc]);
		return vm.Mod.MV(cp);
	}

	void Str::Clear()
	{
		if (Is(BaseObjectFlags::Const))
			throw RSRuntimeException{ "trying to modify constant string" };

		Bytes.clear();
		Length = 0;
		SetEncoding(Latin1);
	}

	std::string Str::ToString() const
	{
		return fmt::format("string of length {}", Length);
	}

	char32_t Str::GetCP(intptr_t index) const
	{
		switch (Encoding())
		{
		case Latin1: return (char32_t)Latin1View()[index];
		case UBMP: return (char32_t)UBMPView()[index];
		case UTF32: return UTF32View()[index];
		}
		return 0;
	}

	void Str::SetEncoding(StringEncoding enc) noexcept
	{
		auto fl = Flags;
		fl &= ~(3ULL << StartCustomFlags);
		fl |= ((uint32_t(enc) & 3ULL) << StartCustomFlags);
		Flags = fl;
	}

	template <typename TYPE_TO, typename VIEW_FROM>
	std::vector<uint8_t> Transcode(VIEW_FROM const& from)
	{
		static_assert(sizeof(typename VIEW_FROM::element_type) <= sizeof(TYPE_TO));
		static_assert(!std::is_same_v<char8_t, typename VIEW_FROM::element_type>);
		std::vector<uint8_t> result;
		result.resize(from.size() * sizeof(TYPE_TO));
		auto udata = (TYPE_TO*)result.data();
		for (auto& cp : from)
			*udata++ = (TYPE_TO)cp;
		return result;
	}

	Str::StringEncoding Str::SwitchToEncoding(StringEncoding requested_encoding)
	{
		auto our_encoding = Encoding();
		if (requested_encoding <= our_encoding)
			return our_encoding;

		switch (our_encoding)
		{
		case Latin1:
			switch (requested_encoding)
			{
			case UBMP: Bytes = Transcode<char16_t>(std::span{ Latin1View(),(size_t)Length }); goto end;
			case UTF32: Bytes = Transcode<char32_t>(std::span{ Latin1View(), (size_t)Length }); goto end;
			}
		case UBMP:
			Bytes = Transcode<char16_t>(std::span{ UBMPView(), (size_t)Length });
			goto end;
		}

	end:
		SetEncoding(requested_encoding);
		return requested_encoding;
	}
}
/*
char32_t Str::GetUTF8At(intptr_t index) const
{
	auto it = Bytes.data();
	utf8::unchecked::advance(it, index);
	return utf8::unchecked::peek_next(it);
}

void Str::SetUTF8At(intptr_t index, char32_t cp)
{
	auto it = Bytes.data();
	utf8::unchecked::advance(it, index);
	const auto byte_index = it - Bytes.data();
	const auto current_cp_len = U8_COUNT_TRAIL_BYTES_UNSAFE(*it) + 1;
	const auto new_cp_len = U8_LENGTH(cp);
	if (current_cp_len > new_cp_len)
		Bytes.erase(Bytes.begin() + byte_index, Bytes.begin() + byte_index + (current_cp_len - new_cp_len));
	else if (current_cp_len < new_cp_len)
		Bytes.insert(Bytes.begin() + byte_index, new_cp_len - current_cp_len, 0);
	utf8::unchecked::append(cp, it);
}
*/
