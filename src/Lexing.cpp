#include "../include/rsl/RSL.h"
#include "Private.h"
#include <charconv>
#include <system_error>

namespace rsl
{
	enum class CC
	{
		UTF8Invalid,
		UTF8Cb,
		UTF8P2,
		UTF8P3,
		UTF8P4,
		NewLine,
		Whitespace,
		Letter,
		Number,
		Quote,
		Operator,
		Minus,
		Hash,
		Backslash,
		Invalid,
	};

	static constexpr CC CharClass[256] = {
		CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid,
		CC::Invalid, CC::Whitespace, CC::NewLine, CC::Invalid, CC::Invalid, CC::Whitespace, CC::Invalid, CC::Invalid,
		CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid,
		CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid, CC::Invalid,
		/// 32
		CC::Whitespace, CC::Operator, CC::Quote, CC::Hash, CC::Invalid, CC::Operator, CC::Operator, CC::Quote,
		/// 40
		CC::Operator, CC::Operator, CC::Operator, CC::Operator, CC::Operator, CC::Minus, CC::Operator, CC::Operator,
		/// 48, numbers
		CC::Number, CC::Number, CC::Number, CC::Number, CC::Number, CC::Number, CC::Number, CC::Number,
		/// 56, numbers and operators
		CC::Number, CC::Number, CC::Operator, CC::NewLine, CC::Operator, CC::Operator, CC::Operator, CC::Operator,
		/// 64, letters
		CC::Operator, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter,
		CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter,
		CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter,
		/// 88
		CC::Letter, CC::Letter, CC::Letter, CC::Operator, CC::Backslash, CC::Operator, CC::Operator, CC::Letter, /// Actually underscore
		/// 96, small letters
		CC::Invalid, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter,
		CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter,
		CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter, CC::Letter,
		/// 120
		CC::Letter, CC::Letter, CC::Letter, CC::Operator, CC::Operator, CC::Operator, CC::Operator, CC::Invalid,

		/// UTF-8
		/// Continuation bytes
		CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb,
		CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb,
		CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb,
		CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb,
		CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb,
		CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb,
		CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb,
		CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb, CC::UTF8Cb,

		/// Prefix 2
		CC::UTF8Invalid, CC::UTF8Invalid, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2,
		CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2,

		CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2,
		CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2, CC::UTF8P2,

		/// Prefix 3
		CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3,
		CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3, CC::UTF8P3,

		/// Prefix 4 and invalids
		CC::UTF8P4, CC::UTF8P4, CC::UTF8P4, CC::UTF8P4, CC::UTF8P4, CC::UTF8Invalid, CC::UTF8Invalid, CC::UTF8Invalid,
		CC::UTF8Invalid, CC::UTF8Invalid, CC::UTF8Invalid, CC::UTF8Invalid, CC::UTF8Invalid, CC::UTF8Invalid, CC::UTF8Invalid, CC::UTF8Invalid
	};

	void Module::EatNl()
	{
		mCurPos.Line++; mCurPos.Column = 0; mCurLex++;
		mStartLine = mCurLex;
	}

	inline std::string CharToStr(intptr_t ch)
	{
		return std::isprint(uint8_t(ch)) ? fmt::format("{} ('{}')", uint8_t(ch), std::string(1, (char)ch)) : std::to_string(uint8_t(ch));
	}

	void Module::Advance(bool start_exp)
	{
		while (mCurLex < mLexEnd)
		{
			const auto ch = *mCurLex;
			switch (CharClass[(uint8_t)ch])
			{
			case CC::Hash:
				do mCurLex++; while (mCurLex < mLexEnd && (*mCurLex != '\n'));
				if (mCurLex == mLexEnd) goto eof;
			case CC::NewLine:
				mCurPos.Column = mCurLex - mStartLine;
				mCurrentToken = { make_sv(mCurLex, mCurLex + 1), TokType::NewLine };
				EatNl();
				return;
			case CC::Whitespace: mCurLex++; continue;
			case CC::Letter:
				/// TODO: u_isIDStart
				LexID(); return;
			case CC::Minus:
				if (!start_exp/* && CharClass[(uint8_t)mCurLex[1]] != CC::Number*/)
					LexOp();
				else
			case CC::Number: LexNum(); return;
			case CC::Quote: LexSym(); return;
			case CC::Invalid:
			case CC::UTF8Invalid:
			case CC::UTF8Cb:
			case CC::UTF8P2:
			case CC::UTF8P3:
			case CC::UTF8P4:
			case CC::Backslash: ThrowLexException("invalid character #{}", CharToStr(ch));
			default: LexOp(); return;
			}
		}
	eof:
		mCurrentToken = { make_sv(mCurLex, mCurLex), TokType::Eof };
		mCurPos.Column = mCurLex - mStartLine;
	}

	void Module::LexID()
	{
		mCurPos.Column = mCurLex - mStartLine;
		const auto start_id = mCurLex;
		while (mCurLex < mLexEnd)
		{
			const auto ch = *mCurLex;
			/// TODO: u_isIDPart
			switch (CharClass[(uint8_t)ch])
			{
			case CC::Number:
			case CC::Letter:
				mCurLex++;
				continue;
			case CC::UTF8P2: mCurLex += 2; continue;
			case CC::UTF8P3: mCurLex += 3; continue;
			case CC::UTF8P4: mCurLex += 4; continue;
			default:
				goto done;
			}
		}
	done:
		auto const sv = make_sv(start_id, mCurLex);
		mCurrentToken = { sv, TokType::Identifier, MV(MakeSym(sv)) };
		if (auto kw = static_cast<Sym*>(mCurrentToken.ParsedVal.GCVal)->Kw)
		{
			mCurrentToken.Type = TokType::Keyword;
			mCurrentToken.ParsedVal = MV(intptr_t(kw));
		}
		else if (auto op = static_cast<Sym*>(mCurrentToken.ParsedVal.GCVal)->Op)
		{
			mCurrentToken.Type = TokType::Operator;
			mCurrentToken.ParsedVal = MV(intptr_t(op));
		}
	}

	inline void utf8_encode(std::string& out, intptr_t utf)
	{
		if (utf <= 0x7F)
			out += (char)utf;
		else if (utf <= 0x07FF) {
			out += (char)(((utf >> 6) & 0x1F) | 0xC0);
			out += (char)(((utf >> 0) & 0x3F) | 0x80);
		}
		else if (utf <= 0xFFFF) {
			out += (char)(((utf >> 12) & 0x0F) | 0xE0);
			out += (char)(((utf >> 6) & 0x3F) | 0x80);
			out += (char)(((utf >> 0) & 0x3F) | 0x80);
		}
		else if (utf <= 0x10FFFF) {
			out += (char)(((utf >> 18) & 0x07) | 0xF0);
			out += (char)(((utf >> 12) & 0x3F) | 0x80);
			out += (char)(((utf >> 6) & 0x3F) | 0x80);
			out += (char)(((utf >> 0) & 0x3F) | 0x80);
		}
		else {
			out += (char)0xEF;
			out += (char)0xBF;
			out += (char)0xBD;
		}
	}

	void Module::LexSym()
	{
		std::string result_sym;
		const auto start_sym = mCurLex;
		mCurPos.Column = start_sym - mStartLine;
		const auto quote = *mCurLex++;
		while (mCurLex < mLexEnd)
		{
			auto ch = uint8_t(*mCurLex++);
			if (ch == quote)
				break;

			intptr_t cp = 0;
			int escape_len = 2;
			switch (CharClass[ch])
			{
			case CC::Backslash:
				switch (ch = uint8_t(*mCurLex++))
				{
				case '"':
				case '\'':
				case '/':
				case '\\': result_sym += ch; continue;
				case 'n': result_sym += '\n'; continue;
				case 'r': result_sym += '\r'; continue;
				case 't': result_sym += '\t'; continue;
				case '0': result_sym += '\0'; continue;
				case 'U':
					escape_len += 4;
				case 'u':
					escape_len += 2;
				case 'x':
					if (mCurLex + escape_len >= mLexEnd)
						ThrowLexException("encountered end of stream when parsing escape literal");
					for (int i = 0; i < escape_len; i++)
					{
						if (!std::isxdigit(ch = uint8_t(*mCurLex++)))
							ThrowLexException("non-hex digit found when parsing escape literal at position {}: #{}", i, CharToStr(ch));
						ch -= '0';
						cp = (cp << 8) | ((ch > 9) ? (ch - 65) : ch);
					}
					if (escape_len == 2)
						result_sym += (uint8_t)cp;
					else
						utf8_encode(result_sym, cp);
					continue;
				default:
					ThrowLexException("unrecognized string escape literal #{}", CharToStr(ch));
				}
				continue;
			case CC::NewLine:
				ThrowLexException("newline in string");
			case CC::Invalid:
				ThrowLexException("invalid string character #{}", CharToStr(ch));
			default: result_sym += ch; continue;
			}
		}
		if (mCurLex == mLexEnd && mCurLex[-1] != quote)
			ThrowLexException("encountered end of stream while reading string literal");
		auto const sv = make_sv(start_sym, mCurLex);
		mCurrentToken = { sv, TokType::Symbol, MV(MakeSym(result_sym)) };
	}

	void Module::LexNum()
	{
		mCurPos.Column = mCurLex - mStartLine;
		intptr_t value = 0;
		auto result = std::from_chars(mCurLex, mLexEnd, value);
		if (result.ec != std::errc{})
			ThrowLexException("error while parsing integer number: {}", std::make_error_condition(result.ec).message());
		else if (result.ptr < mLexEnd && (*result.ptr == '.' || std::tolower(*result.ptr) == 'e'))
		{
			double Floatval = 0;
			result = std::from_chars(mCurLex, mLexEnd, Floatval);
			if (result.ec != std::errc{})
				ThrowLexException("error while parsing floating-point number: {}", std::make_error_condition(result.ec).message());
			mCurrentToken = { make_sv(mCurLex, result.ptr), TokType::Float, MV(Floatval) };
		}
		else
		{
			mCurrentToken = { make_sv(mCurLex, result.ptr), TokType::Integer, MV(value) };
		}
		switch (CharClass[uint8_t(*result.ptr)])
		{
		case CC::Number:
		case CC::Letter:
		case CC::Quote:
			ThrowLexException("invalid number suffix #{} ('{}')", uint8_t(*result.ptr), std::string(1, *result.ptr));
		default:
			break;
		}
		mCurLex = result.ptr;
	}

	void Module::LexOp()
	{
		const auto start = mCurLex;
		mCurPos.Column = start - mStartLine;
		OpType op = OpType::Unknown;
		switch (*mCurLex++)
		{
		case '!':
			if (mCurLex < mLexEnd && *mCurLex == '=')
			{
				if (++mCurLex < mLexEnd && *mCurLex == '=')
				{
					op = OpType::NotStrictEqual;
					++mCurLex;
				}
				else
					op = OpType::NotEqual;
			}
			else
				op = OpType::Not;
			break;

		case '~': op = OpType::BinNot; break;
		case '.':
			if (mCurLex < mLexEnd && *mCurLex == '.')
			{
				if (++mCurLex < mLexEnd && *mCurLex == '.')
				{
					op = OpType::DotDotDot;
					++mCurLex;
				}
				else
					op = OpType::DotDot;
			}
			else
				op = OpType::Dot;
			break;
		case '*': op = OpType::Star; break;
		case '%': op = OpType::Percent; break;
		case '/': op = OpType::Slash; break;
		case '+': op = OpType::Plus; break;
		case '-': op = OpType::Minus; break;

		case '<':
			if (mCurLex < mLexEnd)
			{
				switch (*mCurLex)
				{
				case '<': op = OpType::Shl; mCurLex++; goto end;
				case '=':
					if (++mCurLex < mLexEnd && *mCurLex == '>')
					{
						op = OpType::Spaceship;
						mCurLex++;
					}
					else
						op = OpType::LessEq;
					goto end;
				default:
					break;
				}
			}
			op = OpType::Less;
			break;

		case '>':
			if (mCurLex < mLexEnd)
				switch (*mCurLex)
				{
				case '>': op = OpType::Shr; mCurLex++; goto end;
				case '=': op = OpType::GreaterEq; mCurLex++; goto end;
				}
			op = OpType::Greater;
			break;

		case '&':
			if (mCurLex < mLexEnd && *mCurLex == '&')
			{
				op = OpType::And;
				mCurLex++;
			}
			else
				op = OpType::BinAnd;
			break;

		case '^': op = OpType::BinXor; break;

		case '|':
			if (mCurLex < mLexEnd && *mCurLex == '|')
			{
				op = OpType::Or;
				mCurLex++;
			}
			else
				op = OpType::BinOr;
			break;

		case '=':
			if (mCurLex < mLexEnd && *mCurLex == '=')
			{
				if (++mCurLex < mLexEnd && *mCurLex == '=')
				{
					op = OpType::StrictEqual;
					++mCurLex;
				}
				else
					op = OpType::EqualEqual;
			}
			else
				op = OpType::Equal;
			break;

		case '?':
			if (mCurLex < mLexEnd && *mCurLex == '.')
			{
				op = OpType::QuestionDot;
				mCurLex++;
			}
			break;

		case ',': op = OpType::Comma; break;
		case ':': op = OpType::Colon; break;
		case '{': op = OpType::OpenBrace; break;
		case '(': op = OpType::OpenParen; break;
		case '[': op = OpType::OpenBracket; break;
		case ']': op = OpType::CloseBracket; break;
		case ')': op = OpType::CloseParen; break;
		case '}': op = OpType::CloseBrace; break;
		}
	end:
		mCurrentToken = { make_sv(start, mCurLex), TokType::Operator, MV(intptr_t(op)) };
	}

}