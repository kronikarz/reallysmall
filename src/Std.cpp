#include "Private.h"
#include "../include/rsl/ExecutionContext.h"
#include <unicode/uchar.h>

namespace rsl
{

	intptr_t CheckArgInt(ExecutionContext& vm, int num, const char* name)
	{
		auto val = vm[num].Value();
		if (!val.Is(IC::Integer))
			vm.ThrowRuntimeException("argument {} ({}) to function '{}' must be an integer, not a '{}'", num, name, vm.CurrentMethod()->FullyQualifiedName(), val.RealType()->FullyQualifiedName());
		return val.Int;
	}

	intptr_t CheckIndexOOB(ExecutionContext& vm, Value index, intptr_t length, const char* type);

	void Module::LoadStdLib()
	{
		/// //////////////////////////////// ///
		/// Null
		/// //////////////////////////////// ///

		AddNativeMethod(NullClass, "GetType", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV(_this.RealType()); });
		/// TODO: FormatTo(Output, ...options)
		/// TODO: WriteTo(Output, ...options)
		/// TODO: ToJSON([replacer[, space]])
		//AddNativeMethod(NullClass, "ToString", [](ExecutionContext& vm, intptr_t nargs) { return vm.Mod.MV(_this.Type->Name); });
		AddNativeMethod(NullClass, "GetHashCode", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV(intptr_t(_this.Type) ^ _this.Int); });
		AddNativeMethod(NullClass, "OrError", 1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			if (_this.IsFalse())
			{
				vm.ThrowRuntimeException("Error: {}", vm[1]->ToString());
				return vm.Null;
			}
			return _this;
		});


		/// static func TryRead(Input)
		/// static func TryParse(Input)

		/// //////////////////////////////// ///
		/// Array
		/// //////////////////////////////// ///

		AddNativeMethod(ArrayClass, "Push", -1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto arr = _this.Array;
			Value result = vm.Null;
			for (intptr_t i = 1; i <= nargs; i++)
				result = arr->Push(vm[i].Value());
			return result;
		});
		AddNativeMethod(ArrayClass, "Resize", -1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto arg = CheckArgInt(vm, 1, "initial_value");
			auto def = nargs > 1 ? vm[2].Value() : vm.Null;
			_this.Array->Resize(arg, def);
			return vm.Null;
		});
		AddNativeMethod(ArrayClass, "Erase", 1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return _this.Array->Erase(CheckArgInt(vm, 1, "position"));
		});
		AddNativeMethod(ArrayClass, "Size", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return vm.Mod.MV(intptr_t(_this.Array->Values.size()));
		});
		AddNativeMethod(ArrayClass, "Pop", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return _this.Array->Pop(vm);
		});
		AddNativeMethod(ArrayClass, "Clear", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			_this.Array->Clear();
			return vm.Null;
		});
		AddNativeMethod(ArrayClass, "IsEmpty", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return _this.Array->Values.empty() ? vm.True : vm.False;
		});
		AddNativeMethod(ArrayClass, "Append", -1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto arr = _this.Array;
			for (intptr_t i = 1; i <= nargs; i++)
			{
				vm.IterateOver(i, [arr](ValueRef const& k, ValueRef const& v) {
					arr->Push(v.Value());
					return false;
				});
			}
			return vm.Null;
		});
		AddNativeMethod(ArrayClass, "Insert", 2, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto arr = _this.Array;
			return arr->Insert(CheckArgInt(vm, 1, "position"), vm[2].Value());
		});
		/// Array.Reverse(this container) -> null
		/// Array.Reversed(this container) -> Array
		/// Array.Sort(this container) -> null
		/// Array.Sorted(this container) -> Array

		/// Interfaces - can have implementations of methods!

		/// //////////////////////////////// ///
		/// Interface Iterable
		/// //////////////////////////////// ///

		/// func Find(value) -> key
		/// func FindAnyOf(Array) -> key
		/// func FindLast(value) -> key
		/// func FindLastAnyOf(Array) -> key
		/// func EraseValue, EraseAllValues
		/// func Contains(value) -> Bool
		/// func HasKey(key) -> Bool
		/// func Fill(value) -> value
		/// func OnlyTrue() -> new container
		/// func OnlyNonNull() -> new container
		/// func Count(value) -> Int
		/// func Keys() -> Array
		/// func Values() -> Array
		/// func Join(separator) -> String
		/// func Replace(a, b) -> Array
		/// func FindBy(field_name, field_value) -> key

		/// Contiguous
		/// func SubRange(a, b) -> Array

		/// //////////////////////////////// ///
		/// Interface StringLike
		/// //////////////////////////////// ///

		/// is Iterable
		/// is Contiguous
		/// func StartsWith(StringLike) ->bool
		/// func EndsWith(StringLike) -> bool
		/// func Codepoints() -> Enumerator
		/// func FindSubstring(StringLike) -> bool
		/// func FindLastSubstring(StringLike) -> bool
		/// func Split(StringLike) -> Array
		/// func ToString() -> String
		/// func Repeat(n), Pad(n[, padstr])
		/// func Substring, TrimLeft, TrimRight, Trim

		/// //////////////////////////////// ///
		/// Symbol
		/// //////////////////////////////// ///

		/// is Iterable
		/// func GetHashCode()
		/// func Size()

		/// //////////////////////////////// ///
		/// Interface Number
		/// //////////////////////////////// ///

		/// func Between(a, b)
		/// func Round(), Floor(), Ceil(), Fract(), Abs()
		/// func Lerp(min, max)
		/// func Step(edge) - 0 if this < edge, else 1
		/// func SmoothStep(edge1, edge2)
		/// func Sin(), Cos(), Tan(), Asin(), Acos(), Atan(), Atan2(y)
		/// func Exp(), Log(base=null), Log10(), Log2()
		/// func Max(y), Min(y), Clamp(min, max), Saturated()
		/// func Pow(power), Sign(), Sqrt(), InvSqrt()
		/// func ToRad(), ToDeg()
		/// func ToInt(), ToFloat()
		/// func IsNan(), IsInf()
		/// static func Max(), Min()

		/// //////////////////////////////// ///
		/// Integer
		/// //////////////////////////////// ///

		/// func LeadingZeroes(), CountOnes()

		/// //////////////////////////////// ///
		/// CodePoint? Would be subclass of integer...
		/// //////////////////////////////// ///

		/// This means we embed ICU database,
		/// RSL_FULL_UNICODE_SUPPORT - unicode support for identifiers
		/// RSL_STRING_UNICODE_SUPPORT - unicode support for codepoints
		/// https://unicode-org.github.io/icu-docs/apidoc/released/icu4c/uchar_8h.html
		/// func IsAlpha() -> bool
		/// func IsAlphaNum() -> bool
		/// ... others from that stuff
		/// isalnum isalpha islower isupper isdigit isxdigit isblank 
		/// iscntrl isgraph isspace isprint ispunct tolower toupper
		/// func NextValid(), PrevValid()

		AddNativeMethod(CodepointClass, "opEqualEqual", 1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto i = CheckArgInt(vm, 1, "codepoint_number");
			auto cp = _this.CP;
			return (i == cp) ? vm.True : vm.False;
		});

		AddNativeMethod(CodepointClass, "IsAlphabetic", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isUAlphabetic(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsLowercase", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isULowercase(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsUppercase", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isUUppercase(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsWhiteSpace", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isUWhiteSpace(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsLower", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_islower(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsUpper", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isupper(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsTitle", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_istitle(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsDigit", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isdigit(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsAlpha", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isalpha(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsAlNum", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isalnum(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsHexDigit", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isxdigit(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsPunct", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_ispunct(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsGraph", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isgraph(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsBlank", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isblank(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsSpace", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isspace(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsCntrl", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_iscntrl(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsControlCode", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isISOControl(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsPrint", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isprint(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "IsMirrored", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return u_isMirrored(_this.CP) ? vm.True : vm.False; });
		AddNativeMethod(CodepointClass, "Mirrored", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV((char32_t)u_charMirror(_this.CP)); });
		AddNativeMethod(CodepointClass, "PairedBracket", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV((char32_t)u_getBidiPairedBracket(_this.CP)); });

		AddNativeMethod(CodepointClass, "ToLower", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV((char32_t)u_tolower(_this.CP)); });
		AddNativeMethod(CodepointClass, "ToUpper", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV((char32_t)u_toupper(_this.CP)); });
		AddNativeMethod(CodepointClass, "Digit", 1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return vm.Mod.MV((intptr_t)u_digit(_this.CP, (int8_t)CheckArgInt(vm, 1, "radix")));
		});
		//AddNativeMethod(CodepointClass, "ToLower", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV((char32_t)u_tolower(_this.CP)); });
		//AddNativeMethod(CodepointClass, "ToLower", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV((char32_t)u_tolower(_this.CP)); });

		/// TODO: u_charDirection, u_charType, ublock_getCode, ... (when we have enums)
		/// TODO: u_charName (unfortunately as string)
		/// TODO: static u_charFromName (maybe?)
		/// TODO: static u_forDigit (digit -> codepoint) "FromDigit"

		AddNativeMethod(CodepointClass, "ToInt", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) { return vm.Mod.MV(intptr_t(_this.CP)); });
		AddNativeMethod(CodepointClass, "NumericValue", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto num = u_getNumericValue(_this.CP);
			/// WTF WHO WROTE THIS CRAP, U_NO_NUMERIC_VALUE is -123456789 ?!
			if (num == U_NO_NUMERIC_VALUE)
				return vm.Null; //num = std::numeric_limits<double>::quiet_NaN();
			return vm.Mod.MV(num);
		});
		AddNativeMethod(CodepointClass, "DigitValue", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return vm.Mod.MV((intptr_t)u_charDigitValue(_this.CP));
		});

		/// //////////////////////////////// ///
		/// Float
		/// //////////////////////////////// ///

		/// func Bits() - returns bits of float
		/// static func Nan(), Inf(), NegInf(), e(), pi(), epsilon()

		/// //////////////////////////////// ///
		/// Boolean
		/// //////////////////////////////// ///

		/// //////////////////////////////// ///
		/// Map
		/// //////////////////////////////// ///

		/// //////////////////////////////// ///
		/// Class
		/// //////////////////////////////// ///

		/// //////////////////////////////// ///
		/// Field
		/// //////////////////////////////// ///

		/// //////////////////////////////// ///
		/// Method
		/// //////////////////////////////// ///

		/// //////////////////////////////// ///
		/// Namespace
		/// //////////////////////////////// ///

		/// //////////////////////////////// ///
		/// String
		/// //////////////////////////////// ///

		AddNativeMethod(StringClass, "Push", -1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto arr = _this.String;
			Value result = vm.Null;
			for (intptr_t i = 1; i <= nargs; i++)
				result = arr->Push(vm[i].Value());
			return result;
		});
		AddNativeMethod(StringClass, "Resize", -1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto arg = CheckArgInt(vm, 1, "initial_value");
			auto def = nargs > 1 ? vm[2].Value() : vm.Mod.ZeroInt;
			_this.String->Resize(arg, def);
			return vm.Null;
		});
		AddNativeMethod(StringClass, "Erase", 1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return _this.String->Erase(CheckArgInt(vm, 1, "position"));
		});
		AddNativeMethod(StringClass, "Size", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return vm.Mod.MV(_this.String->Length);
		});
		AddNativeMethod(StringClass, "Pop", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return _this.String->Pop(vm);
		});
		AddNativeMethod(StringClass, "Clear", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			_this.String->Clear();
			return vm.Null;
		});
		AddNativeMethod(StringClass, "IsEmpty", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return _this.String->Length ? vm.True : vm.False;
		});
		AddNativeMethod(StringClass, "Append", -1, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			auto str = _this.String;
			for (intptr_t i = 1; i <= nargs; i++)
			{
				vm.IterateOver(i, [str](const ValueRef& k, const ValueRef& v) {
					str->Push(v.Value());
					return false;
				});
			}
			return vm.Null;
		});
		AddNativeMethod(StringClass, "Insert", 2, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return _this.String->Insert(CheckArgInt(vm, 1, "position"), vm[2].Value());
		});

		AddNativeMethod(StringClass, "FindSymbol", 0, [](ExecutionContext& vm, Value _this, intptr_t nargs) {
			return vm.Null;/// TODO
		});

		/// //////////////////////////////// ///
		/// Random
		/// //////////////////////////////// ///

		/// //////////////////////////////// ///
		/// Interface Output
		/// //////////////////////////////// ///

		/// //////////////////////////////// ///
		/// Interface Input
		/// //////////////////////////////// ///

		/// FFI
		/// Sqlite, SqliteTable, etc.

	}

}