#include "../include/rsl/RSL.h"
#include "Private.h"
#include "../include/rsl/ExecutionContext.h"
#include "../include/rsl/CppInterop.h"

namespace rsl
{

	const TokProps Toks[(int)TokType::Count] = {
	#define TOKEN(a) { #a },
	#include "../include/rsl/Tokens.h"
	#undef TOKEN
	};

	const TokProps Ops[(int)OpType::Count] = {
	#define OPERATOR(x, ...) { #x, __VA_ARGS__ },
	#include "../include/rsl/Operators.h"
	#undef OPERATOR
	};

	const TokProps Kws[(int)KwType::Count] = {
	#define KEYWORD(k) { #k },
	#include "../include/rsl/Keywords.h"
	#undef KEYWORD
	};

	Module::Module(rsl::OSInterface& alloc)
		: mOSI(alloc)
		, mGlobalNamespace(mHeap.New<Namespace>(nullptr, nullptr))
	{
		/// Create global namespace
		mGlobalNamespace->Name = MakeSym("");
		mGlobalNamespace->Set(BaseObjectFlags::Const);
		mCurrentNamespace = mGlobalNamespace;

		/// Add native classes
		symNull = MakeSym("null");
		NullClass = mHeap.New<InternalClass>(mGlobalNamespace, IC::Null, MakeSym("Null"), flag_bits<uint32_t>(ClassFlags::Internal, ClassFlags::NotGC));
		NullClass->mFullyQualifiedName = ".Null";
		NullClass->mBaseClass = NullClass;
		NullClass->mImplementsClasses = mHeap.New<Map>(true);
		Null = Value{ NullClass, nullptr };

#define INTERNALCLASS(name, ...) \
	name##Class = mHeap.New<InternalClass>(mGlobalNamespace, IC::name, MakeSym(#name), flag_bits<uint32_t>(ClassFlags::Internal, ClassFlags::Final, __VA_ARGS__)); \
	name##Class->mBaseClass = NullClass; \
	name##Class->mImplementsClasses = mHeap.New<Map>(true);
#include "../include/rsl/InternalClasses.h"
#undef INTERNALCLASS

		/// MV IS ONLY VALID AFTER HERE

		Add(NullClass);
#define INTERNALCLASS(name, ...) Add(name##Class);
#include "../include/rsl/InternalClasses.h"
#undef INTERNALCLASS

		NullClass->mBaseClassName = MV(MakeSym("Null"));

		/// Create keyword symbols
		for (uint8_t i = 0; i < (uint8_t)KwType::Count; i++)
		{
			std::string name = Kws[i].Name; name[0] = tolower(name[0]);
			auto sym = MakeSym(name);
			sym->Kw = i;
		}

		/// Create operator symbols
		for (uint8_t i = 0; i < (uint8_t)OpType::Count; i++)
		{
			std::string name = fmt::format("op{}", Ops[i].Name);
			auto sym = MakeSym(name);
			sym->Op = i;
			OperatorNameSymbols[i] = MV(sym);
		}

		/// Mark some keyword symbols as operators
		MakeSym("is")->Op = (uint8_t)OpType::Is;
		MakeSym("as")->Op = (uint8_t)OpType::As;

		MakeSym("and")->Op = (uint8_t)OpType::And;
		MakeSym("or")->Op = (uint8_t)OpType::Or;
		MakeSym("not")->Op = (uint8_t)OpType::Not;

		/// Create common symbols and values
		symNew = MakeSym("new");
		symSpecialVarEnd = MakeSym("@end");
		symCall = MV(MakeSym("Call"));

		True = Value{ BooleanClass, intptr_t{1} };
		False = Value{ BooleanClass, intptr_t{0} };

		ZeroFloat = Value{ FloatClass, double{0} };
		ZeroInt = Value{ IntegerClass, intptr_t{0} };
		ZeroCodepoint = Value{ CodepointClass, intptr_t{} };
		ZeroPoint = Value{ PointClass, intptr_t{} };
		ZeroUserPointer = Value{ UserPointerClass, intptr_t{} };
		ZeroRange = Value{ RangeClass, intptr_t{} };

		/// Add methods for native classes
		LoadStdLib();
	}

	void Module::PatchGotos()
	{
		/// TODO: Patch gotos :P
	}

	void Module::AddNativeMethod(Class* klass, std::string_view _name, intptr_t pc, NativeMethod native)
	{
		if (IsLinked())
			ThrowExceptionPos(ReportModule::Declaration, {}, "trying to add native method  '{}' to class '{}' in linked module", _name, klass->FullyQualifiedName());

		auto name = MakeSym(_name);
		if (auto existing = klass->Members->Values.find(MV(name)); existing != klass->Members->Values.end())
			ThrowExceptionPos(ReportModule::Declaration, {}, "member '{}' of class '{}' already defined here: {}", _name, klass->FullyQualifiedName(), existing->second.Declaration->DeclPos());
		if (klass->Is(ClassFlags::Linked))
			ThrowExceptionPos(ReportModule::Declaration, {}, "trying to add native method '{}' to linked class '{}'", _name, klass->FullyQualifiedName());

		auto method = mHeap.New<Method>(klass, name);
		method->mFullyQualifiedName = klass->mFullyQualifiedName + "." + _name.data();
		method->Native = native;
		if (pc < 0)
		{
			pc = -pc;
			method->Set(MethodFlags::VarArgs);
		}
		method->ParamCount = pc;
		if (name->Op != 0)
			klass->OverloadsOperators.set(name->Op);
		klass->Members->Values.insert_or_assign(MV(name), MV(method));
	}

#ifdef RSL_DEBUG
	Sym* Module::MakeSym(std::string_view str)
	{
		auto it = mSymbolMap.find(str);
		if (it != mSymbolMap.end()) return it->second;

		return mSymbolMap[std::string{ str }] = mHeap.NewSized<Sym>(str.size(), str);
	}
#else
	Sym* Module::MakeSym(std::string_view str)
	{
		auto it = mSymbolMap.find(str);
		if (it != mSymbolMap.end()) return it.value();

		return mSymbolMap[str] = mHeap.NewSized<Sym>(str.size(), str);
	}
#endif

	void Module::Parse(std::string_view str, std::string_view filename)
	{
		mLinked = false;
		mCurLex = str.data();
		mLexEnd = str.data() + str.size();
		mStartLine = mCurLex;
		mCurPos.FileName = MakeSym(filename);

		/// Eat UTF-8 BOM
		if (str.size() >= 3 && std::memcmp(mCurLex, "\xEF\xBB\xBF", 3) == 0)
			mCurLex = mStartLine = mCurLex + 3;

		Advance(true);
		SkipNl();
		while (!IsEOF())
		{
			if (Is(KwType::Class))
				ParseClass();
			else if (Is(KwType::Interface))
				ParseInterface();
			else if (Is(KwType::Enum))
				ParseEnum();
			else if (Is(KwType::Extends))
				ParseClassExtension();
			else if (Is(KwType::Namespace))
			{
				Advance(true);
				auto name = EatId("namespace name");
				StartNamespace(name);
			}
			else if (Is(KwType::End))
			{
				Advance(true);
				if (mCurrentNamespace == mGlobalNamespace)
					ThrowParseException("unmatched end");
				EndNamespace();
			}
			else
				ThrowParseException("expected declaration");
			SkipNl();
		}
	}

	void Module::CheckForCycles(Class* if_class, Class* extends)
	{
		auto current = if_class;
		std::vector<Class*> stack;
		while (current && current != NullClass)
		{
			if (current == extends)
			{
				std::string chain;

				for (auto& cl : stack)
				{
					chain += cl->mFullyQualifiedName;
					chain += " -> ";
				}
				chain += current->mFullyQualifiedName;
				ReportDeclError(extends, "class '{}' extends itself; via {}", extends->FullyQualifiedName(), chain);
			}
			stack.push_back(current);
			current = current->mBaseClass;
		}
	}

	Sym const* Module::FindSymbol(std::string_view str) const
	{
		if (auto sym = mSymbolMap.find(str); sym != mSymbolMap.end())
#ifdef RSL_DEBUG
			return sym->second;
#else
			return sym.value();
#endif
		return nullptr;
	}

	void Module::Link()
	{
		if (mLinked)
			return;

		std::vector<std::string> errors;
		for (auto& klass : mClassesToLink)
		{
			/// Check base class name refers to a class
			if (!klass->mBaseClass)
			{
				if (!klass->mBaseClassName.IsNull())
				{
					auto parent = klass->mParentNamespace->FindNamespaceMember(klass->mBaseClassName);
					if (!parent.IsValid())
						ReportDeclError(klass, "class '{}' declared parent '{}' does not exist", klass->FullyQualifiedName(), JoinQTN(klass->mBaseClassName.Array));
					if (!parent.Is(IC::Class))
						ReportDeclError(klass, "class '{}' declared parent '{}' is not a class but a {}", klass->FullyQualifiedName(), JoinQTN(klass->mBaseClassName.Array), parent.Type->Name->Data);
					klass->mBaseClass = parent.Class;
				}
			}
			assert(klass->mBaseClass);

			if (klass->mBaseClass->Is(ClassFlags::Interface))
				ReportDeclError(klass, "class '{}' declared parent '{}' is not a class but an interface", klass->FullyQualifiedName(), JoinQTN(klass->mBaseClassName.Array));

			/// Check that the class has no cycles in its parents
			CheckForCycles(klass->mBaseClass, klass);

			using Interface = Class;
			/// Map the interface names to actual classes
			for (auto [qtn, pos] : klass->mImplementsClasses->Values)
			{
				if (qtn.Is(IC::Array))
				{
					auto iface = klass->mParentNamespace->FindNamespaceMember(qtn);
					if (!iface.IsValid())
						ReportDeclError(klass, "class '{}' tries to implement non-existent interface '{}'", klass->FullyQualifiedName(), JoinQTN(qtn.Array));
					if (!iface.Is(IC::Class) || !iface.Class->Is(ClassFlags::Interface))
						ReportDeclError(klass, "class '{}' tries to implement '{}' which is not an interface but a {}", klass->FullyQualifiedName(), JoinQTN(qtn.Array), iface.Type->Name->Data);
					CheckForCycles(iface.As<Interface>(), klass);
					klass->mImplementsClasses->Values.try_emplace(iface, pos);
				}
			}

			/// Now erase the QTNs from the interface list
			std::erase_if(klass->mImplementsClasses->Values, [](auto& kv) { return kv.first.Is(IC::Array); });
		}

		for (auto& klass : mClassesToLink)
		{
			Link(klass);
		}

		/// TODO: build VTables so we can have faster function calls

		mLinked = true;

		mHeap.Sweep();
	}

	template <typename FUNC>
	void split(std::string_view source, std::string_view delim, FUNC&& func) noexcept
	{
		size_t start = 0, next = 0;
		while ((next = source.find_first_of(delim, start)) != std::string::npos)
		{
			func(make_sv(source.begin() + start, source.begin() + next), false);
			start = next + delim.size();
		}
		func(make_sv(source.begin() + start, source.end()), true);
	}

	std::vector<std::string_view> split(std::string_view source, std::string_view delim) noexcept
	{
		std::vector<std::string_view> result;
		split(source, delim, [&](std::string_view str, bool last) {
			result.push_back(str);
		});
		return result;
	}

	NativeClass* Module::CreateNativeClass(Namespace const* pn, std::string_view name, NativeClassType type, Class const* parent_class, uint32_t flags)
	{
		if (!pn)
			ThrowExceptionPos(ReportModule::Declaration, {}, "invalid argument 'parent_namespace'");

		if (pn->GlobalNamespace() != GlobalNamespace())
			ThrowExceptionPos(ReportModule::Declaration, {}, "parent namespace belongs to a different module");

		auto sym = MakeSym(name);

		if (auto it = pn->Members.find(sym); it != pn->Members.end())
		{
			ThrowExceptionPos(ReportModule::Declaration, {}, "name '{}' already used here: {}", sym->Data, it->second.Declaration->mDeclPos);
		}

		/// Casting away constness here because we know the module isn't linked yet
		auto klass = mHeap.New<NativeClass>(const_cast<Namespace*>(pn), sym, type, flags);

		if (parent_class)
		{
			klass->mBaseClass = const_cast<Class*>(parent_class);
		}
		else
		{
			klass->mBaseClassName = Null;
			klass->mBaseClass = NullClass;
		}

		klass->Members = mHeap.New<Map>(true);
		klass->Members->Set(BaseObjectFlags::Const);
		klass->mImplementsClasses = mHeap.New<Map>(true);

		Add(klass);

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Added native class {}", klass->FullyQualifiedName());
#endif

		return klass;
	}

	[[noreturn]]
	void Module::ThrowNativeClassExists(std::type_index ti, Class const* klass)
	{
		ThrowExceptionPos(ReportModule::Declaration, {}, "native class of type '{}' already exists as '{}'", ti.name(), klass->FullyQualifiedName());
	}

	Class const* Module::FindClass(std::string_view name) const noexcept
	{
		auto members = split(name, ".");
		auto class_name = FindSymbol(members.back());
		if (!class_name) return nullptr;

		members.pop_back();
		auto ns = mGlobalNamespace;
		for (auto member : members)
		{
			if (auto sym = FindSymbol(member))
			{
				Value result = ns->FindNamespaceMemberSelfOnly(MV(sym));
				if (!result.IsValid() || !result.Is(IC::Namespace))
					return nullptr;
				ns = result.Namespace;
			}
			else
				return nullptr;
		}
		Value klass = ns->FindNamespaceMemberSelfOnly(MV(class_name));
		if (!klass.IsValid() || !klass.Is(IC::Class))
			return nullptr;
		return klass.Class;
	}

	void Module::OpenScope()
	{
		intptr_t start_stack_index = mScopeStack.empty() ? (mCurrentMethod->ParamCount + 1) : mScopeStack.back().FreeStackIndex;
		auto& new_scope = mScopeStack.emplace_back();
		new_scope.StartIP = IP() + 1;
		new_scope.StartPos = mCurPos;
		new_scope.FreeStackIndex = start_stack_index;
	}

	void Module::CloseScope()
	{
		auto& scope = Scope();
		for (auto& [name, local] : scope.mLocals)
		{
			if (!local.Used)
				ReportWarning(ReportModule::CodeGen, "{} '{}' not used, declared here: {}", local.Type, name->Data, local.DeclPos);
		}
		mCurrentMethod->MaxLocalIndex = std::max(mCurrentMethod->MaxLocalIndex, scope.FreeStackIndex);
		scope.EndIP = IP();
		scope.EndPos = mCurPos;
		mScopeStack.pop_back();
	}

	Module::LocalVar* Module::FindLocal(Sym* sym)
	{
		for (intptr_t i = (intptr_t)mScopeStack.size() - 1; i >= 0; --i)
		{
			if (auto it = mScopeStack[i].mLocals.find(sym); it != mScopeStack[i].mLocals.end())
				return &it->second;
		}
		return nullptr;
	}

	void Module::StartNamespace(Sym* name)
	{
		Namespace* new_ns = nullptr;
		if (auto it = mCurrentNamespace->Members.find(name); it == mCurrentNamespace->Members.end())
		{
			new_ns = mHeap.New<Namespace>(mCurrentNamespace, name);
			mCurrentNamespace->Members.insert_or_assign(name, MV(new_ns));
		}
		else if (it->second.Type != NamespaceClass)
			ReportDeclError(mCurPos, "name '{}' already used here: {}", name->Data, it->second.Declaration->mDeclPos);
		else
			new_ns = it->second.Namespace;

		/// Update namespace position to first in file, so we can have nice error reporting
		if (new_ns->mDeclPos.FileName != mCurPos.FileName)
			new_ns->mDeclPos = mCurPos;

		new_ns->Set(BaseObjectFlags::Const);
		Add(new_ns);
		mCurrentNamespace = new_ns;
	}

	void Module::EndNamespace()
	{
		mCurrentNamespace = const_cast<Namespace*>(mCurrentNamespace->mParentNamespace);
	}

	void Module::Add(Class* klass)
	{
		mCurrentNamespace->Members[klass->Name] = MV(klass);
		klass->mFullyQualifiedName = mCurrentNamespace->mFullyQualifiedName + "." + klass->Name->Data;
		mClassesToLink.push_back(klass);
	}

	void Module::Add(Namespace* ns)
	{
		mCurrentNamespace->Members[ns->Name] = MV(ns);
		ns->mFullyQualifiedName = mCurrentNamespace->mFullyQualifiedName + "." + ns->Name->Data;
	}

	intptr_t Module::AddLocal(Sym* name, SourcePos const& pos, const char* local_type)
	{
		auto& scope = mScopeStack.back();
		auto& locals = scope.mLocals;
		if (auto it = locals.find(name); it != locals.end())
			ThrowExceptionPos(ReportModule::Declaration, pos, "{} '{}' already defined here: {}", local_type, name->Data, it->second.DeclPos.ToString());
		auto& local = locals[name];
		local.DeclPos = pos;
		local.Name = name;
		local.Type = local_type;
		if (name->Data[0] == '@') local.Used = true; /// special locals are always used
		return local.StackIndex = scope.FreeStackIndex++;
	}

	std::string Module::JoinQTN(Arr* arr)
	{
		std::string name = "";
		for (auto& m : arr->Values)
		{
			name += '.';
			name += m.Symbol->Data;
		}
		return name;
	}

	std::string Value::ToString() const
	{
		if (!IsValid())
			return "[INVALID]";
		else if (Is(MemberAccess::Name))
			return fmt::format("[->{}]", Symbol->Data);
		//else if (Is(MemberAccess::Method))
		//	return fmt::format("[->{}]", Declaration->FullyQualifiedName());
		else if (Is(MemberAccess::Slot) || Is(MemberAccess::ConstSlot))
			return fmt::format("[this.field #{}]", Int);
		else if (IsNull())
			return "[null]";
		else if (Type->Is(ClassFlags::NotGC))
		{
			switch (Type->Internal())
			{
			case IC::Integer: return fmt::format("[{} {{{}}}]", RealType()->FullyQualifiedName(), Int);
			case IC::Float: return fmt::format("[{} {{{}}}]", RealType()->FullyQualifiedName(), Float);
			case IC::Boolean: return fmt::format("[{} {{{}}}]", RealType()->FullyQualifiedName(), (Int ? "true" : "false"));
			default: return fmt::format("[{} {{{}}}]", RealType()->FullyQualifiedName(), (void*)GCVal);
			}
		}
		else if (Is(IC::Symbol))
			return fmt::format("[symbol \"{}\"]", Symbol->Data);
		else
			return fmt::format("[instance of {}]", RealType()->FullyQualifiedName());
	}

	RSException::RSException(SourcePos const& pos, std::string_view msg)
		: Line(pos.Line), Column(pos.Column)
		, FileName(pos.FileName ? pos.FileName->Data : "internal")
		, Message(msg)
	{
	}

	std::string RSException::ToString() const
	{
		auto line = fmt::format("{}({},{}): {}", FileName, Line, Column + 1, Message);
		for (auto& sub : SubLines)
		{
			line += '\n';
			line += sub;
		}
		return line;
	}

	Value::operator bool() const noexcept { return Type->Internal() != IC::Null; }

	std::string SourcePos::ToString() const
	{
		return FileName ? fmt::format("{}({},{})", (const char*)FileName->Data, Line, Column + 1) : "internal";
	}

}