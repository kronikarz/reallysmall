#include "../include/rsl/CppInterop.h"
#include "Private.h"
#include "../include/rsl/ExecutionContext.h"

namespace rsl
{
	void* Obj::NativeObject()
	{
		return (void*)mAdditionalData;
	}

	std::span<Value> Obj::Slots()
	{
		return { RawSlots(), (size_t)Class->SlotCount() };
	}

	Obj::~Obj()
	{
		auto slots = Slots();
		std::destroy(slots.begin(), slots.end());
		if (Class->Is(ClassFlags::Native))
			((NativeClass*)Class)->CallDestructor(NativeObject());
	}

	NativeClass::NativeClass(Namespace* pn, Sym* name, NativeClassType type, uint32_t flags)
		: Class(pn, name, flags | ghassanpl::flag_bits<uint32_t>(ClassFlags::Native))
		, mType(type)
	{
		if (type == NativeClassType::RefType)
		{
			mToValue = [this](ExecutionContext& vm, void const* ptr) { return Value{ this, (void*)*(void**)ptr }; };
			mFromValue = [](ExecutionContext& vm, void* ptr, Value val) { *(void**)ptr = val.User; };
			Set(ClassFlags::NotGC);
		}
		else if (type == NativeClassType::FullType)
		{
			Set(ClassFlags::Constructable);
		}
		else if (type == NativeClassType::ValueType)
		{
			throw RSException("internal: creating a class for a native value type");
		}
	}

	void NativeClass::CallConstructor(void* obj) const
	{
		if (mNativeConstructor) mNativeConstructor(obj);
	}

	void NativeClass::CallDestructor(void* obj) const
	{
		if (mNativeDestructor) mNativeDestructor(obj);
	}

	Value NativeClass::PreConstruct(ExecutionContext& vm) const
	{
		if (mInternal != IC::UserClass)
			vm.ThrowRuntimeException("internal: trying to construct an object of non-user class '{}'", mFullyQualifiedName);

		if (mType != NativeClassType::FullType)
			throw RSRuntimeException("internal: trying to construct a Native object of {} type", (int)mType);

		ValueRef obj{ Value{ this, vm.Heap().NewSized<Obj>(-1 + ObjectSize + mSlotCount * sizeof(Value), this) } };
		CallConstructor(obj->Object->NativeObject()); /// TODO: Move this to the constructor
		return obj.Value();
	}

}