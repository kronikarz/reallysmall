#include "../include/rsl/RSL.h"
#include "Private.h"

namespace rsl
{

	bool Module::IsBinOperator() const { return mCurrentToken.Type == TokType::Operator && is_flag_set(Ops[mCurrentToken.ParsedVal.Int].Flags, OpFlags::Binary); }
	bool Module::IsUnOperator() const { return mCurrentToken.Type == TokType::Operator && is_flag_set(Ops[mCurrentToken.ParsedVal.Int].Flags, OpFlags::Unary); }

	void Module::Eat(TokType tok)
	{
		if (!Is(tok))
			ThrowParseException("expected {}", Toks[(int)tok].Name);
		Advance(true);
	}

	void Module::Eat(KwType tok)
	{
		if (!Is(tok))
			ThrowParseException("expected keyword '{}'", Kws[(int)tok].Name);
		Advance(true);
	}

	void Module::Eat(OpType tok)
	{
		if (!Is(tok))
			ThrowParseException("expected operator '{}'", Ops[(int)tok].Name);
		Advance(true);
	}

	Sym* Module::EatId(std::string_view description)
	{
		if (Is(TokType::Identifier))
		{
			auto result = MakeSym(mCurrentToken.Range);
			Advance(false);
			return result;
		}
		ThrowParseException("expected {}", description);
	}

	Sym* Module::EatSym()
	{
		if (Is(TokType::Symbol))
		{
			auto result = mCurrentToken.ParsedVal.Symbol;
			Advance(false);
			return result;
		}
		ThrowParseException("expected symbol");
	}

	void Module::SkipNl()
	{
		while (Is(TokType::NewLine))
			Advance(true);
	}

	std::string Module::CurrentTokenStr()
	{
		switch (mCurrentToken.Type)
		{
		case TokType::Eof: return "end of file";
		case TokType::Keyword: return fmt::format("keyword '{}'", Kws[mCurrentToken.ParsedVal.Int].Name);
		case TokType::Operator: return fmt::format("operator '{}' ('{}')", Ops[mCurrentToken.ParsedVal.Int].Name, mCurrentToken.Range);
		case TokType::NewLine: return "newline";
		case TokType::Symbol: return fmt::format("symbol literal '{}'", mCurrentToken.ParsedVal.Symbol->Data, mCurrentToken.Range);
		case TokType::Float: return fmt::format("float literal {} ('{}')", mCurrentToken.ParsedVal.Float, mCurrentToken.Range);
		case TokType::Integer: return fmt::format("integer literal {} ('{}')", mCurrentToken.ParsedVal.Int, mCurrentToken.Range);
		case TokType::Identifier: return fmt::format("identifier '{}'", mCurrentToken.Range);
		case TokType::Unknown:
		default:
			return "unknown token";
		}
	}

	ExpTree Module::ParseExp() { OpType _ = OpType::Unknown; return ParseSub(0, _); }

	ExpTree Module::ParseSuffixed()
	{
		ExpTree exp;
		exp.Pos = mCurPos;

		switch (mCurrentToken.Type)
		{
		case TokType::Integer:
		case TokType::Float:
		case TokType::Symbol:
			exp.Set(mCurrentToken.ParsedVal);
			Advance(false);
			break;
		case TokType::Keyword:
			switch ((KwType)mCurrentToken.ParsedVal.Int)
			{
				/// TODO: PushNull/PushTrue/PushFalse (for smaller )
			case KwType::Null: exp.SetNull(); Advance(false); break;
			case KwType::False: exp.SetFalse(); Advance(false); break;
			case KwType::True: exp.SetTrue(); Advance(false); break;
			case KwType::This: exp.SetThis(); Advance(false); break;
			default: goto error;
			}
			break;
		case TokType::Operator:
			switch ((OpType)mCurrentToken.ParsedVal.Int)
			{
			case OpType::OpenBracket: exp = ParseArray(); break;
			case OpType::OpenBrace: exp = ParseMap(); break;
			case OpType::OpenParen: Advance(true); exp = ParseExp(); Eat(OpType::CloseParen); break;
			default: goto error;
			}
			break;
		case TokType::Identifier: {
			auto varname = EatId("variable name");
			if (auto i = FindLocal(varname))
			{
				exp.SetLocal(i->StackIndex);
				i->Used = true;
			}
			else
				exp.SetFreeVar(varname);
			break;
		}
													error:
		default:
			ThrowParseException("expected expression, got {}", CurrentTokenStr());
		}

		while (true)
		{
			if (TryEat(OpType::Dot))
			{
				if (exp.IsThis())
					exp.SetThisMember(EatId("slot name"));
				else
					exp.SetMember(EatId("slot name"));
				exp.Pos = mCurPos;
			}
			else if (TryEat(OpType::OpenBracket))
			{
				exp.SetAtIndex(ParseExp());
				exp.Pos = mCurPos;
				Eat(OpType::CloseBracket);
			}
			else if (TryEat(OpType::OpenParen))
			{
				/// If this looks like a method call, note that this class requires the use of this member
				if (exp.Is(ExpType::FreeVar) || exp.Is(ExpType::ThisMember))
				{
					if (mCurrentMethod->ParentClass->Is(ClassFlags::Interface))
						mCurrentMethod->ParentClass->RequiresMembers[exp.Name] = exp.Pos;
				}
				exp.SetCall();
				exp.Pos = mCurPos;
				while (!TryEat(OpType::CloseParen))
				{
					exp.Children.push_back(ParseExp());
					if (TryEat(OpType::Comma))
						continue;
					else if (!Is(OpType::CloseParen))
						ThrowParseException("expected comma or ')'");
				}
			}
			else
				return exp;
		}
	}

	ExpTree Module::ParseSub(int limit, OpType& new_op)
	{
		ExpTree exp;
		exp.Pos = mCurPos;

		if (Is(TokType::Operator) && IsUnOperator())
		{
			auto op = mCurrentToken.ParsedVal.Op;
			Advance(true);
			exp.Unary(op, ParseSub(Ops[(int)OpType::Not].Precedence.first, new_op));
		}
		else if (Is(KwType::New))
		{
			Advance(true);
			auto qualified_name = ParseQualifiedTypeName();

			exp.New(qualified_name);

			Eat(OpType::OpenParen);
			while (!TryEat(OpType::CloseParen))
			{
				exp.Children.push_back(ParseExp());
				if (TryEat(OpType::Comma))
					continue;
				else if (!Is(OpType::CloseParen))
					ThrowParseException("expected comma or ')'");
			}
		}
		else if (Is(KwType::Yield))
		{
			Advance(true);
			
			Eat(OpType::OpenParen);
			if (TryEat(OpType::CloseParen))
				exp.Yield(ExpTree{});
			else
			{
				exp.Yield(ParseExp());
				Eat(OpType::CloseParen);
			}
		}
		else
			exp = ParseSuffixed();

		if (!IsBinOperator())
		{
			new_op = OpType::Unknown;
			return exp;
		}

		auto op = mCurrentToken.ParsedVal.Op;
		while (op != OpType::Unknown && is_flag_set(Ops[(int)op].Flags, OpFlags::Binary) && Ops[(int)op].Precedence.first > limit)
		{
			Advance(true);

			OpType nextop_op = OpType::Unknown;
			auto nextop = ParseSub(Ops[(int)op].Precedence.second, nextop_op);

			switch (op) /// TODO: Constant folding
			{
			case OpType::Star:
			case OpType::Percent:
			case OpType::Slash:
			case OpType::Plus:
			case OpType::Minus:
			case OpType::Shl:
			case OpType::Shr:
			case OpType::BinAnd:
			case OpType::BinXor:
			case OpType::BinOr:
			case OpType::Spaceship:
			case OpType::Less:
			case OpType::LessEq:
			case OpType::Greater:
			case OpType::GreaterEq:
			case OpType::Is:
			case OpType::As:
			case OpType::And:
			case OpType::Or:
			case OpType::EqualEqual:
			case OpType::NotEqual:
			case OpType::StrictEqual:
			case OpType::NotStrictEqual: exp.SetBinOp(op, std::move(nextop)); break;
			case OpType::Equal: exp.SetAssign(std::move(nextop), *this); break;
			default:
				ThrowParseException("unhandled operator '{}'", Ops[(int)op].Name);
			}

			op = nextop_op;
		}
		new_op = op;
		return exp;
	}

	ExpTree Module::ParseArray()
	{
		Advance(true);
		ExpTree exp;
		exp.Pos = mCurPos;
		exp.SetArray();

		while (!TryEat(OpType::CloseBracket))
		{
			SkipNl();
			exp.Children.push_back(ParseExp());
			SkipNl();
			if (TryEat(OpType::Comma))
				SkipNl();
		}

		return exp;
	}

	ExpTree Module::ParseMap()
	{
		Advance(true);
		ExpTree exp;
		exp.Pos = mCurPos;
		exp.SetMap();

		while (!TryEat(OpType::CloseBrace))
		{
			SkipNl();
			if (TryEat(OpType::OpenBracket))
			{
				exp.Children.push_back(ParseExp());
				Eat(OpType::CloseBracket);
			}
			else if (Is(TokType::Symbol))
			{
				ExpTree sym;
				sym.Pos = mCurPos;
				sym.Set(MV(EatSym()));
				exp.Children.push_back(std::move(sym));
			}
			else if (Is(TokType::Identifier))
			{
				ExpTree sym;
				sym.Pos = mCurPos;
				sym.Set(MV(EatId("map key")));
				exp.Children.push_back(std::move(sym));
			}
			else
				ThrowParseException("expected key name, symbol, or [ expression ], got {}", CurrentTokenStr());
			SkipNl();
			TryEat(OpType::Colon); /// Should this be non-optional? We'll see if we get any bugs in parsing
			SkipNl();
			exp.Children.push_back(ParseExp());
			SkipNl();
			if (TryEat(OpType::Comma))
				continue;
			else if (TryEat(OpType::CloseBrace))
				break;
		}

		return exp;
	}


	StatTree Module::ParseStat()
	{
		StatTree stat{ mCurPos };
		switch (mCurrentToken.Type)
		{
		case TokType::Keyword:
			switch ((KwType)mCurrentToken.ParsedVal.Int)
			{
			case KwType::If: return ParseIf();
			case KwType::While: return ParseWhile();
			case KwType::For: return ParseFor();
			case KwType::Var: return ParseVar();
			case KwType::Return:
				Advance(true);
				stat.SetReturn(ParseExp());
				return stat;
			case KwType::Assuming: {
				auto before = mCurLex;
				Advance(true);
				auto exp = ParseExp();
				auto after = mCurLex;
				stat.SetAssuming(std::move(exp), MakeSym(trim_whitespace(make_sv(before, after))));
				return stat;
			}
			default: break;
			}
		case TokType::Identifier:
		default:
			stat.SetExp(ParseExp());
			return stat;
		}
	}

	StatTree Module::ParseIf()
	{
		StatTree stat{ mCurPos };
		OpenScope();
		Advance(true);
		stat.SetIf(ParseExp());
		TryEat(KwType::Then);
		SkipNl();
		while (!TryEat(KwType::End))
		{
			if (TryEat(KwType::Elseif))
			{
				CloseScope();
				OpenScope();
				StatTree elseif{ mCurPos };
				elseif.SetElseif(ParseExp());
				stat.Statements.push_back(std::move(elseif));
				TryEat(KwType::Then);
			}
			else if (TryEat(KwType::Else))
			{
				CloseScope();
				OpenScope();
				StatTree els{ mCurPos };
				els.SetElse();
				stat.Statements.push_back(std::move(els));
			}
			else
				stat.Statements.push_back(ParseStat());
			SkipNl();
		}
		CloseScope();
		return stat;
	}

	StatTree Module::ParseWhile()
	{
		StatTree stat{ mCurPos };
		OpenScope();
		Advance(true);
		stat.SetWhile(ParseExp());
		TryEat(KwType::Do);
		SkipNl();
		while (!TryEat(KwType::End))
		{
			stat.Statements.push_back(ParseStat());
			SkipNl();
		}
		CloseScope();
		return stat;
	}

	StatTree Module::ParseFor()
	{
		OpenScope();
		StatTree stat{ mCurPos };
		stat.SetFor();
		Advance(true);
		auto pos = mCurPos;
		//stat.Name = ;
		stat.Local = AddLocal(EatId("iterator variable name"), pos, "for iterator");
		Eat(OpType::Equal);
		stat.Expressions[0] = ParseExp();
		Eat(OpType::Comma);
		AddLocal(symSpecialVarEnd, mCurPos, "for end value");
		stat.Expressions[1] = ParseExp();
		TryEat(KwType::Do);
		SkipNl();
		while (!TryEat(KwType::End))
		{
			stat.Statements.push_back(ParseStat());
			SkipNl();
		}
		CloseScope();
		return stat;
	}

	StatTree Module::ParseVar()
	{
		StatTree stat{ mCurPos };
		Advance(true);
		stat.SetVar(AddLocal(EatId("local variable name"), mCurPos, "local"));
		Eat(OpType::Equal);
		stat.Expressions[0] = ParseExp();
		return stat;
	}

	void Module::ParseInterface()
	{
		auto pos = mCurPos;
		Eat(KwType::Interface);
		auto name = EatId("interface name");

		if (auto it = mCurrentNamespace->Members.find(name); it != mCurrentNamespace->Members.end())
		{
			ThrowExceptionPos(ReportModule::Declaration, pos, "name '{}' already used here: {}", name->Data, it->second.Declaration->mDeclPos);
		}

		auto iface = mHeap.New<Class>(mCurrentNamespace, pos, name, flag_bits<uint32_t>(ClassFlags::Interface));
		Add(iface);

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Starting interface {}", iface->FullyQualifiedName());
#endif

		iface->Set(ClassFlags::Interface);
		iface->mBaseClassName = Null;
		iface->mBaseClass = NullClass;
		iface->Members = mHeap.New<Map>(true);
		iface->Members->Set(BaseObjectFlags::Const);
		iface->mImplementsClasses = mHeap.New<Map>(true);

		/// Eat interface list
		if (TryEat(OpType::Is))
			ParseClassInterfaceList(iface);

		/// Eat qualifier list
		if (TryEat(KwType::Also))
			ParseClassAlso(iface);

		/// Body!
		SkipNl();
		while (!TryEat(KwType::End))
		{
			if (TryEat(KwType::Func))
			{
				ParseMethod(iface);
				SkipNl();
			}
			else
				ThrowParseException("expected interface member, got {}", CurrentTokenStr());
		}

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Finished interface {}", iface->FullyQualifiedName());
#endif
	}

	void Module::ParseClass()
	{
		auto pos = mCurPos;
		Eat(KwType::Class);
		auto name = EatId("class name");

		if (auto it = mCurrentNamespace->Members.find(name); it != mCurrentNamespace->Members.end())
		{
			ThrowExceptionPos(ReportModule::Declaration, pos, "name '{}' already used here: {}", name->Data, it->second.Declaration->mDeclPos);
		}

		auto klass = mHeap.New<Class>(mCurrentNamespace, pos, name, flag_bits<uint32_t>(ClassFlags::Constructable));

		if (TryEat(KwType::Extends))
		{
			klass->mBaseClassName = MV(ParseQualifiedTypeName());
		}
		else
		{
			klass->mBaseClassName = Null;
			klass->mBaseClass = NullClass;
		}

		Add(klass);

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Starting class {}", klass->FullyQualifiedName());
#endif

		klass->Members = mHeap.New<Map>(true);
		klass->Members->Set(BaseObjectFlags::Const);
		klass->mConstructor = mHeap.New<Method>(klass, mCurPos, symNew);
		klass->mConstructor->Statements->emplace_back(SourcePos{}); /// <- for the initializer list 'statement'
		klass->mConstructor->mFullyQualifiedName = klass->mFullyQualifiedName + ".new";
		klass->mImplementsClasses = mHeap.New<Map>(true);

		/// Eat interface list
		if (TryEat(OpType::Is))
			ParseClassInterfaceList(klass);

		/// Eat qualifier list
		if (TryEat(KwType::Also))
			ParseClassAlso(klass);

		/// Body
		SkipNl();
		while (!TryEat(KwType::End))
		{
			if (TryEat(KwType::Var))
			{
				ParseField(klass);
				SkipNl();
			}
			else if (TryEat(KwType::Func))
			{
				ParseMethod(klass);
				SkipNl();
			}
			else
				ThrowParseException("expected either class member, got {}", CurrentTokenStr());
		}

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Finished class {}", klass->FullyQualifiedName());
#endif
	}

	void Module::ParseEnum()
	{
		auto pos = mCurPos;
		Eat(KwType::Enum);
		auto name = EatId("enum name");

		if (auto it = mCurrentNamespace->Members.find(name); it != mCurrentNamespace->Members.end())
		{
			ThrowExceptionPos(ReportModule::Declaration, pos, "name '{}' already used here: {}", name->Data, it->second.Declaration->mDeclPos);
		}

		auto klass = mHeap.New<Class>(mCurrentNamespace, pos, name, 0U);

		klass->mBaseClassName = Null;
		klass->mBaseClass = NullClass;
		//klass->mBaseClassName = Enum;
		//klass->mBaseClass = EnumClass;

		Add(klass);

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Starting enum {}", klass->FullyQualifiedName());
#endif

		klass->Members = mHeap.New<Map>(true);
		klass->Members->Set(BaseObjectFlags::Const);
		klass->mImplementsClasses = mHeap.New<Map>(true);

		/// Eat qualifier list
		if (TryEat(KwType::Also))
			ParseEnumAlso(klass);

		/// Body
		SkipNl();
		Value last_value = MV(0LL);
		while (!TryEat(KwType::End))
		{
			auto& value = ParseConstant(klass, "enumerator");
			if (!value.IsValid())
				value = last_value;
			else if (value.Is(IC::Integer))
				last_value = value;
			last_value.Int++;

			SkipNl();
		}

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Finished enum {}", klass->FullyQualifiedName());
#endif
	}

	Value& Module::ParseConstant(Class* klass, const char* desc)
	{
		auto curpos = mCurPos;
		auto name = MV(EatId(fmt::format("{} name", desc)));
		if (auto member = klass->Members->Values.find(name); member != klass->Members->Values.end())
		{
			auto existing_member = member->second.Declaration;
			ThrowExceptionPos(ReportModule::Declaration, curpos, "a member with the name '{}' already defined here: {}", name.Symbol->Data, existing_member->mDeclPos);
		}

		auto constant = mHeap.New<Constant>(klass, curpos, name.Symbol);
		constant->ParentClass = klass;
		constant->mFullyQualifiedName = klass->mFullyQualifiedName + "." + name.Symbol->Data;

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Starting {} {}", desc, constant->FullyQualifiedName());
#endif

		if (TryEat(OpType::Equal))
		{
			auto exppos = mCurPos;
			auto exp = ParseExp();
			if (exp.Type == ExpType::Constant && exp.Val.IsInt())
				constant->Value = exp.Val;
			else
				ThrowExceptionPos(ReportModule::Declaration, exppos, "{} value must be a constant integer expression", desc);
		}
		else
			constant->Value = Value::Invalid();

		klass->Members->Values.insert_or_assign(name, Value{ ConstantClass, constant });

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Finished {} {}", desc, constant->FullyQualifiedName());
#endif

		return constant->Value;
	}

	void Module::ParseEnumAlso(Class* klass)
	{
		/// TODO: this
	}

	void Module::ParseClassExtension()
	{
		auto pos = mCurPos;
		Eat(KwType::Extends);
		auto name = MV(ParseQualifiedTypeName());

		auto it = mCurrentNamespace->FindNamespaceMember(name);
		if (!it.IsValid() || !it.Is(IC::Class))
		{
			ThrowExceptionPos(ReportModule::Declaration, pos, "name '{}' does not refer to a class (yet)", JoinQTN(name.Array));
		}

		auto klass = it.Class;

		if (TryEat(OpType::Is))
			ParseClassInterfaceList(klass);

		if (TryEat(KwType::Also))
			ParseClassAlso(klass);

		SkipNl();
		while (!TryEat(KwType::End))
		{
			if (TryEat(KwType::Func))
			{
				ParseMethod(klass);
				SkipNl();
			}
			else
				ThrowParseException("expected func, got {}", CurrentTokenStr());
		}
	}


	void Module::ParseField(Class* klass)
	{
		auto curpos = mCurPos;
		auto name = MV(EatId("field name"));
		if (name.Symbol == klass->Name)
			ThrowParseException("cannot declare field with the same name as class");
		if (auto member = klass->Members->Values.find(name); member != klass->Members->Values.end())
		{
			auto existing_member = member->second.Declaration;
			ThrowExceptionPos(ReportModule::Declaration, curpos, "a member with the name '{}' already defined here: {}", name.Symbol->Data, existing_member->mDeclPos);
		}

		auto field = mHeap.New<Field>(klass, curpos, name.Symbol);
		field->ParentClass = klass;
		field->SlotID = klass->mSlotCount;
		field->mFullyQualifiedName = klass->mFullyQualifiedName + "." + name.Symbol->Data;

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Starting field {}", field->FullyQualifiedName());
#endif

		if (TryEat(OpType::Equal))
			field->InitExp = ParseExp();
		else
			field->InitExp.SetNull();

		if (TryEat(KwType::Also))
		{
			ParseFieldAlso(field);
		}

		klass->Members->Values.insert_or_assign(name, Value{ FieldClass, field });
		klass->mSlotCount++;

#ifdef RSL_DEBUG
		Report(ReportType::Trace, ReportModule::CodeGen, "Finished field {}", field->FullyQualifiedName());
#endif
	}

	void Module::ParseFieldAlso(Field* field)
	{
		do
		{
			SkipNl();
			switch (mCurrentToken.ParsedVal.Kw)
			{
			case KwType::Static: field->Set(FieldFlags::Static); break;
			case KwType::Const: field->Set(FieldFlags::Const); break;
			case KwType::Readonly: field->Set(FieldFlags::ReadOnly); break;
			default:
				ThrowParseException("expected field qualifier (static, const, readonly, etc...)");
			}
			Advance(false);
		} while (TryEat(OpType::Comma));
	}

	void Module::ParseMethodAlso(Method* method)
	{
		do
		{
			SkipNl();
			switch (mCurrentToken.ParsedVal.Kw)
			{
			case KwType::Static: method->Set(MethodFlags::Static); break;
			case KwType::Const: method->Set(MethodFlags::Const); break;
			case KwType::Abstract: method->Set(MethodFlags::Abstract); break;
			case KwType::Final: method->Set(MethodFlags::Final); break;
				/// TODO: discardable_result
			default:
				ThrowParseException("expected method qualifier (static, const, abstract, final, etc...)");
			}
			Advance(false);
		} while (TryEat(OpType::Comma));
	}

	void Module::ParseClassInterfaceList(Class* klass)
	{
		do
		{
			SkipNl();
			klass->mImplementsClasses->Values.try_emplace(MV(ParseQualifiedTypeName()), True); continue;
			Advance(false);
		} while (TryEat(OpType::Comma));
	}

	void Module::ParseClassAlso(Class* klass)
	{
		do
		{
			SkipNl();

			switch (mCurrentToken.ParsedVal.Kw)
			{
			case KwType::Abstract: klass->Set(ClassFlags::Abstract); break;
			case KwType::Final: klass->Set(ClassFlags::Final); break;
			default: klass->mImplementsClasses->Values.try_emplace(MV(ParseQualifiedTypeName()), True); continue;
			}
			Advance(false);
		} while (TryEat(OpType::Comma));
	}


	void Module::ParseMethod(Class* klass)
	{
		auto curpos = mCurPos;
		auto name = MV(EatId("method name"));
		if (name.Symbol == klass->Name)
			ThrowParseException("cannot declare method with the same name as class");
		if (auto member = klass->Members->Values.find(name); member != klass->Members->Values.end())
			ThrowExceptionPos(ReportModule::Declaration, curpos, "a member with the name '{}' already defined here: {}", name.Symbol->Data, member->second.Declaration->mDeclPos);

		/// Start new method

		mLabelIPs.clear();
		mUnpatchedGotos.clear();
		mScopeStack.clear();

		mCurrentMethod = mHeap.New<Method>(klass, curpos, name.Symbol);
		mCurrentMethod->mFullyQualifiedName = klass->mFullyQualifiedName + "." + name.Symbol->Data;
		mCurrentMethod->ParentClass = klass;

		OpenScope();
		Eat(OpType::OpenParen);
		while (!TryEat(OpType::CloseParen))
		{
			auto pos = mCurPos;
			auto argname = EatId("parameter name");
			AddLocal(argname, pos, "argument");
			mCurrentMethod->ParamCount++;
			if (TryEat(OpType::Comma))
				continue;
			else if (!Is(OpType::CloseParen))
				ThrowParseException("expected comma or ')'");
		}
		Scope().FreeStackIndex = mCurrentMethod->ParamCount + 1;

		if (TryEat(KwType::Also))
			ParseMethodAlso(mCurrentMethod);

		SkipNl();
		while (!TryEat(KwType::End))
		{
			mCurrentMethod->Statements->push_back(ParseStat());
			SkipNl();
		}

		if (mCurrentMethod->Statements->size() == 0 || mCurrentMethod->Statements->back().Type != StatType::Return)
		{
			StatTree stat{ mCurPos };
			stat.SetReturn();
			mCurrentMethod->Statements->push_back(std::move(stat));
		}

		CloseScope();

		if (klass->Name->Op != 0) klass->OverloadsOperators.set(klass->Name->Op);
		klass->Members->Values.insert_or_assign(name, Value{ MethodClass, mCurrentMethod });

		mCurrentMethod = nullptr;
	}

	Arr* Module::ParseQualifiedTypeName()
	{
		Arr* qualified_name = mHeap.New<Arr>();
		qualified_name->Set(BaseObjectFlags::Const);
		do
		{
			qualified_name->Values.push_back(MV(EatId("class or namespace name")));
		} while (TryEat(OpType::Dot));
		return qualified_name;
	}

}