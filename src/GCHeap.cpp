#include "../include/rsl/GCHeap.h"
#include "Private.h"
#ifdef RSL_DEBUG
#include <chrono>
using namespace std::chrono;
#endif

namespace rsl
{
	void GCBase::Mark()
	{
		if (!are_any_flags_set(Flags, BaseObjectFlags::Marked, BaseObjectFlags::Permanent))
		{
			Set(BaseObjectFlags::Marked);
			MarkMembers();
		}
	}

	void GCHeap::Sweep()
	{
#ifdef RSL_MULTITHREADED_SUPPORT
		std::lock_guard<std::recursive_mutex> lock{ mGCMutex };
#endif
#ifdef RSL_DEBUG
		mOSI.ReportSingle(ReportType::Trace, ReportModule::OSI, {}, fmt::format("gc: started sweep phase for {} objects", mGCObjs.size()));
		auto start = high_resolution_clock::now();
#endif
		std::vector<GCBase*> garbage;
		for (size_t i = 0; i < mGCObjs.size();)
		{
			bool rooted = mGCObjs[i]->Is(BaseObjectFlags::Marked) || mGCObjs[i]->RefCount;
			bool permanent = mGCObjs[i]->Is(BaseObjectFlags::Permanent);
			if (!rooted && !permanent)
			{
				garbage.push_back(mGCObjs[i]);
				mGCObjs[i] = mGCObjs.back();
				mGCObjs.pop_back();
			}
			else
			{
				if (!permanent)
					mGCObjs[i]->Unset(BaseObjectFlags::Marked);
				++i;
			}
		}
#ifdef RSL_DEBUG
		auto time = high_resolution_clock::now() - start;
		mOSI.ReportSingle(ReportType::Trace, ReportModule::OSI, {}, fmt::format("gc: sweep phase took {} ns", time.count()));
#endif
		Destroy(garbage);
	}

	void GCHeap::Destroy(std::vector<GCBase*> const& garbage)
	{
#ifdef RSL_DEBUG
		mOSI.ReportSingle(ReportType::Trace, ReportModule::OSI, {}, fmt::format("gc: collecting {} garbage objects", garbage.size()));
		auto start = high_resolution_clock::now();
#endif
		for (auto& garbagio : garbage)
		{
			std::destroy_at(garbagio);
			mOSI.Free((uint8_t*)garbagio);
		}
#ifdef RSL_DEBUG
		auto time = high_resolution_clock::now() - start;
		mOSI.ReportSingle(ReportType::Trace, ReportModule::OSI, {}, fmt::format("gc: destruction phase took {} ns", time.count()));
#endif
	}

}