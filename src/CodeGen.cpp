#include "Private.h"

namespace rsl
{

	void Module::CodeGen(StatTree const& stat)
	{
		switch (stat.Type)
		{
		case StatType::If: {
			CodeGen(stat.Expressions[0]);
			intptr_t jump_if_false_ins = Emit(I::JumpIfFalse, 0, 1, stat.Pos);
			intptr_t else_point = 0;
			std::vector<intptr_t> jump_if_done_instrs;
			for (auto& block_stat : stat.Statements)
			{
				if (block_stat.Type == StatType::ElseIf)
				{
					jump_if_done_instrs.push_back(Emit(I::Jump, block_stat.Pos));
					PatchToHere(jump_if_false_ins);
					CodeGen(block_stat.Expressions[0]);
					jump_if_false_ins = Emit(I::JumpIfFalse, 0, 1, block_stat.Pos);
					TryEat(KwType::Then);
				}
				else if (block_stat.Type == StatType::Else)
				{
					jump_if_done_instrs.push_back(Emit(I::Jump, block_stat.Pos));
					else_point = IP();
				}
				else
					CodeGen(block_stat);
			}
			auto end_point = IP();
			Patch(jump_if_false_ins, else_point ? else_point : end_point);
			for (auto jid : jump_if_done_instrs)
				Patch(jid, end_point);
			return;
		}
		case StatType::While: {
			auto check_address = IP();
			CodeGen(stat.Expressions[0]);
			auto jump_to_end_instr = Emit(I::JumpIfFalse, 0, 1, stat.Pos);
			for (auto& block_stat : stat.Statements)
			{
				CodeGen(block_stat);
			}
			EmitJump(check_address, stat.Pos);
			PatchToHere(jump_to_end_instr);
			return;
		}
		case StatType::For: {
			CodeGen(stat.Expressions[0]);
			CodeGen(stat.Expressions[1]);
			Emit(I::SetLocalPair, stat.Local, stat.Pos);
			auto jump_to_end_instr = Emit(I::JumpIfForDone, 0, stat.Local, stat.Pos);
			for (auto& block_stat : stat.Statements)
			{
				CodeGen(block_stat);
			}
			Emit(I::IncLocalAndJump, (jump_to_end_instr - IP()) - 1, stat.Local, stat.Pos);
			PatchToHere(jump_to_end_instr);
			return;
		}
		case StatType::Var:
			CodeGen(stat.Expressions[0]);
			Emit(I::SetLocal, stat.Local, stat.Pos);
			return;
		case StatType::Return:
			CodeGen(stat.Expressions[0]);
			Emit(I::Return, stat.Pos);
			return;
		case StatType::Assuming: {
			CodeGen(stat.Expressions[0]);
			Emit(I::AssumeCheck, (intptr_t)stat.Repr, stat.Pos);
			return;
		}
		case StatType::Expression:
			CodeGen(stat.Expressions[0]);
			Emit(I::Pop, stat.Pos);
			return;


		case StatType::ConstructorInitializer:
			mSlotsAccessible = 0;
			for (auto& init : stat.Statements)
			{
				CodeGen(init.Expressions[0]);
				Emit(I::InitField, init.SlotID, init.Pos);
				mSlotsAccessible++;
			}
			return;

		case StatType::Invalid:
		case StatType::InitField:
		case StatType::ElseIf:
		case StatType::Else:
		default:
			ThrowCodeGenException(stat.Pos, "internal error - invalid statement found: {}", int(stat.Type));
		}
	}

	bool Module::EmitThisMember(Value name, SourcePos const& pos)
	{
		if (auto it = mCurrentMethod->ParentClass->Members->Values.find(name); it != mCurrentMethod->ParentClass->Members->Values.end())
		{
			auto member = it->second;
			/// If we refer to a field of this class, turn it into a slot access
			if (member.Is(IC::Field))
			{
				auto field = member.Field;
				if (!mThisConstructed && field->SlotID >= mSlotsAccessible)
					ThrowCodeGenException(pos, "field '{}' has not been initialized at this point", field->FullyQualifiedName());
				Emit(I::GetThisMember, field->Is(FieldFlags::Const) ? (intptr_t)MemberAccess::ConstSlot : (intptr_t)MemberAccess::Slot, field->SlotID, pos);
				return true;
			}
			/// If we refer to a method of this class, turn it into a name access (for polymorphism)
			else if (member.Is(IC::Method))
			{
				auto method = member.Method;
				if (!mThisConstructed && !mCurrentMethod->ParentClass->Is(ClassFlags::Interface))
					ReportWarningPos(ReportModule::CodeGen, pos, "this class has not yet been fully constructed at this point, using member methods can result in invalid behavior");
				Emit(I::GetThisMember, (intptr_t)MemberAccess::Name, (intptr_t)method->Name, pos);
				return true;
			}
			/// If we refer to a constant of this class, just push it
			else if (member.Is(IC::Constant))
			{
				Emit(I::PushConstant, member.Constant->Value, pos);
				return true;
			}
		}
		return false;
	}

	void Module::CodeGen(ExpTree const& exp)
	{
		switch (exp.Type)
		{
		case ExpType::ConstantNull: EmitPush(Null, exp.Pos); return;
		case ExpType::ConstantFalse: EmitPush(False, exp.Pos); return;
		case ExpType::ConstantTrue: EmitPush(True, exp.Pos); return;
		case ExpType::Constant: EmitPush(exp.Val, exp.Pos); return;
		case ExpType::This: {
			Emit(I::GetLocal, 0, exp.Pos);
			return;
		}
		case ExpType::Local:
			Emit(I::GetLocal, exp.Local, exp.Pos);
			return;
		case ExpType::FreeVar: {
			auto name = MV(exp.Name);
			/// Check if the name refers to a member
			if (!EmitThisMember(name, exp.Pos))
			{
				auto ns = mCurrentMethod->mParentNamespace->FindNamespaceMember(name);
				if (ns.IsValid())
				{
					Emit(I::PushConstant, intptr_t(ns.Type), ns.Int, exp.Pos);
				}
				else
				{
					ThrowExceptionPos(ReportModule::Declaration, exp.Pos, "undeclared identifier: '{}'", exp.Name->Data);
				}
			}
			return;
		}
		case ExpType::ThisMember:
			if (!EmitThisMember(MV(exp.Name), exp.Pos))
				ThrowExceptionPos(ReportModule::Declaration, exp.Pos, "'{}' is not a member of class '{}'", exp.Name->Data, mCurrentMethod->ParentClass->FullyQualifiedName());
			return;

		case ExpType::Member:
			CodeGen(exp.Children[0]);
			Emit(I::GetMember, Value{ (Class const*)MemberAccess::Name, exp.Name }, exp.Pos); /// TODO: Store this constant in the code
			return;
		case ExpType::AtIndex:
			CodeGen(exp.Children[0]);
			CodeGen(exp.Children[1]);
			Emit(I::GetAt, exp.Pos);
			return;
		case ExpType::Call:
			for (auto& c : exp.Children)
				CodeGen(c);
			Emit(I::Call, exp.Children.size() - 1, exp.Pos);
			return;
		case ExpType::UnOp: /// TODO: Const folding
			CodeGen(exp.Children[0]);
			Emit(I::UnOp, intptr_t(exp.Op), exp.Pos);
			return;
		case ExpType::BinOp: {
			CodeGen(exp.Children[0]);

			intptr_t patch_jump = 0;
			if (exp.Op == OpType::And)
			{
				patch_jump = Emit(I::JumpIfFalse, exp.Children[0].Pos);
				Emit(I::Pop, exp.Children[0].Pos);
			}
			else if (exp.Op == OpType::Or)
			{
				patch_jump = Emit(I::JumpIfTrue, exp.Children[0].Pos);
				Emit(I::Pop, exp.Children[0].Pos);
			}

			CodeGen(exp.Children[1]);

			switch (exp.Op) /// TODO: Constant folding
			{
			case OpType::DotDot:
				///Emit(I::MakeRange, intptr_t(op)); /// TODO: Range operator
				Emit(I::Pop, exp.Pos);
				return;;
			case OpType::Star:
			case OpType::Percent:
			case OpType::Slash:
			case OpType::Plus:
			case OpType::Minus:
			case OpType::Shl:
			case OpType::Shr:
			case OpType::BinAnd:
			case OpType::BinXor:
			case OpType::BinOr:
			case OpType::Spaceship:
			case OpType::Less:
			case OpType::LessEq:
			case OpType::Greater:
			case OpType::GreaterEq:
				Emit(I::BinOp, (intptr_t)exp.Op, exp.Pos);
				/// TODO: Constant folding
				return;
			case OpType::Is: Emit(I::Is, exp.Pos); return;; /// TODO: Constant folding
			case OpType::As: Emit(I::Cast, exp.Pos); return;; /// TODO: Constant folding
			case OpType::EqualEqual: Emit(I::EqOp, exp.Pos); return;; /// TODO: Constant folding
			case OpType::NotEqual: Emit(I::NotEqOp, exp.Pos); return;; /// TODO: Constant folding
			case OpType::StrictEqual: Emit(I::StrictEqOp, exp.Pos); return;; /// TODO: Constant folding
			case OpType::NotStrictEqual: Emit(I::NotStrictEqOp, exp.Pos); return;; /// TODO: Constant folding

			case OpType::And:
			case OpType::Or:
				PatchToHere(patch_jump);
				return;
			default: ThrowCodeGenException(exp.Pos, "internal error - unknown operator");
			}
		}
		case ExpType::New: {
			/// Check for the type name given
			auto klass = mCurrentMethod->mParentNamespace->FindNamespaceMember(MV(exp.QTN));
			if (!klass.IsValid())
			{
				auto name = Module::JoinQTN(exp.QTN);
				ThrowExceptionPos(ReportModule::Declaration, exp.Pos, "class '{}' does not exist", name);
			}
			if (!klass.Is(IC::Class))
				ThrowExceptionPos(ReportModule::Declaration, exp.Pos, "name '{}' does not refer to class", klass.Declaration->FullyQualifiedName());
			if (!klass.Class->Is(ClassFlags::Constructable))
				ThrowExceptionPos(ReportModule::Declaration, exp.Pos, "objects of class '{}' are not constructable", klass.Declaration->FullyQualifiedName());

			if (klass.Class->mConstructor)
			{
				/// TODO: Check for constructor matching number of arguments to expressions given

				Emit(I::PushConstant, Null, exp.Pos);
				for (auto& arg : exp.Children)
					CodeGen(arg);
				Emit(I::New, exp.Children.size(), (intptr_t)klass.GCVal, exp.Pos);
			}
			else
			{
				Emit(I::NewNoConstructor, 0, (intptr_t)klass.GCVal, exp.Pos);
			}
			return;
		}

		case ExpType::Assign:
			CodeGen(exp.Children[0]);
			CodeGen(exp.Children[1]);
			Emit(I::SetSlot, exp.Pos);
			return;
		case ExpType::Array:
			Emit(I::PushArray, exp.Children.size(), exp.Pos);
			for (auto& el : exp.Children)
			{
				CodeGen(el);
				Emit(I::ArrayAdd, el.Pos);
			}
			return;
		case ExpType::Map:
			Emit(I::PushMap, exp.Pos);
			for (size_t i = 0; i < exp.Children.size(); i += 2)
			{
				CodeGen(exp.Children[i]);
				CodeGen(exp.Children[i + 1]);
				Emit(I::SetMap, exp.Children[i].Pos);
			}
			return;

		case ExpType::Yield: /// TODO: Const folding
			if (exp.Children.size())
				CodeGen(exp.Children[0]);
			else
				EmitPush(Null, exp.Pos);
			Emit(I::Yield, exp.Pos);
			return;

		case ExpType::Invalid:
		default:
			ThrowCodeGenException(exp.Pos, "internal error - invalid expression found: {}", int(exp.Type));
		}
	}

	intptr_t Module::Emit(I instr, intptr_t a, intptr_t b, SourcePos const& pos)
	{
		mCurrentMethod->Code.push_back({ instr, a, b, (int)pos.Line, (int)pos.Column });
		return intptr_t(mCurrentMethod->Code.size()) - 1;
	}

	intptr_t Module::Emit(I instr, Value v, SourcePos const& pos)
	{
		return Emit(instr, intptr_t(v.Type), v.Int, pos);
	}

	intptr_t Module::EmitJump(intptr_t dest_address, SourcePos const& pos)
	{
		return Emit(I::Jump, (dest_address - IP()) - 1, pos);
	}

	intptr_t Module::EmitGoto(Sym* label, SourcePos const& pos) { return mUnpatchedGotos.emplace_back(Emit(I::Jump, MV(label), pos)); }

	intptr_t Module::EmitPush(Value v, SourcePos const& pos)
	{
		return Emit(I::PushConstant, v, pos);
	}

	void Module::PatchPos(intptr_t ip, SourcePos const& pos)
	{
		mCurrentMethod->Code[ip].SourceColumn = (int)pos.Column;
		mCurrentMethod->Code[ip].SourceLine = (int)pos.Line;
	}

	intptr_t Module::IP() const { return intptr_t(mCurrentMethod->Code.size()); }

	void Module::Patch(intptr_t instruction_to_patch, intptr_t new_address)
	{
		mCurrentMethod->Code[instruction_to_patch].Param0 = new_address - (instruction_to_patch + 1);
	}

	void Module::PatchToHere(intptr_t instruction_to_patch)
	{
		Patch(instruction_to_patch, IP());
	}

	SourcePos Module::PosForIP(intptr_t ip) const
	{
		ThrowParseException("PosForIP unimplemented");
	}

}