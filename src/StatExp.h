#pragma once

#include "../include/rsl/RSL.h"
#include <array>

namespace rsl
{

	enum class ExpType
	{
		Invalid,
		ConstantNull,
		ConstantFalse,
		ConstantTrue,
		Constant,
		This,
		FreeVar,
		ThisMember,
		Member,
		AtIndex,
		Call,
		UnOp,
		BinOp,
		New,
		Assign,
		Array,
		Map,
		Local,
		Yield,
	};

	struct ExpTree
	{
		ExpTree() noexcept = default;
		ExpTree(ExpTree&&) noexcept = default;
		ExpTree& operator=(ExpTree&&) noexcept = default;

		union
		{
			Value Val{ Value::Invalid() };
			OpType Op;
			Sym* Name;
			Arr* QTN;
			intptr_t Local;
		};
		std::vector<ExpTree> Children; /// TODO: Maybe just a single linked list instead of this?
		SourcePos Pos;
		ExpType Type = ExpType::Invalid;

		bool Is(ExpType type) const { return Type == type; }
		bool IsThis() const { return Type == ExpType::This; }
		void Set(Value v) { Type = ExpType::Constant; Val = v; }
		void SetNull() { Type = ExpType::ConstantNull; }
		void SetTrue() { Type = ExpType::ConstantTrue; }
		void SetFalse() { Type = ExpType::ConstantFalse; }
		void SetArray() { Type = ExpType::Array; }
		void SetMap() { Type = ExpType::Map; }
		void SetLocal(intptr_t id) { Type = ExpType::Local; Local = id; }
		void SetThis() { Type = ExpType::This; }
		void SetFreeVar(Sym* id) { Type = ExpType::FreeVar; Name = id; }
		void SetThisMember(Sym* id) { Type = ExpType::ThisMember; Name = id; Children.clear(); Children.shrink_to_fit(); }

		void FoldThis(ExpType new_type);
		void SetMember(Sym* id) { FoldThis(ExpType::Member); Name = id; }
		void SetAtIndex(ExpTree index) { FoldThis(ExpType::AtIndex); Children.push_back(std::move(index)); }
		void SetCall() { FoldThis(ExpType::Call); }
		bool IsConstant() const {
			return Type == ExpType::ConstantNull || Type == ExpType::ConstantFalse || Type == ExpType::ConstantTrue || Type == ExpType::Constant;
		}
		bool IsTrueConstant() const { return Type == ExpType::ConstantTrue || Type == ExpType::Constant; }
		bool IsFalseConstant() const { return Type == ExpType::ConstantNull || Type == ExpType::ConstantFalse; }
		void Unary(OpType op, ExpTree exp);
		void SetBinOp(OpType op, ExpTree right);
		void SetAssign(ExpTree right, Module const& mod);
		void New(Arr* qtn);
		void Yield(ExpTree exp);
	};

	enum class StatType
	{
		Invalid,
		If,
		ElseIf,
		Else,
		While,
		For,
		Var,
		Return,
		Assuming,
		Expression,
		ConstructorInitializer,
		InitField,
	};

	struct StatTree
	{
		StatTree(SourcePos pos) noexcept : Pos(std::move(pos)) {}
		StatTree(StatTree&&) noexcept = default;
		StatTree& operator=(StatTree&&) noexcept = default;

		SourcePos Pos;
		StatType Type = StatType::Invalid;
		union
		{
			intptr_t Instruction = 0;
			intptr_t Local;
			intptr_t SlotID;
			Sym* Repr;
		};
		std::array<ExpTree, 2> Expressions;
		std::vector<StatTree> Statements;

		void SetExp(ExpTree exp) { Type = StatType::Expression; Expressions[0] = std::move(exp); }
		void SetAssuming(ExpTree exp, Sym* repr) { Type = StatType::Assuming; Expressions[0] = std::move(exp); Repr = repr; }
		void SetIf(ExpTree exp) { Type = StatType::If; Expressions[0] = std::move(exp); }
		void SetElseif(ExpTree exp) { Type = StatType::ElseIf; Expressions[0] = std::move(exp); }
		void SetElse() { Type = StatType::Else; }
		void SetWhile(ExpTree exp) { Type = StatType::While; Expressions[0] = std::move(exp); }
		void SetVar(intptr_t local) { Type = StatType::Var; Local = local; }
		void SetFor() { Type = StatType::For; }
		void SetReturn() { Type = StatType::Return; Expressions[0].SetNull(); }
		void SetReturn(ExpTree exp) { Type = StatType::Return; Expressions[0] = std::move(exp); }
		void SetConstructorInitializer(size_t field_count);
		void SetInitField(ExpTree exp, intptr_t slot_id);
	};
}