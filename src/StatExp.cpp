#include "StatExp.h"
#include "Private.h"

namespace rsl
{

	void ExpTree::FoldThis(ExpType new_type)
	{
		auto old = std::move(*this);
		Children.push_back(std::move(old));
		Type = new_type;
	}

	void ExpTree::Unary(OpType op, ExpTree exp)
	{
		assert(Is(ExpType::Invalid));

		/// Fold constants
		switch (op)
		{
		case OpType::Not:
			if (exp.IsTrueConstant())
			{
				SetFalse();
				return;
			}
			else if (exp.IsFalseConstant())
			{
				SetTrue();
				return;
			}
			break;
		case OpType::BinNot:
			if (exp.Type == ExpType::Constant && exp.Val.Is(IC::Integer))
			{
				exp.Val.Int = ~exp.Val.Int;
				Set(exp.Val);
				return;
			}
		default:
			break;
		}

		Children.push_back(std::move(exp));
		Op = op;
		Type = ExpType::UnOp;
	}

	void ExpTree::Yield(ExpTree exp)
	{
		assert(Is(ExpType::Invalid));

		Type = ExpType::Yield;
		if (!exp.Is(ExpType::Invalid))
			Children.push_back(std::move(exp));
	}

	void ExpTree::SetBinOp(OpType op, ExpTree right)
	{
		/// TODO: Fold constants
		switch (op)
		{
		case OpType::Star:
		case OpType::Percent:
		case OpType::Slash:
		case OpType::Plus:
		case OpType::Minus:
		case OpType::Shl:
		case OpType::Shr:
		case OpType::BinAnd:
		case OpType::BinXor:
		case OpType::BinOr:
		case OpType::Spaceship:
		case OpType::Less:
		case OpType::LessEq:
		case OpType::Greater:
		case OpType::GreaterEq:

		case OpType::Is:
		case OpType::As:

		case OpType::EqualEqual:
		case OpType::NotEqual:
		case OpType::StrictEqual:
		case OpType::NotStrictEqual:
			break; /// TODO
		default:
			break;
		}

		FoldThis(ExpType::BinOp);
		Children.push_back(std::move(right));
		Op = op;
	}

	void ExpTree::SetAssign(ExpTree right, Module const& mod)
	{
		switch (Type)
		{
			/// Definitely cannot assign to these
		case ExpType::Invalid:
		case ExpType::ConstantNull:
		case ExpType::ConstantFalse:
		case ExpType::ConstantTrue:
		case ExpType::Constant:
		case ExpType::Call:
		case ExpType::New:
		case ExpType::Array:
		case ExpType::Map:
		case ExpType::This:
		case ExpType::Yield:
			mod.ThrowCodeGenException(Pos, "cannot assign to this");
			break;

			/// Probably cannot assign to these?
		case ExpType::UnOp:
		case ExpType::BinOp:
			break;

			/// Can assign to these
		case ExpType::FreeVar: /// -> SetLocal(n, val) / SetSlot(0, m, val)
		case ExpType::ThisMember: /// -> SetSlot(0, n, val)
		case ExpType::Local:
		case ExpType::Member: /// -> SetSlot(n, m, val)
		case ExpType::AtIndex: /// -> SetAt(cont, ind, val)
		case ExpType::Assign: /// -> Emit(Members[0]); Emit(Members[0].Members[0], Members[1]);
			break;
		}

		FoldThis(ExpType::Assign);
		Children.push_back(std::move(right));
	}

	void ExpTree::New(Arr* qtn)
	{
		Type = ExpType::New;
		QTN = qtn;
	}

	void StatTree::SetConstructorInitializer(size_t field_count)
	{
		Type = StatType::ConstructorInitializer;
		Statements.reserve(field_count);
	}

	void StatTree::SetInitField(ExpTree exp, intptr_t slot_id)
	{
		Type = StatType::InitField;
		Expressions[0] = std::move(exp);
		SlotID = slot_id;
	}

}