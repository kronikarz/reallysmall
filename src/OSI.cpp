#undef Yield
#include "../include/rsl/OSI.h"
using namespace ghassanpl;

namespace rsl
{
	uint8_t* OSInterface::Allocate(size_t n)
	{
		return (uint8_t*)malloc(n);
	}

	uint8_t* OSInterface::Reallocate(uint8_t* data, size_t n)
	{
		return (uint8_t*)realloc(data, n);
	}

	void OSInterface::Free(uint8_t* data)
	{
		return free(data);
	}
}

#include <magic_enum.hpp>
#include <fstream>
#undef Yield
#include "Private.h"

namespace rsl
{
	void OSInterface::ReportSingle(ReportType type, ReportModule in_module, SourcePos const& pos, std::string_view message)
	{
		/// green 
		/// red 
		static const char* const colors[] = {
			"\x1b[36m",
			"\x1b[33m",
			"\x1b[31m",
			"\x1b[35m",
		};

		std::string strpos = "internal";
		if (pos.FileName)
			strpos = fmt::format("{}({},{})", pos.FileName->Data, pos.Line, pos.Column + 1);

#ifndef RSL_DEBUG
		if (type >= ReportType::Warning)
#endif
		{
			fmt::print("{} {}{}\x1b[0m: {}: {}\n", magic_enum::enum_name(in_module), colors[(int)type], magic_enum::enum_name(type), strpos, message);
		}

		//fmt::print(output, "{} {}: {}: {}\n", magic_enum::enum_name(in_module), magic_enum::enum_name(type), strpos, message);
	}

}