#include "../include/rsl/ExecutionContext.h"
#include "Private.h"
#include <gsl/gsl_assert>
#ifdef RSL_DEBUG
#include <chrono>
using namespace std::chrono;
#include <iostream>
#include <iomanip>
#endif
#include <utf8/unchecked.h>

namespace rsl
{
	struct CallFrame
	{
		Method const* Method;
		intptr_t SavedStackFrame;
		intptr_t SavedIP;
	};

#define ES() if (GSL_UNLIKELY(Top >= EndStack)) EnsureStack()

	void ExecutionContext::PushCallFrame(Method const* method, Instruction const* current_instruction, intptr_t nargs)
	{
		auto start_stack_frame = Top - nargs;

#ifdef RSL_DEBUG
		if (start_stack_frame->Unresolved())
			ThrowRuntimeException("internal compiler error: 'this' is unresolved");
#endif

		for (auto param = Top; param != start_stack_frame; --param) /// Resolve all arguments
			MakeResolved(param);

		if (CallFrames.size())
			CallFrames.back().SavedIP = current_instruction - CurrentMethod()->Code.data();
		StartFrame = start_stack_frame;
		CallFrames.push_back({ method, start_stack_frame - StartStack });

		if (!method->Native)
		{
			const auto n = (method->MaxLocalIndex - nargs) - 1;
			Top += n;
			ES();
#ifdef RSL_DEBUG
			std::fill_n((Top - n) + 1, n, LazyValue{ Null,Null });
#endif
		}
	}

	Instruction const* ExecutionContext::PopCallFrame()
	{
		Top = StartFrame;
		CallFrames.pop_back();
		if (CallFrames.size())
		{
			auto& cur_frame = CallFrames.back();
			StartFrame = cur_frame.SavedStackFrame + StartStack;
			return cur_frame.Method->Code.data() + cur_frame.SavedIP;
		}

		StartFrame = nullptr;
		return nullptr;
	}

	void ExecutionContext::EnsureStack()
	{
		const auto top_id = Top - ExecutionStack.data();
		const auto frame_id = StartFrame - ExecutionStack.data();
		ExecutionStack.resize(std::max(intptr_t(ExecutionStack.size() * 2), top_id + mMinStack));
		Top = ExecutionStack.data() + top_id;
		StartStack = ExecutionStack.data();
		EndStack = ExecutionStack.data() + ExecutionStack.size();
		StartFrame = ExecutionStack.data() + frame_id;
	}

	Value ExecutionContext::ToValue(bool const& v) const { return v ? True : False; }
	Value ExecutionContext::ToValue(intptr_t const& v) const { return Mod.MV(v); }
	Value ExecutionContext::ToValue(int32_t const& v) const { return Mod.MV(intptr_t{ v }); }
	Value ExecutionContext::ToValue(double const& v) const { return Mod.MV(v); }

	Value ExecutionContext::ToValue(int x, int y) const { return Mod.MV(x, y); }

	void ExecutionContext::Push(Value v)
	{
#ifdef RSL_DEBUG
		Mod.Report(ReportType::Trace, ReportModule::Runtime, "push({})", v.ToString());
#endif
		* ++Top = { Value{IndexType::ImmediateValue}, v };
		ES();
	}

	void ExecutionContext::PushLocalRef(intptr_t local)
	{
		Push(LazyValue{ Value{ IndexType::LocalIndex, local },{} });
	}

	void ExecutionContext::Push(LazyValue v)
	{
#ifdef RSL_DEBUG
		Mod.Report(ReportType::Trace, ReportModule::Runtime, "push({})", v.ToString(*this));
#endif
		* ++Top = v;
		ES();
	}

	void ExecutionContext::Pop(size_t n)
	{
#ifdef RSL_DEBUG
		Mod.Report(ReportType::Trace, ReportModule::Runtime, "pop({})", n);
#endif
		Top -= n;
		if (Top < StartStack - 1)
			ThrowRuntimeExceptionPos({}, "stack underflow");
	}

	ValueRef ExecutionContext::ContinueCall(std::string_view name, size_t nargs)
	{
		auto self = (Top - nargs);
		auto method = self->Value.Type->FindMethod(Mod, name);
		if (!method)
			ThrowRuntimeException("type '{}' does not contain method '{}'", self->Value.Type->ToString(), name);
		auto result = ExecuteMethod(*method, nargs);
		Pop();
		return result;
	}

	ValueRef ExecutionContext::ContinueCallConstructor(size_t nargs)
	{
		auto self = (Top - nargs);
		auto method = self->Value.Type->Constructor();
		if (!method)
			ThrowRuntimeException("type '{}' does not contain a constructor", self->Value.Type->ToString());
		auto result = ExecuteMethod(*method, nargs);
		Pop();
		return result;
	}

	void ExecutionContext::SetFieldInternal(Value container, Value member_name, Value value)
	{
		Value member = Value::Invalid();

		if (container.Is(IC::Class)) /// If we search in a class, first search in the actual class
			member = container.Class->FindMember(member_name);
		if (!member.IsValid()) /// If the container is not a class, or we didn't find the member in the class, look in the actual class of the value of the container
			member = container.RealType()->FindMember(member_name);

		if (!member.IsValid())
			ThrowRuntimeException("class '{}' has no member named '{}'", container.RealType()->FullyQualifiedName(), member_name.Symbol->Data);
		if (member.Is(IC::Method))
			ThrowRuntimeException("cannot assign to method '{}' of class '{}'", member_name.Symbol->Data, container.RealType()->FullyQualifiedName());
		else if (member.Is(IC::Constant))
			ThrowRuntimeException("cannot assign to constant '{}' of class '{}'", member_name.Symbol->Data, container.Class->FullyQualifiedName());
		auto field = member.Field;
		if (field->Is(FieldFlags::Const))
			ThrowRuntimeException("cannot assign to const field '{}'", member_name.Symbol->Data);
		if (field->Is(FieldFlags::ReadOnly) && CurrentMethod()->ParentClass != field->ParentClass)
			ThrowRuntimeException("cannot assign to read-only field '{}' outside of class method", member_name.Symbol->Data);

		container.Object->RawSlots()[field->SlotID] = value;
	}

	void ExecutionContext::ContinueSetField(std::string_view name)
	{
		const auto member_name = Mod.FindSymbol(name);
		if (!member_name)
			ThrowRuntimeException("class '{}' has no member named '{}'", Top[-1].Value.RealType()->FullyQualifiedName(), name);

		SetFieldInternal(Top[-1].Value, Mod.MV(member_name), Top->Value);
		
		Pop<2>();
	}

	ValueRef ExecutionContext::TryOverloadedOp(ValueRef const& a, ValueRef const& b, OpType op)
	{
		if (a->RealType()->OverloadsOperator(op))
		{
			auto method = a->RealType()->FindMember(Mod.OperatorNameSymbols[int(op)]);
			assert(method);
			if (method.IsValid() && method.Is(IC::Method))
			{
				auto method_ptr = method.As<Method>();
				Push(a.Value());
				Push(b.Value());
				return ExecuteMethod(*method_ptr, 1); /// TODO: Make sure everything is okay if this yields
			}
		}
		ThrowRuntimeException("could not perform operation '{}' on values of type '{}' and '{}'", Ops[int(op)].Name, a->RealType()->FullyQualifiedName(), b->RealType()->FullyQualifiedName());
	}

	ValueRef ExecutionContext::TryOverloadedOp(ValueRef const& a, OpType op)
	{
		if (a->RealType()->OverloadsOperator(op))
		{
			auto method = a->RealType()->FindMember(Mod.OperatorNameSymbols[int(op)]);
			assert(method);
			if (method.IsValid() && method.Is(IC::Method))
			{
				auto method_ptr = method.As<Method>();
				Push(a.Value());
				return ExecuteMethod(*method_ptr, 1); /// TODO: Make sure everything is okay if this yields
			}
		}
		ThrowRuntimeException("could not perform operation '{}' on value of type '{}'", Ops[int(op)].Name, a->RealType()->FullyQualifiedName());
	}

	Method const* ExecutionContext::TryOverloadedCall(ValueRef const& a, intptr_t argc)
	{
		auto method = a->RealType()->FindMember(Mod.symCall);
		if (method.IsValid() && method.Is(IC::Method))
		{
			return method.As<Method>();
		}
		ThrowRuntimeException("value of type '{}' is not callable", a->RealType()->FullyQualifiedName());
	}

	/// TODO: Should we merge this with the main Instruction set?
	ValueRef ExecutionContext::DoBinOp(ValueRef const& a, ValueRef const& b, OpType _op)
	{
		double da = 0, db = 0;

#define DONUMOP(op) \
	if (a.Value().IsInt() && b.Value().IsInt()) \
		return Mod.MV(a.Value().Int op b.Value().Int); \
	else if (a.Value().ToFloat(da) && b.Value().ToFloat(db)) \
		return Mod.MV(da op db); \
	else \
		return TryOverloadedOp(a, b, _op);

#define DOINTOP(op) \
	if (a.Value().IsInt() && b.Value().IsInt()) \
		return Mod.MV(a.Value().Int op b.Value().Int); \
	else \
		return TryOverloadedOp(a, b, _op);

		switch (_op)
		{
		case OpType::Plus: DONUMOP(+);
		case OpType::Minus: DONUMOP(-);
		case OpType::Star: DONUMOP(*);
		case OpType::Percent: DOINTOP(%);
		case OpType::Slash:
			if (a.Value().IsInt() && b.Value().IsInt())
			{
				const auto d = std::div(a.Value().Int, b.Value().Int);
				return d.rem ? Mod.MV((double)a.Value().Int / b.Value().Int) : Mod.MV(intptr_t(d.quot));
			}
			else if (a.Value().ToFloat(da) && b.Value().ToFloat(db))
				return Mod.MV(da / db);
			else
				return TryOverloadedOp(a, b, _op);
		case OpType::And: return a.Value().IsFalse() ? b.Value() : a.Value();
		case OpType::Or: return a.Value().IsFalse() ? b.Value() : a.Value();

		case OpType::Less: return (a.Value() < b.Value()) ? True : False;
		case OpType::LessEq: return (a.Value() <= b.Value()) ? True : False;
		case OpType::Greater: return (a.Value() >= b.Value()) ? True : False;
		case OpType::GreaterEq: return (a.Value() > b.Value()) ? True : False;
		case OpType::Shl: DOINTOP(<< );
		case OpType::Shr: DOINTOP(>> );
		case OpType::BinAnd: DOINTOP(&);
		case OpType::BinOr: DOINTOP(| );
		case OpType::BinXor: DOINTOP(^);
		case OpType::Spaceship: return Mod.MV(intptr_t((a.Value() < b.Value()) ? -1 : (a.Value() > b.Value() ? 1 : 0)));

		default:
			ThrowRuntimeException("unhandled op {}", Ops[(int)_op].Name);
		}
	}

	ValueRef ExecutionContext::DoUnOp(ValueRef const& b, OpType op)
	{
		switch (op)
		{
		case OpType::Not: return b->IsFalse() ? True : False;
		case OpType::BinNot:
			if (!b.Value().IsInt())
				return TryOverloadedOp(b, op);
			return Mod.MV(~b.Value().Int);
		default:
			ThrowRuntimeException("unhandled op {}", Ops[(int)op].Name);
		}
	}

	ValueRef ExecutionContext::ExecuteMethod(Method const& method, intptr_t nargs)
	{
		PushCallFrame(&method, nullptr, nargs);

		if (method.Native)
		{
			*StartFrame = method.Native(*this, This(), nargs);
			(void)PopCallFrame();
		}
		else
			DoCode(CurrentMethod()->Code.data());

		return GetResolved();
	}

	void ExecutionContext::Suspend(Instruction const* ip)
	{
		auto& cur_frame = CallFrames.back();
		cur_frame.SavedIP = ip - cur_frame.Method->Code.data();
		cur_frame.Suspended = true;
	}

	ValueRef ExecutionContext::ResumeExecution()
	{
		if (!Suspended())
			ThrowRuntimeException("trying to resume context that is not suspended");
		
		auto& cur_frame = CallFrames.back();
		cur_frame.Suspended = false;

		DoCode(cur_frame.Method->Code.data() + cur_frame.SavedIP);

		return GetResolved();
	}

	[[noreturn]]
	void ExecutionContext::ThrowRuntimeExceptionPos(SourcePos const& pos, std::string message) const
	{
		mOSI.ReportSingle(ReportType::Error, ReportModule::Runtime, pos, message);
		throw RSRuntimeException{ std::move(message) };
	}

	void ExecutionContext::Mark()
	{
#ifdef RSL_DEBUG
		mOSI.ReportSingle(ReportType::Trace, ReportModule::OSI, {}, "gc: started mark phase");
		auto start = high_resolution_clock::now();
#endif
		for (auto st = StartStack; st <= Top; ++st)
			st->Mark();
#ifdef RSL_DEBUG
		auto time = high_resolution_clock::now() - start;
		mOSI.ReportSingle(ReportType::Trace, ReportModule::OSI, {}, fmt::format("gc: mark phase took {} ns", time.count()));
#endif
	}

	void ExecutionContext::FromValue(bool& to, Value from) const
	{
		if (from.Is(IC::Boolean))
			to = !!from.Int;
		else
			ThrowRuntimeException("expected boolean, got '{}'", from.ToString());
	}

	void ExecutionContext::FromValue(intptr_t& to, Value from) const
	{
		if (from.Is(IC::Integer))
			to = from.Int;
		else
			ThrowRuntimeException("expected integer, got '{}'", from.ToString());
	}

	void ExecutionContext::FromValue(int32_t& to, Value from) const
	{
		/// TODO: Instead of casting, should we check whether the value fits into int32_t range?
		if (from.Is(IC::Integer))
			to = (int32_t)from.Int;
		else
			ThrowRuntimeException("expected integer, got '{}'", from.ToString());
	}

	void ExecutionContext::IterateOver(intptr_t index, std::function<bool(ValueRef const&, ValueRef const&)> func)
	{
		ValueRef obj = operator[](index);
		switch (obj->Type->Internal())
		{
		case IC::Array: {
			auto size = (intptr_t)obj->Array->Values.size();
			for (intptr_t i = 0; i < size; i++)
				func(Mod.MV(i), obj->Array->Values[i]);
			return;
		}
		case IC::Map:
			for (auto& [k, v] : obj->Map->Values)
				func(k, v);
			return;
		case IC::String: {
			auto size = (intptr_t)obj->String->Length;
			for (intptr_t i = 0; i < size; i++)
				func(Mod.MV(i), Mod.MV(obj->String->GetCP(i)));
			return;
		}
		case IC::Symbol:
		{
			auto data = obj->Symbol->Data;
			auto end = data + obj->Symbol->Len;
			intptr_t i = 0;
			while (data < end)
				func(Mod.MV(i++), Mod.MV((char32_t)utf8::unchecked::next(data)));
			return;
		}
		default: break;
		}

		/// Have to use iterator interface
		ThrowRuntimeException("iterating over type '{}' unimplemented yet", obj->RealType()->FullyQualifiedName());
	}

	void ExecutionContext::DoCode(Instruction const* ip)
	{
		Method const* method = nullptr;
		while (true)
		{
#ifdef RSL_DEBUG
			DumpStack();
#endif

			const auto i = *ip++;

#ifdef RSL_DEBUG
			Mod.Report(ReportType::Trace, ReportModule::Runtime, "instr: {}: {}", ((ip - 1) - CurrentMethod()->Code.data()), i.ToString());
#endif
			try
			{
#ifdef RSL_DEBUG
				if (ip - 1 >= CurrentMethod()->Code.data() + CurrentMethod()->Code.size() || ip - 1 < CurrentMethod()->Code.data())
					ThrowRuntimeException("invalid ip {} in method '{}'", (ip - 1) - CurrentMethod()->Code.data(), CurrentMethod()->FullyQualifiedName());
#endif

#define RT() Value top = MakeResolved()
#define R2() Value below_top = MakeResolved<-1>(); RT()

				switch (i.OpCode)
				{
				case I::Pop: Pop(); continue;
				case I::GetMember: { RT(); *Top = { top, i.Val() }; continue; }
				case I::GetThisMember: Push({ StartFrame->Value, i.Val() }); continue;
				case I::GetAt: { R2(); Top[-1] = { below_top, top }; Pop(); continue; }
				case I::SetSlot: { RT(); SetSlot(Top[-1].Container, Top[-1].Key, top); Pop(); continue; }
											 /// Since SetMap is only used by the table constructor, we can skip IndexSet and assign straight to the map
				case I::SetMap: { R2(); Top[-2].Value.Map->Values.insert_or_assign(below_top, top); Pop<2>(); continue; }
				case I::ReturnNull: *StartFrame = Null; goto pop_frame;
				case I::Return: { RT(); *StartFrame = top; } /// FALLTHROUGH
					pop_frame: ip = PopCallFrame();
#ifdef RSL_DEBUG
					Mod.Report(ReportType::Trace, ReportModule::Runtime, "ret");
#endif
					if (!Executing()) return; continue;
				case I::JumpIfFalse: ip += GetResolved().IsFalse() ? i.Param0 : 0; Pop(i.Param1); continue;
				case I::JumpIfTrue: ip += GetResolved().IsFalse() ? 0 : i.Param0; Pop(i.Param1); continue;
				case I::SetLocalPair: {
					R2();
					StartFrame[i.Param0] = below_top;
					StartFrame[i.Param0 + 1] = top;
					Pop<2>();
					continue;
				}
				case I::IncLocalAndJump: StartFrame[i.Param1].Value.Int++; /// FALLTHROUGH
				case I::Jump: ip += i.Param0; continue;
				case I::JumpIfForDone: ip += (StartFrame[i.Param1].Value.Int >= StartFrame[i.Param1 + 1].Value.Int) ? i.Param0 : 0; continue;
				case I::InitField: { RT(); StartFrame->Value.Object->RawSlots()[i.Param0] = top; Pop(); continue; }
				case I::SetLocal: { RT(); StartFrame[i.Param0] = top; Pop(); continue; }
				case I::PushArray: Push(Mod.MV(mHeap.New<Arr>(i.Param0))); continue;
				case I::PushMap: Push(Mod.MV(mHeap.New<Map>())); continue;
				case I::ArrayAdd: { RT(); Top[-1].Value.Array->Values.push_back(top); Pop(); continue; }
				case I::GetLocal: PushLocalRef(i.Param0); continue;
				case I::PushConstant: Push(i.Val()); continue;
				case I::BinOp: { R2(); auto result = DoBinOp(below_top, top, (OpType)i.Param0).Value(); Top[-1] = result; Pop(); continue; }
				case I::UnOp: { RT(); auto result = DoUnOp(top, (OpType)i.Param0).Value(); *Top = result; continue; }
				case I::Is: {
					Value val = MakeResolved<-1>();
					Value klass = MakeResolved();
					if (!klass.Is(IC::Class))
						ThrowRuntimeException("second operand to 'is' must be a class value, but was: '{}'", klass.RealType()->FullyQualifiedName());
					Top[-1] = (klass.Class == Mod.GetNullClass() || val.IsClass(klass)) ? True : False;
					Pop();
					continue;
				}
				case I::Cast: {
					Value val = MakeResolved<-1>();
					Value klass = MakeResolved();
					if (!klass.Is(IC::Class))
						ThrowRuntimeException("second operand to 'as' must be a class value, but was: '{}'", klass.RealType()->FullyQualifiedName());
					if (klass.Class != Mod.GetNullClass())
					{
						auto is = val.IsClass(klass);
						Top[-1] = is ? Value{ is, val.GCVal } : Null;
					}
					Pop();
					continue;
				}
				case I::EqOp: {
					Value a = MakeResolved<-1>();
					Value b = MakeResolved();
					if (a == b)
						Top[-1] = True;
					else if (a.IsClass(Mod.MV(b.Type)) || b.IsClass(Mod.MV(a.Type)))
					{
						Top[-1] = (a.Int == b.Int) ? True : False;
					}
					else
					{
						auto result = TryOverloadedOp(a, b, OpType::EqualEqual);
						Top[-1] = result;
					}
					Pop();
					break;
				}
				case I::NotEqOp: {
					Value a = MakeResolved<-1>();
					Value b = MakeResolved();
					if (a == b)
						Top[-1] = False;
					else if (a.IsClass(Mod.MV(b.Type)) || b.IsClass(Mod.MV(a.Type)))
					{
						Top[-1] = (a.Int != b.Int) ? True : False;
					}
					else
					{
						auto result = TryOverloadedOp(a, b, OpType::NotEqual);
						Top[-1] = result;
					}
					Pop();
					break;
				}
				case I::StrictEqOp: { R2(); Top[-1] = (below_top == top) ? True : False; Pop(); break; }
				case I::NotStrictEqOp: { R2(); Top[-1] = (below_top != top) ? True : False; Pop(); break; }
				case I::NewNoConstructor: Push(RawNew((Class const*)i.Param1)); continue;
				case I::New: {
					auto klass = (Class const*)i.Param1;
					method = klass->Constructor(); /// We are calling the constructor
					*(Top - i.Param0) = RawNew(klass); /// Set this for call to the new value
					goto call; /// Perform call
				}
				case I::Call: {
					auto _this = Top - i.Param0;
					ValueRef actual_thing_to_call;
					if (_this->Unresolved())
					{
						actual_thing_to_call = GetResolved(Top[-i.Param0]);
						*_this = _this->Container;

						if (actual_thing_to_call->Is(IC::Method))
						{
							method = actual_thing_to_call->Method;
							goto call;
						}
						else
							method = nullptr;
					}
					else
					{
						actual_thing_to_call = _this->Value;
						method = nullptr;
					}

					method = TryOverloadedCall(actual_thing_to_call, i.Param0);
				}
				call:
#ifdef RSL_DEBUG
					Mod.Report(ReportType::Trace, ReportModule::Runtime, "call: {}({}, ...{} args)", method->FullyQualifiedName(), StartFrame->ToString(*this), i.Param0);
#endif
					if (i.Param0 < method->ParamCount)
						ThrowRuntimeException("too few arguments to method '{}', {} expected, {} provided", method->FullyQualifiedName(), method->ParamCount, i.Param0);
					if (!method->Is(MethodFlags::VarArgs) && i.Param0 > method->ParamCount)
						ThrowRuntimeException("too many arguments to method '{}', {} expected, {} provided", method->FullyQualifiedName(), method->ParamCount, i.Param0);
					PushCallFrame(method, ip, i.Param0);
					if (method->Native)
					{
						*StartFrame = method->Native(*this, This(), i.Param0);
						ip = PopCallFrame();
#ifdef RSL_DEBUG
						Mod.Report(ReportType::Trace, ReportModule::Runtime, "ret: {}", Top->ToString(*this));
#endif
					}
					else
						ip = CurrentMethod()->Code.data();
					continue;
				case I::AssumeCheck: {
					RT();
					Pop();
					if (top.IsFalse())
						ThrowRuntimeException("assumption failed: {}", ((Sym*)i.Param0)->Data);
					continue;
				}
				case I::Yield: { MakeResolved(); Suspend(ip); return; }
				case I::AssumeStart:
				case I::NOP:
					continue; /// nop
				case I::DbgVal: /// FALLTHROUGH
				case I::BreakPoint:
				case I::MakeRange:
					ThrowRuntimeException("unimplemented opcode {} ({})", (int)i.OpCode, Is[(int)i.OpCode]);
				default:
					ThrowRuntimeException("invalid opcode {} ({})", (int)i.OpCode, Is[(int)i.OpCode]);
				}
			}
			catch (RSRuntimeException& e)
			{
#ifdef RSL_DEBUG
				DumpStack();
#endif

				auto method = GetLastNonNativeMethod();
				assert(method);
				e.MethodName = method->FullyQualifiedName();
				e.Line = i.SourceLine;
				e.Column = i.SourceColumn;
				if (method->Native)
					e.IP = 0;
				else
					e.IP = (int)(ip - method->Code.data()) - 1;
				e.FileName = method->DeclPos().FileName->Data;
				throw;
			}
		}
	}

	Method const* ExecutionContext::GetLastNonNativeMethod()
	{
		for (auto it = CallFrames.rbegin(); it != CallFrames.rend(); ++it)
			if (!it->Method->Native)
				return it->Method;
		return nullptr;
	}

	/// TODO: Make sure all this is aggressively inlined

	Value ExecutionContext::GetResolved(LazyValue const* v)
	{
	start:
		switch ((IndexType)(intptr_t)v->Container.Type)
		{
		case IndexType::ImmediateValue: return v->Value;
		case IndexType::LocalIndex: v = StartFrame + v->Container.Int; goto start;
		default: return GetSlotRaw(v->Container, v->Key);
		}
	}

	ValueRef ExecutionContext::operator[](intptr_t index)
	{
		const auto stack_elems = Top - StartFrame;
		if (index < 0)
			index = index + stack_elems; /// Should this be mCurrentCallFrame->StartStack ?
		if (index < 0 || index > stack_elems)
			ThrowRuntimeException("invalid function stack index {}, stack element count: ", index, stack_elems);
		return GetResolved(StartFrame[index]);
	}

	Value ExecutionContext::GetSlotRaw(Value container, Value key)
	{
		/// Member access
		if (key.Is(MemberAccess::Name))
		{
			const auto member_name = Mod.MV(key.Symbol);
			if (container.Is(IC::Namespace))
			{
				auto existing = container.Namespace->FindNamespaceMemberSelfOnly(member_name.Symbol);
				if (existing.IsValid())
					return existing;
				ThrowRuntimeException("namespace '{}' has no type named '{}'", container.RealType()->FullyQualifiedName(), member_name.Symbol->Data);
			}
			else
			{
				Value member = Value::Invalid();

				if (container.Is(IC::Class)) /// If we search in a class, first search in the actual class
					member = container.Class->FindMember(member_name);
				if (!member.IsValid()) /// If the container is not a class, or we didn't find the member in the class, look in the actual class of the value of the container
					member = container.RealType()->FindMember(member_name);

				if (!member.IsValid())
					ThrowRuntimeException("class '{}' has no member named '{}'", container.RealType()->FullyQualifiedName(), member_name.Symbol->Data);
				if (member.Is(IC::Method))
					return Mod.MV(member.Method);
				else if (member.Is(IC::Constant))
					return member.Constant->Value;
				else
					return container.Object->RawSlots()[member.Field->SlotID];
			}
		}
		//else if (key.Is(MemberAccess::Method))
		//	return Mod.MV(key.Method);
		//else if (key.Is(MemberAccess::Field))
		//	return Mod.MV(key.Field);
		else if (key.Is(MemberAccess::Slot) || key.Is(MemberAccess::ConstSlot))
			return StartFrame->Value.Object->RawSlots()[key.Int];
		/// At access
		else if (container.Type->Is(ClassFlags::NotGC))
			ThrowRuntimeException("class '{}' cannot be indexed", container.RealType()->FullyQualifiedName());
		else
		{
			return container.GCVal->IndexGet(*this, key);
		}
	}

	void ExecutionContext::SetSlot(Value container, Value key, Value value)
	{
		/// Member access
		if (container.Is(IndexType::ImmediateValue))
			ThrowRuntimeException("internal: trying to set to immediate value");
		else if (container.Is(IndexType::LocalIndex))
			StartFrame[container.Int] = value;
		else if (key.Is(MemberAccess::Name))
		{
			if (container.Is(IC::Namespace))
				ThrowRuntimeException("cannot assign to namespace members");
			else
			{
				const auto member_name = Mod.MV(key.Symbol);

				SetFieldInternal(container, member_name, value);

				/*
				Value member = Value::Invalid();

				if (container.Is(IC::Class)) /// If we search in a class, first search in the actual class
					member = container.Class->FindMember(member_name);
				if (!member.IsValid()) /// If the container is not a class, or we didn't find the member in the class, look in the actual class of the value of the container
					member = container.RealType()->FindMember(member_name);

				//auto member = container.RealType()->FindMember(member_name);
				if (!member.IsValid())
					ThrowRuntimeException("class '{}' has no member named '{}'", container.RealType()->FullyQualifiedName(), member_name.Symbol->Data);
				if (member.Is(IC::Method))
					ThrowRuntimeException("cannot assign to method '{}' of class '{}'", member_name.Symbol->Data, container.RealType()->FullyQualifiedName());
				else if (member.Is(IC::Constant))
					ThrowRuntimeException("cannot assign to constant '{}' of class '{}'", member_name.Symbol->Data, container.Class->FullyQualifiedName());
				auto field = member.Field;
				if (field->Is(FieldFlags::Const))
					ThrowRuntimeException("cannot assign to const field '{}'", member_name.Symbol->Data);
				if (field->Is(FieldFlags::ReadOnly) && CurrentMethod()->ParentClass != field->ParentClass)
					ThrowRuntimeException("cannot assign to read-only field '{}' outside of class method", member_name.Symbol->Data);

				container.Object->RawSlots()[field->SlotID] = value;
				*/
			}
		}
		/*
		else if (key.Is(MemberAccess::Method))
			ThrowRuntimeException("cannot assign to method '{}'", key.Method->FullyQualifiedName());
		else if (key.Is(MemberAccess::Field))
		{
			auto field = key.Field;
			if (field->Is(FieldFlags::Const))
				ThrowRuntimeException("cannot assign to const field '{}'", field->FullyQualifiedName());
			container.Object->RawSlots()[field->SlotID] = value;
		}
		*/
		else if (key.Is(MemberAccess::Slot))
			StartFrame->Value.Object->RawSlots()[key.Int] = value;
		else if (key.Is(MemberAccess::ConstSlot))
		{
			auto field = StartFrame->Value.Object->Class->FindFieldBySlot(key.Int); assert(field);
			ThrowRuntimeException("cannot assign to const field '{}'", field->FullyQualifiedName());
		}
		/// At access
		else if (container.Type->Is(ClassFlags::NotGC))
			ThrowRuntimeException("class '{}' cannot be indexed", container.RealType()->FullyQualifiedName());
		else
			return container.GCVal->IndexSet(*this, key, value);
	}

	Value ExecutionContext::RawNew(Class const* klass)
	{
		if (!klass->Is(ClassFlags::Constructable))
			ThrowRuntimeException("objects of class '{}' are not constructable", klass->FullyQualifiedName());

		switch (klass->Internal())
		{
		case IC::Null: return Null;

		case IC::Float: return Mod.ZeroFloat;
		case IC::Integer: return Mod.ZeroInt;
		case IC::Boolean: return False;
		case IC::Codepoint: return Mod.ZeroCodepoint;
		case IC::Point: return Mod.ZeroPoint;
		case IC::Range: return Mod.ZeroRange;
		case IC::UserPointer: return Mod.ZeroUserPointer;

		case IC::Array: return Mod.MV(mHeap.New<Arr>());
		case IC::Map: return Mod.MV(mHeap.New<Map>());
		case IC::String: return Mod.MV(mHeap.New<Str>());

		case IC::UserClass: return klass->PreConstruct(*this);
		default:
			ThrowRuntimeException("internal");
		}
	}

	std::string RSRuntimeException::ToString() const
	{
		if (IP != -1)
			return fmt::format("{}\n\tat instruction #{} of method {}", RSException::ToString(), IP, MethodName);
		else
			return RSException::ToString();
	}

	std::string ExecutionContext::LazyValue::ToString(ExecutionContext const& vm) const
	{
		switch ((IndexType)(intptr_t)Container.Type)
		{
		case IndexType::ImmediateValue: return Key.ToString();
		case IndexType::LocalIndex: return "{ local #" + std::to_string(Container.Int) + ": " + vm.StartFrame[Container.Int].Value.ToString() + " }";
		default: return "{" + Container.ToString() + ", " + Key.ToString() + "}";
		}
	}

	void ExecutionContext::DumpStack()
	{
		std::string st;
		for (auto base = StartStack; base <= Top; base++)
		{
			char c[2] = "?";
			if (base == StartFrame) c[0] = 'T';
			else if (base > StartFrame)
			{
				if (!CallFrames.empty() && base - StartFrame < CurrentMethod()->MaxLocalIndex)
					c[0] = 'L';
				else
					c[0] = '*';
			}
			st += fmt::format("{}{} = {}, ", c, base - StartStack, base->ToString(*this));
		}
		Mod.Report(ReportType::Trace, ReportModule::Runtime, "stack: {}", st);
	}

}