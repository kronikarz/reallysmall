#pragma once
#include <string_view>
#include <algorithm>
#include <set>
#include "../include/rsl/RSL.h"
#include "../include/rsl/CppInterop.h"
#include "StatExp.h"
#include <functional>
#include <cassert>
#include <gsl/span>

using namespace ghassanpl;

namespace rsl
{
	enum class I
	{
#define OPCODE(i) i,
#include "OpCodes.h"
#undef OPCODE
	};

	inline std::string_view Is[] = {
	#define OPCODE(i) #i,
	#include "OpCodes.h"
	#undef OPCODE
	};

	template <typename T>
	inline constexpr std::string_view make_sv(T start, T end) noexcept { return std::string_view{ std::to_address(start), static_cast<size_t>(end - start) }; }
	inline std::string_view trim_whitespace_right(std::string_view str) noexcept { return make_sv(str.begin(), std::find_if_not(str.rbegin(), str.rend(), ::isspace).base()); }
	inline std::string_view trim_whitespace_left(std::string_view str) noexcept { return make_sv(std::find_if_not(str.begin(), str.end(), ::isspace), str.end()); }
	inline std::string_view trim_whitespace(std::string_view str) noexcept { return trim_whitespace_left(trim_whitespace_right(str)); }

	struct TokProps
	{
		const char* Name = {};
		uint64_t Flags = 0;
		std::pair<int, int> Precedence = { 0,0 };
	};

	enum class OpType : intptr_t
	{
#define OPERATOR(x, ...) x,
#include "../include/rsl/Operators.h"
#undef OPERATOR
		Count
	};

	static_assert((int)OpType::Count <= 64);

	enum class OpFlags
	{
		Binary,
		Unary,
		RightAssoc,
	};

	enum class KwType : intptr_t
	{
#define KEYWORD(k) k,
#include "../include/rsl/Keywords.h"
#undef KEYWORD
		Count
	};

	extern const TokProps Toks[];
	extern const TokProps Ops[];
	extern const TokProps Kws[];

	enum class MemberAccess : intptr_t
	{
		Name = 1,
		//Field,
		//Method,
		Slot,
		ConstSlot
	};

	/*
	template <typename T>
	std::string to_string(T&& t)
	{
		if constexpr (std::is_constructible_v<std::string_view, std::decay_t<T>>)
			return (std::string)std::string_view{ std::forward<T>(t) };
		else if constexpr (std::is_pointer_v<std::decay_t<T>>)
			return std::to_string((uintptr_t)t);
		else if constexpr (std::is_same_v<SourcePos, std::decay_t<T>>)
			return t.ToString();
		else
			return std::to_string(std::forward<T>(t));
	}
	*/

	/*
	template <typename T>
	std::string to_string(std::vector<T>& t)
	{
		std::string str;
		for (auto const& s : t)
		{
			str += to_string(s);
			str += '\n';
		}
		return str;
	}
	*/

	struct Sym final : GCBase
	{
		const size_t Hash = 0;
		const size_t Len = 0;
		uint8_t Kw = 0; uint8_t Op = 0;
		const char Data[1];

		explicit Sym(std::string_view str);
		virtual std::string ToString() const override;

		virtual void IndexSet(ExecutionContext& vm, Value key, Value val) override;
		virtual Value IndexGet(ExecutionContext& vm, Value key) const override;
		virtual intptr_t IndexSize(ExecutionContext& vm) const override { return Len; }
		virtual void MarkMembers() override { }
	};

	/// Smart smart python

	enum class StringFlags
	{
		StringEnc1 = StartCustomFlags,
		StringEnc2,
	};


	struct Str final : GCBase
	{
		enum StringEncoding : uint64_t
		{
			Latin1,
			UBMP,
			UTF32,
		};

		static inline constexpr intptr_t ByteWidth[3] = { sizeof(char8_t),sizeof(char16_t),sizeof(char32_t) };

		intptr_t Length = 0;
		std::vector<uint8_t> Bytes;

		StringEncoding Encoding() const noexcept { return StringEncoding((Flags >> StartCustomFlags) & 3); }

		/// Returns actual final encoding
		StringEncoding SwitchToEncoding(StringEncoding requested_encoding);

		StringEncoding MinimizeEncoding(); /// TODO

		Value Push(Value val);
		Value Insert(intptr_t at, Value val);
		void Resize(intptr_t s, Value v);
		Value Erase(intptr_t s);
		Value Pop(ExecutionContext const& vm);
		void Clear();

		virtual void IndexSet(ExecutionContext& vm, Value key, Value val) override;
		virtual Value IndexGet(ExecutionContext& vm, Value key) const override;
		virtual intptr_t IndexSize(ExecutionContext& vm) const override { return Length; }
		virtual void MarkMembers() override {}
		virtual std::string ToString() const override;

		char32_t GetCP(intptr_t at) const;

	private:

		void SetEncoding(StringEncoding enc) noexcept;

		uint8_t* Latin1View() { return Bytes.data(); }
		char16_t* UBMPView() { return (char16_t*)Bytes.data(); }
		char32_t* UTF32View() { return (char32_t*)Bytes.data(); }

		const uint8_t* Latin1View() const { return Bytes.data(); }
		const char16_t* UBMPView() const { return (char16_t*)Bytes.data(); }
		const char32_t* UTF32View() const { return (char32_t*)Bytes.data(); }

		char32_t EnsureValidCodepoint(Value v);
	};

	enum class MapFlags { Ephemeral = StartCustomFlags };

	struct Map final : GCBase
	{
		std::map<Value, Value> Values;

		explicit Map(bool permanent = false);
		virtual void IndexSet(ExecutionContext& vm, Value key, Value val) override;
		virtual Value IndexGet(ExecutionContext& vm, Value key) const override;
		virtual std::string ToString() const override;
		virtual void MarkMembers() override;
	};

	struct Arr final : GCBase
	{
		std::vector<Value> Values;

		Arr() = default;
		Arr(intptr_t n) { Values.reserve((size_t)n); }
		Value Push(Value val);
		Value Insert(intptr_t at, Value val);
		void Resize(intptr_t s, Value v);
		Value Erase(intptr_t s);
		Value Pop(ExecutionContext const& vm);
		void Clear();
		virtual std::string ToString() const override;

		intptr_t CheckIndex(intptr_t index, intptr_t size) const;
		intptr_t CheckIndex(intptr_t index) const { return CheckIndex(index, intptr_t(Values.size())); }

		virtual void IndexSet(ExecutionContext& vm, Value key, Value val) override;
		virtual Value IndexGet(ExecutionContext& vm, Value key) const override;
		virtual intptr_t IndexSize(ExecutionContext& vm) const override { return intptr_t(Values.size()); }
		virtual void MarkMembers() override;
	};

	struct Instruction
	{
		I OpCode;
		intptr_t Param0 = 0;
		intptr_t Param1 = 0;
		int SourceLine = 0;
		int SourceColumn = 0;

		std::string ToString() const;
		Value Val() const { return Value{ (Class const*)Param0, Param1 }; }
	};

	enum class MethodFlags
	{
		VarArgs = StartCustomFlags,
		Final,
		Abstract,
		Static,
		Const,
	};

	struct Method final : Declaration
	{
		Method(const Method& other) = default;
		/// Native method
		Method(Class* parent_class, Sym* name, uint32_t flags = 0);

		/// Source method
		Method(Class* parent_class, SourcePos const& pos, Sym* name, uint32_t flags = 0);

		Class* ParentClass = nullptr;
		intptr_t ParamCount = 0;

		void Link(Module& pr);

		bool Is(MethodFlags fl) const noexcept { return is_flag_set(Flags, fl); }
		void Set(MethodFlags fl) noexcept { set_flags(Flags, fl); }
		void Unset(MethodFlags fl) noexcept { unset_flags(Flags, fl); }

		virtual std::string ToString() const override;

		/// Codegen

		NativeMethod Native = {};
		std::vector<Instruction> Code;
		std::shared_ptr<std::vector<StatTree>> Statements;
		//std::vector<std::pair<Sym*, SourcePos>> Parameters;
		intptr_t MaxLocalIndex = 1;

		/// TODO: If we're creating this from a file, we can just allocate NewSized<Method>(instruction_count) and copy it here!
		/// Instruction CodeInline[0];
	};


	enum class FieldFlags
	{
		ReadOnly = StartCustomFlags,
		Const,
		Static,
	};

	struct InternalClass final : Class
	{
		Map InternalMembers;
		InternalClass(Namespace* pn, IC internal, Sym* name, uint32_t flags)
			: Class(pn, internal, name, flags, &InternalMembers, nullptr)
		{
		}
	};

	struct Field final : Declaration
	{
		Field(Class* parent_class, SourcePos const& declpos, Sym* name);

		Class* ParentClass = nullptr;
		intptr_t SlotID = 0;
		ExpTree InitExp;
		virtual std::string ToString() const override;

		bool Is(FieldFlags fl) const noexcept { return ghassanpl::is_flag_set(Flags, fl); }
		void Set(FieldFlags fl) noexcept { ghassanpl::set_flags(Flags, fl); }
		void Unset(FieldFlags fl) noexcept { ghassanpl::unset_flags(Flags, fl); }
	};

	struct Constant final : Declaration
	{
		Constant(Class* parent_class, SourcePos const& declpos, Sym* name);

		Class* ParentClass = nullptr;
		//ExpTree InitExp;
		Value Value = Value::Invalid();

		virtual std::string ToString() const override
		{
			return "const";
		}
	};

	struct Obj : GCBase
	{
		Obj(rsl::Class const* klass);
		~Obj();

		Class const* Class = nullptr;

		void* NativeObject();
		std::span<Value> Slots();
		Value* RawSlots() { return (Value*)(mAdditionalData + Class->SlotsOffset()); };

		Obj* FromNative(void* obj);

		virtual std::string ToString() const override;

		virtual void IndexSet(ExecutionContext& vm, Value key, Value val) override;
		virtual Value IndexGet(ExecutionContext& vm, Value key) const override;
		virtual void MarkMembers() override;

	protected:

		alignas(Value) char mAdditionalData[1];
	};

	inline bool Value::Is(IC cl) const noexcept { return Type->Internal() == cl; }

	inline Class const* Value::IsClass(Value cl) const noexcept
	{
		assert(cl.Is(IC::Class));
		auto const* type = RealType();
		return (type == cl.Class || type->ImplementsClasses()->Values.contains(cl)) ? type : nullptr;
	}

	inline void Value::Mark() const
	{
		/// Note: We don't mark type as types are permanent
		if (!((intptr_t)Type < 10 || Type->Is(ClassFlags::NotGC)))
			GCVal->Mark();
	}

	inline bool Value::IsFalse() const noexcept {
		return Int == 0 && (Type->Internal() == IC::Boolean || Type->Internal() == IC::Null);
	}
	inline bool Value::IsNull() const noexcept { return Type->Internal() == IC::Null; }
	inline bool Value::IsInt() const { return Type->Internal() == IC::Integer; }
	inline bool Value::IsFloat() const { return Type->Internal() == IC::Float; }
	inline bool Value::ToFloat(double& d) const {
		switch (Type->Internal())
		{
		case IC::Integer: d = (double)Int; return true;
		case IC::Float: d = Float; return true;
		default: return false;
		}
	}

	inline rsl::Class const* Value::RealType() const
	{
		if (Is(IC::UserClass))
		{
			if (!Type->Is(ClassFlags::Native) || ((rsl::NativeClass*)Type)->Type() == NativeClassType::FullType)
				return Object->Class;
		}
		return Type;
	}

	template<typename ...ARGS>
	void Module::ReportWarningPos(ReportModule in_module, SourcePos const& pos, ARGS&& ...args) const
	{
		auto message = fmt::format(std::forward<ARGS>(args)...);
		mOSI.ReportSingle(ReportType::Warning, in_module, pos, message);
	}

	template<typename ...ARGS>
	[[noreturn]] void Module::ThrowExceptionPos(ReportModule in_module, SourcePos const& pos, ARGS&& ...args) const
	{
		auto message = fmt::format(std::forward<ARGS>(args)...);
		mOSI.ReportSingle(ReportType::Error, in_module, pos, message);
		throw RSException{ pos, message };
	}

	template <typename... ARGS>
	void Module::ReportDeclError(Declaration* decl, ARGS&&... args) const
	{
		ThrowExceptionPos(ReportModule::Declaration, decl->mDeclPos, std::forward<ARGS>(args)...);
	}

}