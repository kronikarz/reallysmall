#pragma once
#include <map>
#include <typeindex>
#include <functional>

#ifndef NDEBUG
#define RSL_DEBUG
#endif
#ifndef RSL_DEBUG
#if defined(__INTELLISENSE__) || !defined(__clang__)
#define TSL_HT_HAS_STRING_VIEW 1
#endif
#include "X:\Code\Native\reallysmall\lib\hat-trie\include\tsl\htrie_map.h"
#else
#include <string>
#include <vector>
#endif
#include <span>

#include "OSI.h"
#include "GCHeap.h"
#include "Format.h"

namespace rsl
{
	struct Module;
	struct GCBase;
	struct Class;
	struct NativeClass;
	struct Method;
	struct Field;
	struct Constant;
	struct Arr;
	struct Map;
	struct Obj;
	struct Str;
	struct Sym;
	struct Declaration;
	struct Namespace;
	struct ExecutionContext;
	struct ExpTree;
	struct StatTree;
	enum class I;
	enum class IndexType : intptr_t;
	enum class MemberAccess : intptr_t;
	enum class NativeClassType;

	struct RSException
	{
		intptr_t Line = 0, Column = 0;
		std::string FileName;
		std::string Message;
		std::vector<std::string> SubLines;
		template <typename... ARGS>
		RSException(std::string_view fmt, ARGS&&... args) : Message(fmt::format(fmt, std::forward<ARGS>(args)...)) {}
		RSException(SourcePos const& pos, std::string_view msg);

		virtual ~RSException() noexcept = default;
		virtual std::string ToString() const;
	};

	enum class OpType : intptr_t;
	enum class KwType : intptr_t;

	enum class TokType
	{
#define TOKEN(a) a,
#include "Tokens.h"
#undef TOKEN
		Count
	};

	struct Value
	{
		union
		{
			struct Class const* Type;
			IndexType IType;
		};
		union
		{
			intptr_t Int;
			char32_t CP;
			OpType Op;
			KwType Kw;
			static_assert(sizeof(intptr_t) == sizeof(float) * 2);
			struct { float X; float Y; };
			static_assert(sizeof(intptr_t) == sizeof(int32_t) * 2);
			struct { int32_t StartRange; int32_t EndRange; };
			struct { int32_t IX; int32_t IY; };
			double Float;
			GCBase* GCVal;
			Class* Class;
			NativeClass* NativeClass;
			Method* Method;
			Field* Field;
			Constant* Constant;
			Arr* Array;
			Map* Map;
			Obj* Object;
			Str* String;
			Sym* Symbol;
			Declaration* Declaration;
			Namespace* Namespace;
			void* User;
		};

		static inline constexpr Value Invalid() { return Value{ nullptr }; }

		template <typename T>
		constexpr T* As() const noexcept
		{
			return static_cast<T*>(GCVal);
		}

		constexpr explicit Value(IndexType type) : IType(type), Int(0) {}
		Value() noexcept = default;
		constexpr Value(const Value& v) noexcept = default;
		constexpr Value(Value&& v) noexcept = default;
		constexpr Value& operator=(const Value&) noexcept = default;
		constexpr Value& operator=(Value&&) noexcept = default;

		constexpr Value(rsl::Class const* type, double val) noexcept : Type(type), Float(val) { }
		constexpr Value(rsl::Class const* type, GCBase* val) noexcept : Type(type), GCVal(val) { }
		constexpr Value(rsl::Class const* type, intptr_t val) noexcept : Type(type), Int(val) { }
		template <typename T, typename = std::enable_if_t<std::is_same_v<void, T>>>
		constexpr Value(rsl::Class const* type, T* val) noexcept : Type(type), User(val) { }
		template <typename T, typename = std::enable_if_t<std::is_enum_v<T>>>
		constexpr Value(T ma, GCBase* val) noexcept : Type((rsl::Class*)ma), GCVal(val) { }
		template <typename T, typename = std::enable_if_t<std::is_enum_v<T>>>
		constexpr Value(T ma, intptr_t val) noexcept : Type((rsl::Class*)ma), Int(val) { }

		constexpr Value(rsl::Class const* type, int32_t x, int32_t y) noexcept : Type(type), IX(x), IY(y) { }
		constexpr Value(rsl::Class const* type, float x, float y) noexcept : Type(type), X(x), Y(y) { }

		bool Is(enum class IC cl) const noexcept;
		rsl::Class const* IsClass(Value cl) const noexcept;

		bool Is(MemberAccess ma) const noexcept { return (MemberAccess)(intptr_t)Type == ma; }
		bool Is(IndexType ma) const noexcept { return (IndexType)(intptr_t)Type == ma; }

		void Mark() const;

		constexpr bool operator==(Value const& other) const noexcept { return Type == other.Type && Int == other.Int; }
		constexpr bool operator!=(Value const& other) const noexcept { return !(Type == other.Type && Int == other.Int); }
		constexpr auto operator<(Value const& other) const noexcept { return std::make_tuple(Type, Int) < std::make_tuple(other.Type, other.Int); }
		constexpr auto operator<=(Value const& other) const noexcept { return std::make_tuple(Type, Int) <= std::make_tuple(other.Type, other.Int); }
		constexpr auto operator>(Value const& other) const noexcept { return std::make_tuple(Type, Int) > std::make_tuple(other.Type, other.Int); }
		constexpr auto operator>=(Value const& other) const noexcept { return std::make_tuple(Type, Int) >= std::make_tuple(other.Type, other.Int); }

		constexpr bool IsValid() const noexcept { return Type != nullptr; }
		bool IsFalse() const noexcept;
		bool IsNull() const noexcept;
		explicit operator bool() const noexcept;

		bool IsInt() const;
		bool IsFloat() const;
		bool ToFloat(double& d) const;

		rsl::Class const* RealType() const;

		std::string ToString() const;

	private:
		
		constexpr explicit Value(std::nullptr_t) : Type(nullptr), Int(0) {}

	};

	struct Declaration : GCBase
	{
		virtual void MarkMembers() override;

		std::string_view FullyQualifiedName() const { return mFullyQualifiedName; }

		Namespace const* ParentNamespace() const { return mParentNamespace; }

		SourcePos DeclPos() const { return mDeclPos; }

	protected:

		Declaration(Namespace const* pn, Sym* name, uint32_t flags = 0);
		Declaration(Namespace const* pn, SourcePos const& pos, Sym* name, uint32_t flags = 0);

		friend struct Module;

		std::string mFullyQualifiedName;
		Sym* Name = nullptr;
		Namespace const* mParentNamespace = nullptr;
		SourcePos mDeclPos;
	};

	struct Namespace final : Declaration
	{
		/// Can return Value::Invalid()
		Value FindNamespaceMember(Value name_or_array) const;

		/// Can return Value::Invalid()
		Value FindNamespaceMemberSelfOnly(Value name_or_array, size_t start_array_at = 0) const;

		/// Can return Value::Invalid()
		Value FindNamespaceMemberSelfOnly(Sym const* name) const;

		Namespace const* GlobalNamespace() const;

		virtual std::string ToString() const override;

	protected:

		friend struct Module;
		friend struct GCHeap;

		Namespace(Namespace* pn, Sym* name);

		std::map<Sym*, Value, std::less<>> Members;

	};

	struct NativeClassDescriptor;

	enum class ClassFlags
	{
		Internal = StartCustomFlags,
		Interface,
		Constructable,
		NotGC,
		Final,
		Abstract,
		Polymorphic, /// TODO: Set these for script classes that override base class methods
		Trivial, /// TODO: Set these for script classes that don't have a copy/clone overloaded op
		Linked,
		Native,
	};

	enum class IC
	{
		UserClass,
		Null,
#define INTERNALCLASS(name, ...) name,
#include "InternalClasses.h"
#undef INTERNALCLASS
		Count
	};

	struct Class : Declaration
	{
		Class(Namespace* pn, SourcePos const& pos, Sym* name, uint32_t flags);
		Class(Namespace* pn, Sym* name, uint32_t flags) : Class(pn, SourcePos{}, name, flags) {}
		virtual std::string ToString() const override;

		virtual Value PreConstruct(ExecutionContext& vm) const;

		bool Is(ClassFlags fl) const noexcept { return ghassanpl::is_flag_set(Flags, fl); }

		/// Can return Value::Invalid()
		Value FindMember(Value name) const;
		Value FindMemberUnlinked(Value name) const;

		Method* FindMethod(Module const& mod, std::string_view name) const;

		Field const* FindFieldBySlot(intptr_t slot) const;

		IC Internal() const { return mInternal; }

		intptr_t SlotsOffset() const { return mSlotsOffset; }
		intptr_t SlotCount() const { return mSlotCount; }

		Map* ImplementsClasses() const { return mImplementsClasses; }

		bool OverloadsOperator(OpType op) const { return OverloadsOperators.is_set(op); }

		Method* Constructor() const { return mConstructor; }

	protected:

		void Set(ClassFlags fl) noexcept { ghassanpl::set_flags(Flags, fl); }
		void Unset(ClassFlags fl) noexcept { ghassanpl::unset_flags(Flags, fl); }

		//friend struct Obj;
		//friend struct Value;
		friend struct Module;
		//friend struct ExecutionContext;

		IC mInternal = IC::UserClass;

		Value mBaseClassName = Value::Invalid();
		Class* mBaseClass = nullptr;
		Map* Members = nullptr;
		Method* mConstructor = nullptr;
		intptr_t mSlotCount = 0;
		intptr_t mSlotsOffset = 0;
		ghassanpl::enum_flags<OpType> OverloadsOperators;
		Map* mImplementsClasses = nullptr;
		std::map<Sym*, SourcePos> RequiresMembers;

		std::function<Value(ExecutionContext&, void const*)> mToValue;
		std::function<void(ExecutionContext&, void*, Value)> mFromValue;

		Class(Namespace* pn, IC internal, Sym* name, uint32_t flags, Map* members, Method* constructor); /// <- internal class constructor
	};

	struct ValueRef
	{
	public:

		ValueRef() : mValue(Value::Invalid()) {}
		ValueRef(Value v) : mValue(v) { AddRef(); }
		ValueRef(ValueRef const& other) { mValue = other.mValue; AddRef(); }
		ValueRef(ValueRef&& other) { mValue = other.mValue; other.mValue = Value::Invalid(); }
		ValueRef& operator=(ValueRef const& other) { auto old = std::exchange(mValue, other.mValue); AddRef(); Release(old); return *this; }
		ValueRef& operator=(ValueRef&& other) { 
			auto old = std::exchange(mValue, other.mValue); 
			other.mValue = Value::Invalid(); 
			Release(old); 
			return *this; 
		}
		~ValueRef() { Release(mValue); }

		Value const* operator->() const { return &mValue; }
		Value* operator->() { return &mValue; }

		Value Value() const { return mValue; }

	protected:

		void AddRef() { if (mValue.IsValid() && !mValue.Type->Is(ClassFlags::NotGC)) mValue.GCVal->AddRef(); }
		void Release(rsl::Value& v) { if (v.IsValid() && !v.Type->Is(ClassFlags::NotGC)) v.GCVal->Release(); }

		rsl::Value mValue{};
	};

	struct Token
	{
		std::string_view Range;
		TokType Type = TokType::Unknown;
		Value ParsedVal;
	};

	using NativeMethod = Value(*)(ExecutionContext&, Value, intptr_t);

	struct Module
	{
		Module(OSInterface& alloc);

		void Parse(std::string_view str, std::string_view filename);

		template <typename T>
		NativeClass* CreateNativeClass(Namespace const* parent_namespace, std::string_view name, NativeClassType type, Class const* parent_class = nullptr, uint32_t flags = 0);

		void AddNativeMethod(Class* klass, std::string_view _name, intptr_t param_count, NativeMethod method);

		auto const& SymbolMap() const noexcept { return mSymbolMap; }

		Sym const* FindSymbol(std::string_view str) const;

		void Link();
		bool IsLinked() const noexcept { return mLinked; }

		Namespace const* GlobalNamespace() const noexcept { return mGlobalNamespace; }

		Class const* FindClass(std::string_view name) const noexcept;

		OSInterface& OSInterface() const { return mOSI; }

		template <typename T>
		NativeClass const* FindNativeClass() const;

		Value Null;
		Value True;
		Value False;

		Value ZeroFloat;
		Value ZeroInt;
		Value ZeroCodepoint;
		Value ZeroPoint;
		Value ZeroUserPointer;
		Value ZeroRange;

		Value OperatorNameSymbols[64];
		Value symCall;

		Class const* GetNullClass() const noexcept { return NullClass; }
#define INTERNALCLASS(name, ...) Class const* Get##name##Class() const noexcept { return name##Class; }
#include "InternalClasses.h"
#undef INTERNALCLASS

		inline Value MV(Sym const* v)         const { return Value{ SymbolClass, (GCBase*)v }; }
		inline Value MV(Namespace* v)         const { return Value{ NamespaceClass, (GCBase*)v }; }
		inline Value MV(intptr_t v)           const { return Value{ IntegerClass, v }; }
		inline Value MV(char32_t v)           const { return Value{ CodepointClass, (intptr_t)v }; }
		inline Value MV(double v)             const { return Value{ FloatClass, v }; }
		inline Value MV(int x, int y)         const { return Value{ PointIClass, x, y }; }
		inline Value MV(float x, float y)     const { return Value{ PointClass, x, y }; }
		inline Value MV(Map* v)               const { return Value{ MapClass, (GCBase*)v }; }
		inline Value MV(Arr* v)               const { return Value{ ArrayClass, (GCBase*)v }; }
		inline Value MV(Str* v)               const { return Value{ StringClass, (GCBase*)v }; }
		inline Value MV(Class const* v)       const { return Value{ ClassClass, (GCBase*)v }; }
		inline Value MV(Method* v)            const { return Value{ MethodClass, (GCBase*)v }; }
		inline Value MV(Field* v)             const { return Value{ FieldClass, (GCBase*)v }; }

	protected:

		rsl::OSInterface& mOSI;
		GCHeap mHeap{ mOSI };

		Class* NullClass = nullptr;
		Sym* symNull = nullptr;

		Namespace* const mGlobalNamespace = nullptr;

#define INTERNALCLASS(name, ...) Class* name##Class = nullptr; Sym* sym##name = nullptr;
#include "InternalClasses.h"
#undef INTERNALCLASS

	private:

		/// Lexing

		const char* mLexEnd = nullptr;
		const char* mCurLex = nullptr;
		const char* mStartLine = nullptr;
		SourcePos mCurPos;

#ifdef RSL_DEBUG
		std::map<std::string, Sym*, std::less<>> mSymbolMap;
#else
		tsl::htrie_map<char, Sym*> mSymbolMap;
#endif

		Sym* symSpecialVarEnd = nullptr;
		Sym* symNew = nullptr;

		Sym* MakeSym(std::string_view str);

		void EatNl();
		void Advance(bool start_exp);
		void LexID();
		void LexSym();
		void LexNum();
		void LexOp();

		Token mCurrentToken;

		/// Parsing

		bool Is(TokType tok) const { return mCurrentToken.Type == tok; }
		bool Is(KwType kw) const { return mCurrentToken.Type == TokType::Keyword && mCurrentToken.ParsedVal.Int == (intptr_t)kw; }
		bool Is(OpType kw) const { return mCurrentToken.Type == TokType::Operator && mCurrentToken.ParsedVal.Int == (intptr_t)kw; }
		bool IsBinOperator() const;
		bool IsUnOperator() const;
		bool IsEOF() const { return Is(TokType::Eof); }

		void Eat(TokType tok);
		void Eat(KwType tok);
		void Eat(OpType tok);
		Sym* EatId(std::string_view description);
		Sym* EatSym();
		template <typename T>
		bool TryEat(T tok) { return Is(tok) ? (Advance(true), true) : false; }
		void SkipNl();

		std::string CurrentTokenStr();

		void ParseClass();
		void ParseClassAlso(Class* klass);
		void ParseClassInterfaceList(Class* klass);
		void ParseEnum();
		Value& ParseConstant(Class* klass, const char* constant_type);
		void ParseEnumAlso(Class* klass);
		void ParseInterface();
		void ParseClassExtension();
		void ParseField(Class* klass);
		void ParseFieldAlso(Field* field);
		void ParseMethod(Class* klass);
		void ParseMethodAlso(Method* method);


		Arr* ParseQualifiedTypeName();
		static std::string JoinQTN(Arr* arr);

		[[nodiscard]] ExpTree ParseExp();
		[[nodiscard]] ExpTree ParseSuffixed();
		[[nodiscard]] ExpTree ParseSub(int limit, OpType& new_op);
		[[nodiscard]] ExpTree ParseArray();
		[[nodiscard]] ExpTree ParseMap();
		[[nodiscard]] StatTree ParseStat();
		[[nodiscard]] StatTree ParseIf();
		[[nodiscard]] StatTree ParseWhile();
		[[nodiscard]] StatTree ParseFor();
		[[nodiscard]] StatTree ParseVar();

		/// Codegen

		struct LocalVar
		{
			Sym* Name;
			intptr_t StackIndex = -1;
			SourcePos DeclPos;
			bool Used = false;
			const char* Type = "local";
		};

		struct LexicalScope
		{
			SourcePos StartPos, EndPos;
			intptr_t StartIP = 0, EndIP = 0;
			intptr_t FreeStackIndex = 0;
			std::map<Sym*, LocalVar> mLocals;
		};

		bool mLinked = false;

		std::map<Sym*, intptr_t> mLabelIPs;
		std::vector<intptr_t> mUnpatchedGotos;
		std::vector<LexicalScope> mScopeStack;

		void OpenScope();
		void CloseScope();
		LexicalScope& Scope() { return mScopeStack.back(); }
		intptr_t AddLocal(Sym* name, SourcePos const& pos, const char* local_type = "local");
		LocalVar* FindLocal(Sym* sym);

		/// TODO: Goto, labels, break, continue
		void AddLabel(Sym* name, intptr_t address);
		intptr_t FindLabel(Sym* name);
		void PatchGotos();

		void Link(Class* klass);
		void Link(Method* method);

		void CodeGen(StatTree const& stat);
		void CodeGen(ExpTree const& stat);
		bool EmitThisMember(Value name, SourcePos const& pos);

		intptr_t Emit(I instr, intptr_t a, intptr_t b, SourcePos const& pos);
		intptr_t Emit(I instr, intptr_t a, SourcePos const& pos) { return Emit(instr, a, 0, pos); }
		intptr_t Emit(I instr, SourcePos const& pos) { return Emit(instr, 0, 0, pos); }
		intptr_t Emit(I instr, Value v, SourcePos const& pos);
		intptr_t EmitJump(intptr_t dest_address, SourcePos const& pos);
		intptr_t EmitGoto(Sym* label, SourcePos const& pos);
		intptr_t EmitPush(Value v, SourcePos const& pos);
		void PatchPos(intptr_t ip, SourcePos const& pos);
		intptr_t IP() const;
		void Patch(intptr_t instruction_to_patch, intptr_t new_address);
		void PatchToHere(intptr_t instruction_to_patch);
		SourcePos PosForIP(intptr_t ip) const;

		bool mThisConstructed = true;
		intptr_t mSlotsAccessible = 0; /// Used in constructor initializer list creation, to make sure we are not referring to fields not yet initalized
		Method* mCurrentMethod = nullptr;
		Namespace* mCurrentNamespace = nullptr;
		std::vector<Class*> mClassesToLink;

		/// Declarations

		void StartNamespace(Sym* sym);
		void EndNamespace();
		void Add(Class* klass);
		void Add(Namespace* ns);

		void CheckForCycles(Class* if_class, Class* extends);

		void LoadStdLib();

		friend struct ClassBuilder;

		/// Cpp Interop

		NativeClass* CreateNativeClass(Namespace const* pn, std::string_view name, NativeClassType type, Class const* parent_class = nullptr, uint32_t flags = 0);

		[[noreturn]] void ThrowNativeClassExists(std::type_index ti, Class const* klass);

		std::map<std::type_index, NativeClass*> mNativeClasses;

	public:

		/// Errors

		template <typename... ARGS>
		[[noreturn]] void ThrowExceptionPos(ReportModule in_module, SourcePos const& pos, ARGS&&... args) const;
		template <typename... ARGS>
		void ReportWarningPos(ReportModule in_module, SourcePos const& pos, ARGS&&... args) const;
		template <typename... ARGS>
		[[noreturn]] void ThrowParseException(ARGS&&... args) const { ThrowExceptionPos(ReportModule::Parser, mCurPos, std::forward<ARGS>(args)...); }
		template <typename... ARGS>
		[[noreturn]] void ThrowLexException(ARGS&&... args) const { ThrowExceptionPos(ReportModule::Parser, mCurPos, std::forward<ARGS>(args)...); }
		template <typename... ARGS>
		[[noreturn]] void ThrowCodeGenException(SourcePos const& pos, ARGS&&... args) const { ThrowExceptionPos(ReportModule::CodeGen, pos, std::forward<ARGS>(args)...); }
		template <typename... ARGS>
		[[noreturn]] void ReportDeclError(struct Declaration* decl, ARGS&&... args) const;
		template <typename... ARGS>
		[[noreturn]] void ReportDeclError(SourcePos const& pos, ARGS&&... args) const { ThrowExceptionPos(ReportModule::Declaration, pos, std::forward<ARGS>(args)...); }
		template <typename... ARGS>
		void ReportWarning(ReportModule in_module, ARGS&&... args) const { ReportWarningPos(in_module, mCurPos, std::forward<ARGS>(args)...); }

		template <typename... ARGS>
		void Report(ReportType type, ReportModule in_module, SourcePos const& pos, ARGS&&... args) const
		{
			mOSI.ReportSingle(type, in_module, fmt::format(std::forward<ARGS>(args)...));
		}
		template <typename... ARGS>
		void Report(ReportType type, ReportModule in_module, ARGS&&... args) const
		{
			mOSI.ReportSingle(type, in_module, SourcePos{}, fmt::format(std::forward<ARGS>(args)...));
		}
	};


	template<typename T>
	inline NativeClass const* Module::FindNativeClass() const
	{
		using Type = std::remove_cvref_t<std::decay_t<std::remove_cvref_t<T>>>;
		static_assert(std::is_class_v<Type> || std::is_fundamental_v<Type>);
		std::type_index ti = typeid(Type);
		auto it = mNativeClasses.find(ti);
		if (it == mNativeClasses.end())
			return nullptr;
		return it->second;
	}
	
}