#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <enum_flags.h>

namespace rsl
{

	enum class ReportType
	{
		Trace,
		Warning,
		Error,
		AssumptionFailure,
	};

	enum class ReportModule
	{
		Lexer,
		Parser,
		Declaration,
		CodeGen,
		Runtime,
		OSI,
	};

	struct SourcePos
	{
		intptr_t Line = 1;
		intptr_t Column = 0;
		struct Sym* FileName = nullptr;
		std::string ToString() const;
	};

	inline std::ostream& operator<<(std::ostream& strm, SourcePos const& pos)
	{
		strm << pos.ToString();
		return strm;
	}

	struct OSInterface
	{
		virtual ~OSInterface() = default;

		virtual uint8_t* Allocate(size_t n);
		virtual uint8_t* Reallocate(uint8_t* data, size_t n);
		virtual void Free(uint8_t* data);

		virtual void ReportSingle(ReportType type, ReportModule in_module, SourcePos const& pos, std::string_view message);

	};

}