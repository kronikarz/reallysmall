#pragma once

#include "OSI.h"
#ifdef RSL_MULTITHREADED_SUPPORT
#include <thread>
#include <mutex>
#endif

namespace rsl
{

	enum class BaseObjectFlags
	{
		Marked,
		Permanent,
		Const,
	};
	static inline constexpr int StartCustomFlags = 16;

	struct Value;
	struct ExecutionContext;

	struct GCBase
	{
#ifdef RSL_MULTITHREADED_SUPPORT
		std::atomic_uint32_t RefCount = 0;
#else
		uint32_t RefCount = 0;
#endif
		void AddRef() { ++RefCount; }
		void Release() { --RefCount; }
		uint32_t Flags = 0;
		GCBase() noexcept = default;
		GCBase(GCBase const& other) : Flags(other.Flags) {}
		GCBase(GCBase&& other) = default;
		GCBase(uint32_t flags) noexcept : Flags(flags) {}
		virtual Value IndexGet(ExecutionContext& vm, Value key) const;
		virtual void IndexSet(ExecutionContext& vm, Value key, Value value);
		virtual intptr_t IndexSize(ExecutionContext& vm) const;
		virtual void MarkMembers() = 0;
		virtual std::string ToString() const = 0;
		virtual ~GCBase() = default;

		void Mark();

		bool Is(BaseObjectFlags fl) const noexcept { return ghassanpl::is_flag_set(Flags, fl); }
		void Set(BaseObjectFlags fl) noexcept { ghassanpl::set_flags(Flags, fl); }
		void Unset(BaseObjectFlags fl) noexcept { ghassanpl::unset_flags(Flags, fl); }
	};

	struct GCHeap
	{
		GCHeap(OSInterface& osi) : mOSI(osi) {}
		~GCHeap() { Destroy(mGCObjs); }

		/// GC
		template <typename T, typename... ARGS>
		std::enable_if_t<std::is_base_of_v<GCBase, T>, T*> NewSized(intptr_t add, ARGS&&... args)
		{
#ifdef RSL_MULTITHREADED_SUPPORT
			std::lock_guard<std::recursive_mutex> lock{ mGCMutex };
#endif
			auto obj = new (mOSI.Allocate(size_t(sizeof(T) + add))) T{ std::forward<ARGS>(args)... };
			mGCObjs.push_back(obj);
			return obj;
		}

		template <typename T, typename... ARGS>
		std::enable_if_t<std::is_base_of_v<GCBase, T>, T*> New(ARGS&&... args)
		{
			return this->NewSized<T>(0, std::forward<ARGS>(args)...);
		}

#ifdef RSL_MULTITHREADED_SUPPORT
		void LockGC() { mGCMutex.lock(); }
		void UnlockGC() { mGCMutex.unlock(); }
#else
		void LockGC() { }
		void UnlockGC() { }
#endif

		void Sweep();

	protected:

		void Destroy(std::vector<GCBase*> const& garbage);

		OSInterface& mOSI;
#ifdef RSL_MULTITHREADED_SUPPORT
		std::recursive_mutex mGCMutex;
#endif
		std::vector<GCBase*> mGCObjs;
	};
}