#pragma once

#include "RSL.h"

namespace rsl
{

	enum class IndexType : intptr_t
	{
		ImmediateValue = 1,
		LocalIndex,
	};

	struct Instruction;

	struct RSRuntimeException : RSException
	{
		std::string MethodName;
		int IP = -1;
		using RSException::RSException;
		virtual std::string ToString() const override;
	};

	struct ExecutionContext
	{
		ExecutionContext(Module const& executable, intptr_t min_stack = 64)
			: Mod(executable)
			, mMinStack(min_stack)
		{
			Top = ExecutionStack.data() - 1;
			EnsureStack();
		}

		Module const& Mod;

		Value const Null = Mod.Null;
		Value const True = Mod.True;
		Value const False = Mod.False;

		/// ///////////////////////////////// ///
		/// Queries
		/// ///////////////////////////////// ///

		Value This() const { return StartFrame->Value; }
		bool Ready() const { return CallFrames.empty(); }
		bool Executing() const { return !CallFrames.empty() && !CallFrames.back().Suspended; }
		bool Suspended() const { return !CallFrames.empty() && CallFrames.back().Suspended; }
		GCHeap& Heap() { return mHeap; }
		[[nodiscard]] Method const* CurrentMethod() const { return CallFrames.back().Method; }

		/// ///////////////////////////////// ///
		/// Stack
		/// ///////////////////////////////// ///

		void Push(Value v);
		template <typename T>
		void Push(T&& v)
		{
			return Push(ToValue(v));
		}

		void Pop(size_t n);
		template <size_t N = 1>
		void Pop() {
			Top -= N;
			if (Top < StartStack - 1)
				ThrowRuntimeExceptionPos({}, "stack underflow");
#ifdef RSL_DEBUG
			Mod.Report(ReportType::Trace, ReportModule::Runtime, "pop({})", N);
#endif
		}

		[[nodiscard]] ValueRef operator[](intptr_t index);

		[[nodiscard]] intptr_t StackSize() const { return (Top - StartFrame) + 1; }

		/// ///////////////////////////////// ///
		/// Value access & conversion
		/// ///////////////////////////////// ///

		Value ToValue(bool const& v) const;
		Value ToValue(intptr_t const& v) const;
		Value ToValue(int32_t const& v) const;
		Value ToValue(double const& v) const;
		Value ToValue(Value v) const { return v; }
		Value ToValue(ValueRef const& v) const { return v.Value(); }
		template <typename T>
		Value ToValue(T* ptr) const
		{
			auto klass = Mod.FindNativeClass<T>();
			if (!klass)
				return { Mod.GetUserPointerClass(), (void*)ptr };
			else
				return { klass, (void*)ptr };
		}

		Value ToValue(int x, int y) const;

		template <typename T>
		Value ToValue(T const& v) const
		{
			return to_value(*this, v);
		}

		void FromValue(bool& to, Value from) const;
		void FromValue(intptr_t& to, Value from) const;
		void FromValue(int32_t& to, Value from) const;
		void FromValue(double& to, Value from) const;
		void FromValue(std::string& to, Value from) const;
		void FromValue(Value& to, Value from) const { to = from; }
		void FromValue(ValueRef& v, Value from) const { v = from; }

		ValueRef CreateIterator(intptr_t index);
		void IterateOver(intptr_t index, std::function<bool(ValueRef const&, ValueRef const&)> func);

		/// ///////////////////////////////// ///
		/// Objects
		/// ///////////////////////////////// ///

		template <typename THIS, typename V>
		void SetField(THIS&& _this, std::string_view name, V&& value)
		{
			Push(_this);
			Push(value);
			ContinueSetField(name);
		}

		/// ///////////////////////////////// ///
		/// Calls
		/// ///////////////////////////////// ///

		/// Low level

		/// Function: ExecuteMethod
		/// Leaves result on the stack
		ValueRef ExecuteMethod(Method const& method, intptr_t nargs);

		/// Function: ResumeExecution
		/// Expects yield return value in the stack slot as the yield result
		ValueRef ResumeExecution();

		/// TODO: See if this is possible
		///// Function: YieldFromNative
		//void YieldFromNative();

		/// High level
		template <typename THIS_TYPE, typename... ARGS>
		ValueRef Call(THIS_TYPE&& _this, std::string_view name, ARGS&&... args)
		{
			Push(_this);
			(Push(std::forward<ARGS>(args)), ...);
			return ContinueCall(name, sizeof...(args));
		}

		template <typename THIS_TYPE, typename... ARGS>
		ValueRef CallConstructor(THIS_TYPE&& _this, ARGS&&... args)
		{
			Push(_this);
			(Push(std::forward<ARGS>(args)), ...);
			return ContinueCallConstructor(sizeof...(args));
		}

		template <typename... ARGS>
		ValueRef New(Class const* klass, ARGS&&... args)
		{
			if (!klass)
				ThrowRuntimeExceptionPos({}, "invalid argument 'klass'");

			ValueRef obj = RawNew(klass);
			if (!klass->Is(ClassFlags::NotGC))
				CallConstructor(obj.Value(), std::forward<ARGS>(args)...);
			return obj;
		}

		template <typename T>
		ValueRef Resume(T&& yield_return)
		{
			Push(std::forward<T>(yield_return));
			return ResumeExecution();
		}

		/// ///////////////////////////////// ///
		/// Exceptions
		/// ///////////////////////////////// ///

		template<typename ...ARGS>
		[[noreturn]] void ThrowRuntimeException(ARGS&& ...args) const
		{
			ThrowRuntimeExceptionPos({}, fmt::format(std::forward<ARGS>(args)...));
		}

		[[noreturn]] void ThrowRuntimeExceptionPos(SourcePos const& pos, std::string message) const;

		/// ///////////////////////////////// ///
		/// GC
		/// ///////////////////////////////// ///

		void RunGC()
		{
			Mark();
			Sweep();
		}

		/// ///////////////////////////////// ///
		/// Debugging
		/// ///////////////////////////////// ///

		void DumpStack();

	protected:

		struct CallFrame
		{
			Method const* Method;
			intptr_t SavedStackFrame;
			intptr_t SavedIP;
			bool Suspended = false;
		};

		struct LazyValue
		{
			LazyValue() : Container(IndexType::ImmediateValue), Key(Value::Invalid()) {}
			LazyValue(Value v) : Container{ IndexType::ImmediateValue }, Key{ v } {}
			LazyValue(ValueRef const& v) : Container{ IndexType::ImmediateValue }, Key{ v.Value() } {}
			LazyValue(Value a, Value b) : Container(a), Key(b) {}

			Value Container;
			union { Value Key; Value Value; };

			bool Resolved() const { return Container.Is(IndexType::ImmediateValue); }
			bool Unresolved() const { return !Container.Is(IndexType::ImmediateValue); }

			std::string ToString(ExecutionContext const& vm) const;

			void Mark() { Key.Mark(); Value.Mark(); }
		};

	protected:

		intptr_t const mMinStack = 64;

		OSInterface& mOSI{ Mod.OSInterface() };
		GCHeap mHeap{ mOSI };

		std::vector<LazyValue> ExecutionStack;
		LazyValue* Top = nullptr;
		LazyValue* StartStack = nullptr;
		LazyValue* EndStack = nullptr;
		LazyValue* StartFrame = nullptr;

		std::vector<CallFrame> CallFrames;

		void Suspend(Instruction const* ip);

	protected:

		void Mark();
		void Sweep() { mHeap.Sweep(); }

		void PushCallFrame(Method const* method, Instruction const* current_instruction, intptr_t nargs);
		[[nodiscard]] Instruction const* PopCallFrame();
		void DoCode(Instruction const*);

		ValueRef ContinueCall(std::string_view name, size_t nargs);
		ValueRef ContinueCallConstructor(size_t nargs);

		void SetFieldInternal(Value container, Value member_name, Value value);
		void ContinueSetField(std::string_view name);

		Method const* GetLastNonNativeMethod();

		void EnsureStack();

		void Push(LazyValue v);
		void PushLocalRef(intptr_t local);

		ValueRef TryOverloadedOp(ValueRef const& a, ValueRef const& b, OpType op);
		ValueRef TryOverloadedOp(ValueRef const& a, OpType op);
		Method const* TryOverloadedCall(ValueRef const& a, intptr_t argc);

		[[nodiscard]] ValueRef DoBinOp(ValueRef const& a, ValueRef const& b, OpType op);
		[[nodiscard]] ValueRef DoUnOp(ValueRef const& b, OpType op);

		template <intptr_t STACK_ID = 0>
		Value MakeResolved() {
			if constexpr (STACK_ID > 0)
				return MakeResolved(StartFrame[STACK_ID - 1]);
			else
				return MakeResolved(Top[STACK_ID]);
		}

		Value MakeResolved(LazyValue* v) { *v = GetResolved(v); return v->Value; }
		Value MakeResolved(LazyValue& v) { return MakeResolved(&v); }

		template <intptr_t STACK_ID = 0>
		[[nodiscard]] Value GetResolved() {
			if constexpr (STACK_ID > 0)
				return GetResolved(StartFrame[STACK_ID - 1]);
			else
				return GetResolved(Top[STACK_ID]);
		}

		[[nodiscard]] Value GetResolved(LazyValue const*);
		[[nodiscard]] Value GetResolved(LazyValue const& v) { return GetResolved(&v); }
		/*
		template <intptr_t STACK_ID = 0>
		[[nodiscard]] Value Resolve() {
			if constexpr (STACK_ID > 0)
				return Resolve(StartFrame[STACK_ID - 1]);
			else
				return Resolve(Top[STACK_ID]);
		}

		[[nodiscard]] Value Resolve(LazyValue const*);
		[[nodiscard]] Value Resolve(LazyValue const& v) { return Resolve(&v); }
		*/

		[[nodiscard]] Value GetSlotRaw(Value val, Value key);
		void SetSlot(Value container, Value key, Value val);

		/// Does not call constructor
		Value RawNew(Class const* klass);

	};

}