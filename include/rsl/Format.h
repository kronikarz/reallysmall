#pragma once

#define FMT_USE_WINDOWS_H 0
#define FMT_HEADER_ONLY 1
#define FMT_SAFE_DURATION_CAST 0
#undef _WIN32
#include <fmt/format.h>
#include <fmt/ostream.h>
#define _WIN32 1