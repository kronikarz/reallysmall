#pragma once

#include "RSL.h"
#include "ExecutionContext.h"
#include <gsl/span>

namespace rsl
{
	template <typename T>
	struct CppObjLayout
	{
		std::aligned_storage_t<sizeof(T), alignof(T)> Object;
		Value Slots[1];

		static intptr_t SlotsOffset() { auto self = (CppObjLayout*)nullptr; return reinterpret_cast<char*>(&self->Slots) - reinterpret_cast<char*>(&self); }
	};

	enum class NativeClassType
	{
		RefType,
		ValueType,
		FullType,
	};

	struct NativeClass final : Class
	{
		void* UserData = nullptr;

		NativeClassType Type() const { return mType; }

		void CallConstructor(void* obj) const;
		void CallDestructor(void* obj) const;

		template <typename FUNC>
		void AddMethod(Module& mod, std::string_view name, FUNC&& func);

		virtual Value PreConstruct(ExecutionContext& vm) const override;

	protected:

		friend struct Module;
		friend struct GCHeap;

		NativeClass(Namespace* pn, Sym* name, NativeClassType type, uint32_t flags);

		template <typename T>
		void SetStaticType();

		std::type_index TypeIndex = typeid(void);
		intptr_t ObjectSize = 0;
		uint8_t Alignment = alignof(double);
		intptr_t SlotsOffset = 0;

		NativeClassType mType;

		template <typename TUPLE, size_t... I>
		static void GetArguments(TUPLE& arguments, ExecutionContext& vm, std::index_sequence<I...> seq)
		{
			(vm.FromValue(std::get<I + 1>(arguments), vm[I + 1].Value()), ...);
		}

		template <typename T, typename... ARGS>
		static void GetArguments(std::tuple<T*, ARGS...>& arguments, ExecutionContext& vm, Value _this, intptr_t nargs)
		{
			((NativeClass*)_this.Type)->mFromValue(vm, &std::get<0>(arguments), _this);
			GetArguments(arguments, vm, std::index_sequence_for<ARGS...>{});
		}

		template <typename RESULT, typename T, typename... ARGS>
		static std::tuple<T*, ARGS...> TupleTypeFor(RESULT(T::* fptr)(ARGS...));
		template <typename RESULT, typename T, typename... ARGS>
		static std::tuple<T const*, ARGS...> TupleTypeFor(RESULT(T::* fptr)(ARGS...)const);
		template <typename RESULT, typename T, typename... ARGS>
		static std::tuple<T*, ARGS...> TupleTypeFor(RESULT(*)(T*, ARGS...));
		template <typename RESULT, typename T, typename... ARGS>
		static std::tuple<T const*, ARGS...> TupleTypeFor(RESULT(*)(T const*, ARGS...));

		template <typename FUNC>
		using tuple_type_for = decltype(TupleTypeFor(std::declval<FUNC>()));

		std::function<void(void*)> mNativeConstructor;
		std::function<void(void*)> mNativeDestructor;
		std::function<void(void*, void const*)> mNativeCopyConstructor;
	};

	template<typename T>
	inline NativeClass* Module::CreateNativeClass(Namespace const* parent_namespace, std::string_view name, NativeClassType type, Class const* parent_class, uint32_t flags)
	{
		if (IsLinked())
		{
			Report(ReportType::Warning, ReportModule::Declaration, "trying to create native class '{}' in linked module, returning null", name);
			return nullptr;
		}

		using Type = std::remove_cvref_t<std::decay_t<std::remove_cvref_t<T>>>;
		static_assert(std::is_class_v<Type> || std::is_fundamental_v<Type>);
		std::type_index ti = typeid(Type);
		if (auto existing = FindNativeClass<T>())
			ThrowNativeClassExists(ti, existing);

		auto nc = CreateNativeClass(parent_namespace, name, type, parent_class, flags);
		nc->SetStaticType<T>();
		return mNativeClasses[ti] = nc;
	}

	template<typename FUNC>
	inline void NativeClass::AddMethod(Module& mod, std::string_view name, FUNC&& func)
	{
		using tuple_type = tuple_type_for<FUNC>;
		using result_type = decltype(std::apply(func, tuple_type{}));
		static constexpr size_t tuple_size = std::tuple_size_v<tuple_type>;
		mod.AddNativeMethod(this, name, tuple_size - 1, [func](ExecutionContext& vm, Value _this, intptr_t nargs) {
			tuple_type arguments;
			GetArguments(arguments, vm, _this, nargs);
			if constexpr (std::is_void_v<result_type>)
			{
				std::apply(func, arguments);
				return vm.Null;
			}
			else
				return vm.ToValue(std::apply(func, arguments));
			});
	}

	template<typename T>
	inline void NativeClass::SetStaticType()
	{
		TypeIndex = typeid(T);
		ObjectSize = sizeof(CppObjLayout<T>) - sizeof(Value);
		Alignment = alignof(T);
		SlotsOffset = CppObjLayout<T>::SlotsOffset();

		if constexpr (std::is_default_constructible_v<T>)
		{
			mNativeConstructor = [](void* ptr) { new (ptr) T{}; };
		}
		if constexpr (std::is_copy_constructible_v<T>)
		{
			mNativeCopyConstructor = [](void* _to, void const* _from) {
				auto from = (T const*)_from;
				new (_to) T{ *from };
			};
		}
		if constexpr (std::is_destructible_v<T> && !std::is_trivially_destructible_v<T>) mNativeDestructor = [](void* ptr) { std::destroy_at((T*)ptr); };
		if constexpr (std::is_polymorphic_v<T>) Set(ClassFlags::Polymorphic);
		if constexpr (std::is_final_v<T>) Set(ClassFlags::Final);
		if constexpr (std::is_abstract_v<T>) Set(ClassFlags::Abstract);
		if constexpr (std::is_trivial_v<T>) Set(ClassFlags::Trivial);
	}

	/*
																																													 VObject
																																											+-----------------+
		C++ side                                                                          |C++ virtual      |
		--------                                                                          |class, creatable |
																																											+--------+--------+
					 RefOnlyClass                ValueType               NVObject                        |                     RSLRefType
			 +-----------------+       +-----------------+      +-----------------+         +--------v--------+       +-----------------+
			 |C++ non-virtual  |       | C++ value type  |      |C++ non-virtual  |         |  VObject_Proxy  |       |Some sort of     |
			 |class            |       |                 |      |class, creatable |         |                 |       |refcounted holder|
			 +--------+--------+       +----+--------^---+      +--------+--------+         +--------+--------+       +----+--------^---+
								|                     |        |                   |                           |                     |        |
	--------------|---------------------|--------|-------------------|---------------------------|---------------------|--------|--------------------
								|                     |        |                   |                           |                     |        |
			 +--------v--------+       +----v--------+---+      +--------v--------+         +--------v--------+       +----v--------+---+
			 | Type: CppPtrNV  |       | Any RSL type,   |      | Type: CppObjNV  |         |  Type: CppObj   |       |                 |
			 -------------------       | depends on the  |      -------------------         -------------------       | Any GC RSL type |
			 | Pointer, NonGC  |       | type spec.      |      | GCVal, CppObjNV |         |  GCVal, CppObj  |       |                 |
			 +-----------------+       +-----------------+      +--------+--------+         +--------+--------+       +-----------------+
																																	 |                           |
			 * Owned by C++            * Copy by value, both             |                           |
			 * Ref-counted by RSL?       ways                   +--------v--------+         +--------v--------+
			 * Is a simple pointer,                             | * Storage for   |         | * Storage for   |
				 can't do much                                    |   NVObject      |         |   VObject       |
																													| * Slots for     |         | * Slots for     |
																													|   children      |         |   children      |
																													| * RefCount?     |         | * RefCount?     |
																													+-----------------+         +-----------------+

		RSL side                                              * Owned by RSL              * Owned by RSL            * Owned by RSL
		--------                                              * Has a RefCount, must      * Has a RefCount, must    * Has a RefCount, must
																														be held in a RefPtr on      be held in a RefPtr on    be held in a RefPtr on
																														C++ side                    C++ side                  C++ side
	*/
}