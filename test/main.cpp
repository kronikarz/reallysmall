#include <rsl/RSL.h>
#include <rsl/ExecutionContext.h>
#include <rsl/CppInterop.h>

#define FMT_HEADER_ONLY 1
#include <fmt/format.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <filesystem>
#include <gtest/gtest.h>

rsl::OSInterface alloc;

rsl::ValueRef RunTestCode(rsl::ExecutionContext& vm, std::string const& name)
{
	if (!vm.Mod.SymbolMap().count("test")) return vm.True;
	if (!vm.Mod.SymbolMap().count("run")) return vm.True;

	auto test_sym = vm.Mod.SymbolMap().at("test");
	auto run_sym = vm.Mod.SymbolMap().at("run");
	rsl::ValueRef result = vm.True;
	if (auto test_class = vm.Mod.GlobalNamespace()->FindNamespaceMemberSelfOnly(test_sym))
	{
		auto klass = test_class.Class;
		vm.Push(vm.New(klass));
		auto method = klass->FindMember(vm.Mod.MV(run_sym));
		if (!method.IsValid()) return vm.True;
		fmt::print("[{}] Test has test function, running...", name);
		try
		{
			result = vm.ExecuteMethod(*method.Method, 0);
		}
		catch (...)
		{
			fmt::print(" complete with exception.\n");
			throw;
		}
		fmt::print(" complete.\n");
	}
	vm.RunGC();
	return result;
}

bool RunTest(std::filesystem::path fname, bool fail_test)
{
	auto test_name = fname.stem().string();
	std::stringstream buffer;
	if (fname.extension() == ".json")
		buffer << "class _TEST_ var _JSON_ = ";
	fmt::print("[{}] Starting test\n", test_name);
	std::ifstream srm{ fname };
	buffer << srm.rdbuf();
	srm.close();
	if (fname.extension() == ".json")
		buffer << " end\n";
	rsl::Module pr{ alloc };
	rsl::ExecutionContext vm{ pr };
	rsl::ValueRef result = pr.Null;
	try
	{
		pr.Parse(buffer.str(), fname.string());
		pr.Link();
		result = RunTestCode(vm, test_name);
	}
	catch (rsl::RSException& e)
	{
		if (fail_test)
			fmt::print("[{}] \u001b[32mPASSED\u001b[0m\n", test_name);
		else
			fmt::print("[{}] \u001b[31mFAILED\u001b[0m: {}\n", test_name, e.ToString());
		return !fail_test;
	}

	if (fail_test)
	{
		fmt::print("[{}] \u001b[31mFAILED\u001b[0m: test should have failed but didn't\n", test_name);
		return true;
	}
	else if (result.Value() != pr.True)
	{
		fmt::print("[{}] \u001b[31mFAILED\u001b[0m: code returned '{}'\n", test_name, result->ToString());
		return true;
	}
	
	fmt::print("[{}] \u001b[32mPASSED\u001b[0m\n", test_name);
	return false;
}

struct Test
{

};

int main(int argc, char** argv)
{
	bool stop_on_first_fail = false;
	bool do_jsons = false;
	
	if (RunTest("Tests/MustPass/arrays.rsl", false)) return -1;

	int failed_count = 0, count = 0;
	for (auto it = std::filesystem::recursive_directory_iterator{ "Tests/MustPass" }; it != std::filesystem::recursive_directory_iterator{}; ++it)
	{
		if (!it->is_regular_file())
			continue;
		if (!do_jsons && it->path().extension() == ".json") continue;
		count++;
		bool failed = RunTest(it->path(), false);
		if (failed && stop_on_first_fail)
			return failed_count;
		failed_count += failed;
		fmt::print("===========================\n");
	}

	for (auto it = std::filesystem::recursive_directory_iterator{ "Tests/MustFail" }; it != std::filesystem::recursive_directory_iterator{}; ++it)
	{
		if (!it->is_regular_file())
			continue;
		if (!do_jsons && it->path().extension() == ".json") continue;
		count++;
		bool failed = RunTest(it->path(), true);
		if (failed && stop_on_first_fail)
			return failed_count;
		failed_count += failed;
		fmt::print("===========================\n");
	}

	if (failed_count)
		fmt::print("\u001b[32mPASSED\u001b[0m: {}, \u001b[31mFAILED\u001b[0m: {}\n", count- failed_count, failed_count);
	else
		fmt::print("\u001b[32mALL {} PASSED\u001b[0m\n", count);

	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();

	return failed_count;
}
